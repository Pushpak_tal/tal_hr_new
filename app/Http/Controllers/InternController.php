<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InternDetail;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use App\TempInternUpdate;
use App\InternDailyWork;
use Carbon\Carbon;
use App\updateRequestIntern;
use App\Notification;
use App\ceoinbox;
use App\ceoMessageReply;
use Mail;

class InternController extends Controller
{
    public function internRegistration(){

    	return view('intern.pages.internRegistration');
    }

    public function internRegisterService(Request $request){

    	$rules = [

                    'name' =>'required',
                    'email' =>'required|email',
                    'phone' =>'required|digits:10',
                    'dob' =>'required',
                    'join_date' =>'required',
                    'image' =>'required',
                    'id_proof' =>'required',
                    'address_proof'=>'required',
                    ];

        $this->validate($request,$rules);

        $data=$request->all();

        $email_check=InternDetail::where('email',$request->email)->first();

        if(!empty($email_check)){

        	return back();
        }

        else{

        	if($request->hasfile('image')){

        		$file=$request->file('image');
		    	$filename='image'.time().'.'.$request->image->extension();
		    	$destination=storage_path('../public/upload/intern');
		    	$file->move($destination,$filename);
		    	$path1="/".$filename;
        	}

        	if($request->hasfile('id_proof')){

        		$file=$request->file('id_proof');
		    	$filename='id_proof'.time().'.'.$request->id_proof->extension();
		    	$destination=storage_path('../public/upload/intern');
		    	$file->move($destination,$filename);
		    	$path2="/".$filename;
        	}

        	if($request->hasfile('address_proof')){

        		$file=$request->file('address_proof');
		    	$filename='address_proof'.time().'.'.$request->address_proof->extension();
		    	$destination=storage_path('../public/upload/intern');
		    	$file->move($destination,$filename);
		    	$path3="/".$filename;
        	}

        	$employee=new InternDetail;

        	$employee->name=$data['name'];
        	$employee->email=$data['email'];
        	$employee->phone=$data['phone'];
        	$employee->dob=$data['dob'];
        	$employee->join_date=$data['join_date'];
        	$employee->image=$path1;
        	$employee->id_proof=$path2;
        	$employee->address_proof=$path3;

            // print_r($employee);die();

        	$employee->save();

        	$to_name=$data['name'];
            $to_email=$data['email'];


            $data=array('name' => "$to_name");

            Mail::send('intern.pages.mail.internMail',$data,function($message) use ($to_name,$to_email){
                             $message->to($to_email)->subject('Intern Mail');
            });

              \Session::flash("message","Thank you for registering, Activation details has been sent to your e-mail, Please follow the link and complete the registration.");
              \Session::flash("status","success");


             return back();


        }
    }

    public function internLogin(){

    	return view('intern.pages.internLogin');
    }

    public function internLoginServeice(Request $request){

    	$rules = [

            'username' => 'required',
            'password' => 'required'

        ];

        $this->validate($request, $rules);

        $username = $request->input('username');
        $password = $request->input('password');


        $check_login = InternDetail::where('username', $username)->first();

         if (!empty($check_login)) {



            $db_password = $check_login->password;


            if (Hash::check($password, $db_password)) {


               
                $u_name = $check_login->username;

                $intern_id=$check_login->intern_id;

                $name=$check_login->name;

                // print_r($intern_id);die();

                $encrypt_username = Crypt::encryptString($u_name);

                session(['internsession' => $encrypt_username]);
                session(['internidsession' => $intern_id]);
                session(['internname' => $name]);
                session(['internusername' => $u_name]);
                

            \Session::flash('message', "You are successfully loggedin");
            \Session::flash('alert-class', 'alert-success');

                return redirect('/internDashboard');
            } else {

                \Session::flash('message', "You have entered wrong password");
                \Session::flash('alert-class', 'alert-danger');
                return back();
            }
        } else {

            \Session::flash('message', "You have entered wrong username");
            \Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function internLogout(){

        \Session::flush();
        \Session::flash('message', "You are log out");
        \Session::flash('alert-class', 'alert-success');
        return redirect('/internLogin');
    }

    public function internProfile(){

        $internDetail=InternDetail::where('intern_id',session()->get('internidsession'))->first();

        // print_r($internDetail);die();

        return view('intern.pages.internProfile',compact('internDetail'));
    }

    public function editTempintern(Request $request){

        $data=$request->all();

        if($data['old_name'] != $data['name']){

            $tempEmployee=new TempInternUpdate;

            $tempEmployee->username=$data['username'];
            $tempEmployee->field_name='Name';
            $tempEmployee->old_value=$data['old_name'];
            $tempEmployee->new_value=$data['name'];
            $tempEmployee->status='0';
            $tempEmployee->save();
        }
        if($data['old_email'] != $data['email']){

            $tempEmployee=new TempInternUpdate;

            $tempEmployee->username=$data['username'];
            $tempEmployee->field_name='Email';
            $tempEmployee->old_value=$data['old_email'];
            $tempEmployee->new_value=$data['email'];
            $tempEmployee->status='0';
            $tempEmployee->save();
        }
        if($data['old_phone'] != $data['phone']){

            $tempEmployee=new TempInternUpdate;

            $tempEmployee->username=$data['username'];
            $tempEmployee->field_name='Phone';
            $tempEmployee->old_value=$data['old_phone'];
            $tempEmployee->new_value=$data['phone'];
            $tempEmployee->status='0';
            $tempEmployee->save();
        }
        if($data['old_join_date'] != $data['join_date']){

                $tempEmployee=new TempInternUpdate;

                $tempEmployee->username=$data['username'];
                $tempEmployee->field_name='Join Date';
                $tempEmployee->old_value=$data['old_join_date'];
                $tempEmployee->new_value=$data['join_date'];
                $tempEmployee->status='0';
                $tempEmployee->save();            
        }

        if(isset($data['image'])){

           if($data['old_image'] != $data['image']){

            if($request->hasfile('image')){

                $file=$request->file('image');
                $filename='image'.time().'.'.$request->image->extension();
                $destination=storage_path('../public/upload/intern');
                $file->move($destination,$filename);
                $path1="/".$filename;
            }

            $tempEmployee=new TempInternUpdate;
            $tempEmployee->username=$data['username'];
            $tempEmployee->field_name='Image';
            $tempEmployee->old_value=$data['old_image'];
            $tempEmployee->new_value=$path1;
            $tempEmployee->status='0';
            $tempEmployee->save();

        } 
        }
        
        return back();
    }

    public function tempInternImage(Request $request){

        $data=$request->all();

        if($request->hasfile('image')){

                $file=$request->file('image');
                $filename='image'.time().'.'.$request->image->extension();
                $destination=storage_path('../public/upload/intern');
                $file->move($destination,$filename);
                $path1="/".$filename;

                
        }

        $intern_image=new TempInternUpdate;

        $intern_image->image=$path1;
        $intern_image->username=$data['username'];
        $intern_image->name=$data['name'];
        $intern_image->email=$data['email'];
        $intern_image->phone=$data['phone'];
        $intern_image->dob=$data['dob'];
        $intern_image->join_date=$data['join_date'];

        $intern_image->save();
        
        return back();

    }

    public function internWorkDetail(Request $request){

        $date=Carbon::now()->toDateString();
        $white_ips=['103.242.190.64','10.14.205.179','125.22.76.178'];

        $date_check=InternDailyWork::where('added_by',\Session::get('internusername'))->first();

        if($date_check){

            \Session::flash('message', "You can't update twice in one day");
            \Session::flash('alert-class', 'alert-danger');

            return back();
        }

         else{

            if(!in_array($request->ip(), $white_ips)){

                $daily_work=new InternDailyWork;

                $daily_work->name=\Session::get('internname');
                $daily_work->body=$request->body;
                $daily_work->date=$date;
                $daily_work->added_by=\Session::get('internusername');
                $daily_work->type=0;

                $daily_work->save();

                \Session::flash('message', "Update has been posted");
                \Session::flash('alert-class', 'alert-success');

                return back();

            }

            else{

                $daily_work=new InternDailyWork;

                $daily_work->name=\Session::get('internname');
                $daily_work->body=$request->body;
                $daily_work->date=$date;
                $daily_work->added_by=\Session::get('internusername');
                $daily_work->type=1;

                $daily_work->save();

                 \Session::flash('message', "Update has been posted");
                    \Session::flash('alert-class', 'alert-success');

                return back();

            }
        }
    }

    public function viewInternDailyUpdate(){

        $internUpdate=InternDailyWork::all();

        return view('intern.pages.internDailyUpdate',compact('internUpdate'));
    }

    public function updateInternDailyTask(Request $request){

        $intern_id=$request->input('id');
        $body=$request->input('body');

        $intern_update=InternDailyWork::where('id',$intern_id)->update(['body'=>$body]);

        \Session::flash('message', "Your task successfully posted");
        \Session::flash('alert-class', 'alert-success');

        return back();
    }

    public function internRequestUpdate(){

        return view('intern.pages.internRequestUpdate');
    }

     public function updateRequestIntern(Request $request){

        $rules=[

            'date'=>'required',
            'workplace'=>'required',
            'body'=>'required',
            'reason'=>'required',

        ];

        $this->validate($request,$rules);

        $current_date=Carbon::now()->toDateString();
        $date=$request->input('date');
        $whiteIps = ['103.242.190.64','10.14.205.179','125.22.76.178'];

        $dtcheck=InternDailyWork::where('added_by',\Session::get('employeesession'))->where('date',$date)->first();

        $reqcheck=updateRequestIntern::where('added_by',\Session::get('employeesession'))->where('date',$date)->first();

        if($date>$current_date){

            \Session::flash('message', "Please enter a valid date for request.");
            \Session::flash('alert-class', 'alert-danger');
                
            return redirect()->back();
        }

        if($dtcheck || $reqcheck){

            \Session::flash('message', "You have already posted update for this date!");
            \Session::flash('alert-class', 'alert-danger');
                
            return redirect()->back();
        }

        if($date==$current_date){

            \Session::flash('message', "Please go to the regular update section for today's update.");
            \Session::flash('alert-class', 'alert-danger');
                
            return redirect()->back();
        }

        else{

            $task=new updateRequestIntern;

            $task->name=\Session::get('internname');
            $task->date_of_submission=$current_date;
            $task->date=$request->date;
            $task->reason=$request->reason;
            $task->body=$request->body;
            $task->type=$request->workplace;
            $task->added_by=\Session::get('internusername');


            $task->save();

            \Session::flash('message', "Request for update has been sent to admin");
             \Session::flash('alert-class', 'alert-success');

            return redirect()->back();

        }
    }

    public function changeNotificationStatusIntern(Request $request){

        $sended_to=$request->input('username');

        $chageStatus=Notification::where('sended_to',$sended_to)->update(['status'=>1]);
        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function fullViewInterNotification(){

       $getAllNotification = Notification::where('sended_to',\Session::get('internusername'))->orderBy('id','desc')->get();

        return view('intern.pages.fullViewInterNotification',compact('getAllNotification')); 
    }

    public function inboxMessageIntern(){

       $showQuery = ceoinbox::where('sended_to',\Session::get('internusername'))->get();

        return view('intern.pages.inboxMessageIntern',compact('showQuery')); 
    }

    public function internInboxAnwer(Request $request,$id=null){

        $question= ceoinbox::where('id',$id)->first();

        $answer= ceoMessageReply::where('message_id',$id)->get();

        return view('intern.pages.internInboxAnwer',compact('question','answer'));
    }

    public function saveReplyIntern(Request $request){

        $id = $request->input('id');
        $reply = $request->input('reply');
        $sended_to = $request->input('sended_to');
        $message = $request->input('message');

        // print_r($id);die();

        $update=ceoinbox::where('id',$id)->update(['status'=>1]);

        $saveReply = new ceoMessageReply;

        $saveReply->message_id=$id;
        $saveReply->sended_by=\Session::get('internusername');
        $saveReply->sended_to=$sended_to;
        $saveReply->reply=$reply;
        $saveReply->message=$message;

        $saveReply->save();

        $notfication = new Notification;

        $notfication->added_by=\Session::get('internusername');
        $notfication->sended_to=$sended_to;
        $notfication->field='Message Reply';
        $notfication->type='Inbox';
        $notfication->status='0';

        $notfication->save();

        \Session::flash('message', "Message successfully sended");
        \Session::flash('alert-class', 'alert-success');

        return back();
    }
}
