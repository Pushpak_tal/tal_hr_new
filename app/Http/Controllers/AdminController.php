<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use App\EmployeeDetails;
use App\AdminDetail;
use App\InternDetail;
use App\TempEmployeeUpdate;
use App\TempInternUpdate;
use App\EmployeeDailyWork;
use App\InternDailyWork;
use Carbon\Carbon;
use App\updateRequestEmployee;
use App\updateRequestIntern;
use App\EmpBirthday;
use App\InternBirthday;
use App\TempAmbasadorUpdate;
use App\CampusAmbasador;
use App\AssignTaskAmbasador;
use App\Notification;
use App\ceoinbox;
use App\ceoMessageReply;
use DB;
use Mail;


class AdminController extends Controller
{

	public function adminRegistration(){

		$admin=new AdminDetail;

		$admin->name="Admin";
		$admin->username = "user@gmail.com";
        $admin->mobile = "6053056890";
        $admin->email = "jon@email.com";
        $admin->designation = "Manageing director";
        $admin->password = Hash::make('pswd');
        $admin->save();
	}

	public function adminLogin(){

		return view('admin.pages.adminLogin');
	}

	public function adminloginserveice(Request $request){

		$rules = [

            'username' => 'required',
            'password' => 'required'

        ];

        $this->validate($request, $rules);

        $username = $request->input('username');
        $password = $request->input('password');

         $check_login = AdminDetail::where('username', $username)->first();

         if (!empty($check_login)) {



            $db_password = $check_login->password;


            if (Hash::check($password, $db_password)) {

                $name = $check_login->name;
                $u_name = $check_login->username;
                $mobile = $check_login->mobile;
                $admin_id = $check_login->admin_id;

                $encrypt_userid = $admin_id;

                session(['adminsession' => $u_name]);
                session(['adminname' => $name]);
                session(['adminmobile' => $mobile]);
                session(['adminidsession' => $encrypt_userid]);

            // $emp=EmployeeDetails::whereMonth('dob', '=', \Carbon\Carbon::now()->format('m'))->whereDay('dob', '=', \Carbon\Carbon::now()->format('d'))->get();

            // $count = Notification::where('created_at',\Carbon\Carbon()->now())->count();

            // if(!empty($emp)){

            //     foreach ($emp as $key => $birthday) {

            //         $notification = new Notification;

            //         $notification->added_by=\Session::get('adminsession');
            //         $notification->sended_to=$birthday->username;
            //         $notification->field='Birthday';
            //         $notification->reason='birthday';
            //         $notification->type='birthday Wish';
            //         $notification->status='0';

            //         $notification->save();
            //     }

            // }


                \Session::flash('message', "You are successfully loggedin");
                \Session::flash('alert-class', 'alert-success');

                return redirect('/adminDashboard');
            } else {

                \Session::flash('message', "You have entered wrong password");
                \Session::flash('alert-class', 'alert-danger');
                return back();
            }
        } else {

            \Session::flash('message', "You have entered wrong username");
            \Session::flash('alert-class', 'alert-danger');
            return back();
        }
	}

	public function adminLogout(){

		\Session::flush();
        \Session::flash('message', "You are log out");
        \Session::flash('alert-class', 'alert-success');
        return redirect('/admin/login')->with('flash_message_error','You are log out successfully');
	}

    public function employeeDetail(){

        $employeeDetail=EmployeeDetails::all();

        // print_r($employeeDetail);die();

        return view('admin.pages.employeeDetail',compact('employeeDetail'));
    }

    public function updateApproveStatus(Request $request){

        $employee_id=$request->input('employee_id');

        $employeeDetail=EmployeeDetails::where('employee_id',$employee_id)->update(['status'=>1]);

        $employee=EmployeeDetails::where('employee_id',$employee_id)->first();

        $name=$employee->name;
        $email=$employee->email;
        $password=str::random(9);


        $username="TAL".substr($name, 0,3).rand(1,999).rand(1,999);
        // print_r($name);die();


        $data=array('name' => "$name",'password' => "$password",'username'=>"$username");

            Mail::send('employee.pages.mail.loginDetail',$data,function($message) use ($name,$email,$password){
                             $message->to($email)->subject('Employee Detail');
            });

        $hash_password=Hash::make($password);

        $employeeDetail=EmployeeDetails::where('employee_id',$employee_id)->update(['password'=>$hash_password,'username'=>$username]);

        return back();




    }

    public function updateCancelStatus(Request $request){

        $employee_id=$request->input('employee_id');

        $employeeDetail=EmployeeDetails::where('employee_id',$employee_id)->update(['status'=>2]);

        return back()->with('flash_message_error','Employee successfully rejected!!');;
    }



    public function internDetail(){

        $internDetail=InternDetail::all();

        // print_r($employeeDetail);die();

        return view('admin.pages.internDetail',compact('internDetail'));
    }


    public function updateApproveStatusIntern(Request $request){

        $intern_id=$request->input('intern_id');

        $internDetail=InternDetail::where('intern_id',$intern_id)->update(['status'=>1]);

        $intern=InternDetail::where('intern_id',$intern_id)->first();

        $name=$intern->name;
        $email=$intern->email;
        $password=str::random(9);

        // print_r($name);die();

        $username="TAL".substr($name, 0,3).rand(1,999).rand(1,999);;


        $data=array('name' => "$name",'username' => "$username",'password' => "$password");

            Mail::send('intern.pages.mail.loginDetailIntern',$data,function($message) use ($name,$email,$password){
                             $message->to($email)->subject('Intern Detail');
            });

        $hash_password=Hash::make($password);

        $employeeDetail=InternDetail::where('intern_id',$intern_id)->update(['password'=>$hash_password,'username'=>$username]);

        return back();

             
    }

    public function updateCancelStatusIntern(Request $request){

        $intern_id=$request->input('intern_id');

        $internDetail=InternDetail::where('intern_id',$intern_id)->update(['status'=>2]);

        return back()->with('flash_message_error','Employee successfully rejected!!');;
    }

    public function employeeProfileUpdateReq(){

        $request=TempEmployeeUpdate::orderBy('temp_employee_id','desc')->get();

        // print_r($request);die();

        return view('admin.pages.tempEmployeeUpdate',compact('request'));
    }

    public function approveEmployeeProfileStatus(Request $request){

        $temp_employee_id=$request->input('temp_employee_id');

        $reason=$request->input('reason');

        $username=$request->username;

        $Detail=TempEmployeeUpdate::where('temp_employee_id',$temp_employee_id)->update(['status'=>1]);

        $data=TempEmployeeUpdate::where('temp_employee_id',$temp_employee_id)->first();

        // echo "<pre>";print_r($data);die();

        if($data->field_name=="Name"){

            $update=EmployeeDetails::where('username',$username)->update(['name'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Name";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="Email"){

            $update=EmployeeDetails::where('username',$username)->update(['email'=>$data['new_value']]);
        }

        if($data->field_name=="Phone"){

            $update=EmployeeDetails::where('username',$username)->update(['phone'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Phone";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="Join Date"){

            $update=EmployeeDetails::where('username',$username)->update(['join_date'=>$data['new_value']]);
            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Join Date";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="Image"){

            $update=EmployeeDetails::where('username',$username)->update(['image'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Image";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }


        

        \Session::flash('message', "Employee has been updated");
             \Session::flash('alert-class', 'alert-success');

        return back();
         
    }


    public function cancelEmployeeProfileStatus(Request $request){

        $temp_employee_id=$request->input('temp_employee_id');
        $reason = $request->input('reason');
        $username=$request->input('username');

        $internDetail=TempEmployeeUpdate::where('temp_employee_id',$temp_employee_id)->first();

        if($internDetail->field_name=="Name"){

            $update=TempEmployeeUpdate::where('temp_employee_id',$temp_employee_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Name";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="Email"){

            $update=TempEmployeeUpdate::where('temp_employee_id',$temp_employee_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Email";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="Phone"){

            $update=TempEmployeeUpdate::where('temp_employee_id',$temp_employee_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Phone";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="Join Date"){

            $update=TempEmployeeUpdate::where('temp_employee_id',$temp_employee_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Join Date";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="Image"){

            $update=TempEmployeeUpdate::where('temp_employee_id',$temp_employee_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Image";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        \Session::flash('message', "Employee has been rejected");
             \Session::flash('alert-class', 'alert-danger');

        return back();
    }


    public function internProfileUpdateReq(){

        $request=TempInternUpdate::orderBy('temp_intern_id','desc')->get();

        // print_r($request);die();

        return view('admin.pages.tempInternUpdate',compact('request'));
    }

    public function approveImpProfileStatus(Request $request){

        $temp_intern_id=$request->input('temp_intern_id');

        // $data=$request->all();

        // print_r($data);die();

        $username=$request->username;
        $reason = $request->input('reason');

        $Detail=TempInternUpdate::where('temp_intern_id',$temp_intern_id)->update(['status'=>1]);

        $data=TempinternUpdate::where('temp_intern_id',$temp_intern_id)->first();

        // echo "<pre>";print_r($data);die();
        

        if($data->field_name=="Name"){

            $update=InternDetail::where('username',$username)->update(['name'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Name";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="Email"){

            $update=InternDetail::where('username',$username)->update(['email'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Email";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="Phone"){

            $update=InternDetail::where('username',$username)->update(['phone'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Phone";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="Join Date"){

            $update=InternDetail::where('username',$username)->update(['join_date'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Join Date";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="Image"){

            $update=InternDetail::where('username',$username)->update(['image'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Image";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        \Session::flash('message', "Intern has been updated");
             \Session::flash('alert-class', 'alert-success');      

        return back();
    }

    public function cancelInternProfileStatus(Request $request){

        $temp_intern_id=$request->input('temp_intern_id');

        $reason = $request->input('reason');

        $username=$request->input('username');

        $internDetail=TempInternUpdate::where('temp_intern_id',$temp_intern_id)->first();

        if($internDetail->field_name=="Name"){

            $update=TempInternUpdate::where('temp_intern_id',$temp_intern_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Name";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="Email"){

            $update=TempInternUpdate::where('temp_intern_id',$temp_intern_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Email";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="Phone"){

            $update=TempInternUpdate::where('temp_intern_id',$temp_intern_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Phone";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="Join Date"){

            $update=TempInternUpdate::where('temp_intern_id',$temp_intern_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Join Date";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="Image"){

            $update=TempInternUpdate::where('temp_intern_id',$temp_intern_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Image";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        \Session::flash('message', "Employee has been rejected");
             \Session::flash('alert-class', 'alert-danger');

        return back();
    }

    public function employeeDailyUpate(){

        $empDetail=EmployeeDetails::all();

        return view('admin.pages.employeeDailyUpate',compact('empDetail'));
    }

    public function dailyUpdateFiltered(Request $request){

        // $data=$request->all();

        // print_r($data);die();

        $empDetail=EmployeeDetails::all();

        $username=$request->input('name');
        $workplace= $request->input('work_place'); 
        $date1=$request->input('startDate');
        
        $date=$request->input('endDate');

        $date2 = date($date, strtotime($date .' +1 day'));

        // print_r($date2);die();

        if($date1 > $date2){

            \Session::flash('message', "Start date should be less than end date ");
            \Session::flash('alert-class', 'alert-danger');

            return redirect()->back();
        }

        elseif($date1> \Carbon\Carbon::now()->toDateString()){

            \Session::flash('message', "Start date should be less than current date ");
            \Session::flash('alert-class', 'alert-danger');

            return redirect()->back();
        }

        else{

            switch($workplace){

                case '1':
                   $list=EmployeeDailyWork::where('added_by',$username)->where('type','1')->whereBetween('date',[$date1,$date2])->orderBy('created_at','desc')->paginate(10);

                return view('admin.pages.selectedEmployeeUpdate',compact('empDetail','list'));


                case '0':
                   $list=EmployeeDailyWork::where('added_by',$username)->where('type','0')->whereBetween('date',[$date1,$date2])->orderBy('created_at','desc')->paginate(10);

                return view('admin.pages.selectedEmployeeUpdate',compact('empDetail','list'));

                default:

                $list=EmployeeDailyWork::where('added_by',$username)->whereBetween('date',[$date1,$date2])->orderBy('created_at','desc')->paginate(10);

                return view('admin.pages.selectedEmployeeUpdate',compact('empDetail','list'));

            }

        }
    }

    public function internDailyUpdate(){

        $internDetail=InternDetail::all();

        return view('admin.pages.internDailyUpdate',compact('internDetail'));
    }

    public function interDailyUpdateFiltered(Request $request){

        $internDetail=InternDetail::all();

        $username=$request->input('name');
        $workplace= $request->input('work_place'); 
        $date1=$request->input('startDate');
        
        $date=$request->input('endDate');

        $date2 = date($date, strtotime($date .' +1 day'));

        if($date1 > $date2){

            \Session::flash('message', "Start date should be less than end date ");
            \Session::flash('alert-class', 'alert-danger');

            return redirect()->back();
        }

        elseif($date1> \Carbon\Carbon::now()->toDateString()){

            \Session::flash('message', "Start date should be less than current date ");
            \Session::flash('alert-class', 'alert-danger');

            return redirect()->back();
        }

        else{

            switch($workplace){

                case '1':
                   $list=InternDailyWork::where('added_by',$username)->where('type','1')->whereBetween('date',[$date1,$date2])->orderBy('created_at','desc')->paginate(10);

                return view('admin.pages.selectedInternUpdate',compact('internDetail','list'));


                case '0':
                   $list=InternDailyWork::where('added_by',$username)->where('type','0')->whereBetween('date',[$date1,$date2])->orderBy('created_at','desc')->paginate(10);

                return view('admin.pages.selectedInternUpdate',compact('internDetail','list'));

                default:

                $list=InternDailyWork::where('added_by',$username)->whereBetween('date',[$date1,$date2])->orderBy('created_at','desc')->paginate(10);

                return view('admin.pages.selectedInternUpdate',compact('internDetail','list'));

            }
        }


    }

    public function viewUpdateRequestEmployee(){

        $getEmployee=updateRequestEmployee::orderBy('id','desc')->get();

        return view('admin.pages.viewUpdateRequestEmployee',compact('getEmployee'));
    }

    public function approveRequestEmployee(Request $request){

        $employee_id=$request->input('id');

        $task=new EmployeeDailyWork;

        $task->name = $request->input('name');
        $task->date = $request->input('date');
        $task->body = $request->input('body');
        $task->added_by = $request->input('added_by');
        $task->type = $request->input('type');
        $task->save();

        $update_req=updateRequestEmployee::where('id',$employee_id)->update(['status'=>1]);

        $notification = new Notification;

        $notification->added_by = \Session::get('adminname');
        $notification->sended_to = $request->input('added_by');
        $notification->field = 'daily task request';
        $notification->reason = $request->input('reason');
        $notification->type = 'approved';
        $notification->status='0';

        $notification->save();


        \Session::flash('message', "Employee successfully approved");
            \Session::flash('alert-class', 'alert-success');

            return redirect()->back();
    }

    public function rejectRequestEmployee(Request $request){

        $employee_id=$request->input('id');
        $added_by=$request->input('added_by');

        $notification = new Notification;

        $notification->added_by = \Session::get('adminname');
        $notification->sended_to = $added_by;
        $notification->field = 'daily task request';
        $notification->reason = $request->input('reason');
        $notification->type = 'rejected';
        $notification->status='0';

        $notification->save();

        $update_req=updateRequestEmployee::where('id',$employee_id)->update(['status'=>2]);


        \Session::flash('message', "Employee successfully rejected");
            \Session::flash('alert-class', 'alert-danger');

            return redirect()->back();
    }

    public function viewUpdateRequestintern(){

        $getIntern=updateRequestIntern::orderBy('id','desc')->get();

        return view('admin.pages.viewUpdateRequestIntern',compact('getIntern'));
    }

    public function approveRequestIntern(Request $request){

        $intern_id=$request->input('id');

        // echo $intern_id;die();

        $task=new InternDailyWork;

        $task->name = $request->input('name');
        $task->date = $request->input('date');
        $task->body = $request->input('body');
        $task->added_by = $request->input('added_by');
        $task->type = $request->input('type');
        $task->save();

        $update_req=updateRequestIntern::where('id',$intern_id)->update(['status'=>1]);

        $notification = new Notification;

        $notification->added_by = \Session::get('adminname');
        $notification->sended_to = $request->input('added_by');
        $notification->field = 'daily task request';
        $notification->reason = $request->input('reason');
        $notification->type = 'approved';
        $notification->status='0';

        $notification->save();


        \Session::flash('message', "Intern successfully approved");
            \Session::flash('alert-class', 'alert-success');

            return redirect()->back();
    }

    public function rejectRequestIntern(Request $request){

        $intern_id=$request->input('id');

        $update_req=updateRequestIntern::where('id',$intern_id)->update(['status'=>2]);

        $notification = new Notification;

        $notification->added_by = \Session::get('adminname');
        $notification->sended_to = $request->input('added_by');
        $notification->field = 'daily task request';
        $notification->reason = $request->input('reason');
        $notification->type = 'rejected';
        $notification->status='0';

        $notification->save();


        \Session::flash('message', "Intern successfully rejected");
            \Session::flash('alert-class', 'alert-danger');

            return redirect()->back();
    }

    public function searchEmployeeDetail(Request $request){

        $data=$request->all();

        $search=EmployeeDetails::where('username','LIKE','%'.$data['username'].'%')->where('name','LIKE','%'.$data['name'].'%')->get();

        // echo "<pre>";print_r($search);die();

        return view('admin.pages.employeeDetail',compact('search'));
    }

    public function searchInternDetail(Request $request){

        $data=$request->all();

        $search=InternDetail::where('username','LIKE','%'.$data['username'].'%')->where('name','LIKE','%'.$data['name'].'%')->get();

        // echo "<pre>";print_r($search);die();

        return view('admin.pages.internDetail',compact('search'));
    }

    public function searchEmployeeDailyUpdate(Request $request){

        $data=$request->all();

        $search=updateRequestEmployee::where('added_by','LIKE','%'.$data['employee_id'].'%')->where('name','LIKE','%'.$data['name'].'%')->get();

        // echo "<pre>";print_r($search);die();

        return view('admin.pages.viewUpdateRequestEmployee',compact('search'));
    }

    public function searchInternDailyUpdate(Request $request){

        $data=$request->all();

        $search=updateRequestIntern::where('added_by','LIKE','%'.$data['intern_id'].'%')->where('name','LIKE','%'.$data['name'].'%')->get();

        // echo "<pre>";print_r($search);die();

        return view('admin.pages.viewUpdateRequestIntern',compact('search'));
    }

    public function upcomingBirthday(){

       $start = date('z') + 1 - 7;

        $end = date('z') + 1 + 7;

        $birthday = EmployeeDetails::whereRaw("DAYOFYEAR(dob) BETWEEN $start AND $end")->get();

        // echo "<pre>";print_r($users);die();


        return view('admin.pages.upcomingBirthday',compact('birthday'));
    }

    public function wishEmployee(Request $request){

        $wish=new EmpBirthday;

        $wish->emp_id=$request->emp_id;
        $wish->message=$request->message;
        $wish->added_by=\Session::get('adminname');
        $wish->dob=$request->dob;

        $wish->save();

        return back();

    }

    public function upcomingBirthdayIntern(){

        $start = date('z') + 1 - 7;

        $end = date('z') + 1 + 7;

        $birthday = InternDetail::whereRaw("DAYOFYEAR(dob) BETWEEN $start AND $end")->get();

        // echo "<pre>";print_r($users);die();

        return view('admin.pages.upcomingBirthdayIntern',compact('birthday'));
    }

    public function wishIntern(Request $request){

       $wish=new InternBirthday;

        $wish->intern_id=$request->intern_id;
        $wish->message=$request->message;
        $wish->added_by=\Session::get('adminname');
        $wish->dob=$request->dob;

        $wish->save();

        return back(); 
    }

    public function ambasadorProfileUpdateReq(){

        $getDetail=TempAmbasadorUpdate::orderBy('temp_ambasador_id','desc')->get();

        return view('admin.pages.ambasadorProfileUpdateReq',compact('getDetail'));
    }

    public function approveAmbasadorPofileStatus(Request $request){


        $temp_ambasador_id=$request->input('temp_ambasador_id');
        $reason=$request->input('reason');

        $username=$request->username;

        $Detail=TempAmbasadorUpdate::where('temp_ambasador_id',$temp_ambasador_id)->update(['status'=>1]);

        $data=TempAmbasadorUpdate::where('temp_ambasador_id',$temp_ambasador_id)->first();

        // echo "<pre>";print_r($data);die();
        

        if($data->field_name=="Name"){

            $update=CampusAmbasador::where('username',$username)->update(['name'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Name";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="Email"){

            $update=CampusAmbasador::where('username',$username)->update(['email'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Email";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="Phone"){

            $update=CampusAmbasador::where('username',$username)->update(['phone'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Phone";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="State"){

            $update=CampusAmbasador::where('username',$username)->update(['state'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="State";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="District"){

            $update=CampusAmbasador::where('username',$username)->update(['district'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="District";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="City"){

            $update=CampusAmbasador::where('username',$username)->update(['city'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="City";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="Pin"){

            $update=CampusAmbasador::where('username',$username)->update(['pin'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Pin";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }

        if($data->field_name=="Image"){

            $update=CampusAmbasador::where('username',$username)->update(['image'=>$data['new_value']]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Image";
            $notification->reason=$reason;
            $notification->type='approved';
            $notification->status='0';

            $notification->save();
        }
        

        \Session::flash('message', "Ambasador has been updated");
             \Session::flash('alert-class', 'alert-success');

        return back();
    }

    public function rejectAmbasadorPofileStatus(Request $request){

        $temp_ambasador_id=$request->input('temp_ambasador_id');

        $reason = $request->input('reason');
        $username = $request->input('username');

        $internDetail=TempAmbasadorUpdate::where('temp_ambasador_id',$temp_ambasador_id)->first();

        if($internDetail->field_name=="Name"){

            $update=TempAmbasadorUpdate::where('temp_ambasador_id',$temp_ambasador_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Name";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="Email"){

            $update=TempAmbasadorUpdate::where('temp_ambasador_id',$temp_ambasador_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Email";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="Phone"){

            $update=TempAmbasadorUpdate::where('temp_ambasador_id',$temp_ambasador_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Phone";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="State"){

            $update=TempAmbasadorUpdate::where('temp_ambasador_id',$temp_ambasador_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="State";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="District"){

            $update=TempAmbasadorUpdate::where('temp_ambasador_id',$temp_ambasador_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="District";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="City"){

            $update=TempAmbasadorUpdate::where('temp_ambasador_id',$temp_ambasador_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="City";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="Pin"){

            $update=TempAmbasadorUpdate::where('temp_ambasador_id',$temp_ambasador_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Pin";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        if($internDetail->field_name=="Image"){

            $update=TempAmbasadorUpdate::where('temp_ambasador_id',$temp_ambasador_id)->update(['status'=>2,'reason'=>$reason]);

            $notification = new Notification;

            $notification->added_by=\Session::get('adminname');
            $notification->sended_to=$username;
            $notification->field="Image";
            $notification->reason=$reason;
            $notification->type='rejected';
            $notification->status='0';

            $notification->save();
        }

        \Session::flash('message', "Employee has been rejected");
             \Session::flash('alert-class', 'alert-danger');

        return back();

        return back();
    }

    public function assignTask(){

        return view('admin.pages.assignTaskAmbasador');
    }

    public function ajax_search(Request $request){

       if(empty($request->search_data)){

        // echo "<div class='alert alert-danger'>Record not found</div>";
       }
       else{ 
        $name=CampusAmbasador::where('name','like',$request->search_data.'%')->get();
        return view('admin.pages.searchAssignTaskAmbasador',compact('name'));
       }

       if(empty($request->state_data)){

        // echo "<div class='alert alert-danger'>Record not found</div>";
       }
       else{
         $state=CampusAmbasador::where('state','like',$request->state_data.'%')->get();
        return view('admin.pages.searchAssignTaskAmbasador',compact('state'));
       }

       if(empty($request->college_data)){

       }
       else{
         $college=CampusAmbasador::where('college','like',$request->college_data.'%')->get();
         return view('admin.pages.searchAssignTaskAmbasador',compact('college'));
       }

       if(empty($request->district_data)){

       }
       else{
        $district=CampusAmbasador::where('district','like',$request->district_data.'%')->get();
        return view('admin.pages.searchAssignTaskAmbasador',compact('district'));
       }

       if(empty($request->city_data)){

       }
       else{
        $city=CampusAmbasador::where('city','like',$request->city_data.'%')->get();
        return view('admin.pages.searchAssignTaskAmbasador',compact('city'));
       }
    }

    public function getData()
   {
       $data=CampusAmbasador::all();

       return view('admin.pages.searchAssignTaskAmbasador',compact('data'));
   }

   public function assignTaskAmbasador(Request $request){

    $ambasadorUsername=implode(",",$request->username);

    return view('admin.pages.assigningTaskToAmbasador',compact('ambasadorUsername'));
   }

   public function assginTaskToAmbasador(Request $request){

    $username=$request->input('ambasador_username');

    $userArr=explode(",", $username);

    

    foreach($userArr as $username){

        $task=new AssignTaskAmbasador;

        $task->added_by=\Session::get('adminname');
        $task->ambasador_username=$username;
    
        $task->points=$request->points;
        $task->task=$request->task;
        $task->task_type=$request->task_type;
        $task->submission_date=$request->submission_date;

        $task->save();

        $getDetail=CampusAmbasador::where('username',$username)->first();

        $to_name=$getDetail->name;
        $to_email=$getDetail->email;

        $data=array('name'=>"$to_name");
                  Mail::send('admin.pages.mail.campus_ambasador_mail',$data,function($message) use ($to_name,$to_email){
                    $message->to($to_email)->subject('TAL-TMS Task');
                  });
    }


    

    \Session::flash('message', "Task assigned successfully!!");
             \Session::flash('alert-class', 'alert-success');

    return redirect("/assignTask");
   }

   public function sendMessageEmployee(){

    return view('admin.pages.sendMessageEmployee');
   }

   public function saveAdminMessageToEmployee(Request $request){

        $data= $request->all();

        $message = new ceoinbox;

        $message->sended_by=\Session::get('adminname');
        $message->sended_to=$data['employee_username'];
        $message->message=$data['message'];
        $message->status='0';

        $message->save();

        $notification = new Notification;

        $notification->added_by=\Session::get('adminname');
        $notification->sended_to=$data['employee_username'];
        $notification->field='Inbox Message';
        $notification->type='Inbox';
        $notification->status= '0';

        $notification->save();

        \Session::flash('message', "Message successfully sended");
        \Session::flash('alert-class', 'alert-success');

        return back();

   }

     public function viewMessageReplies(){

        $data = ceoinbox::where('status',1)->orderBy('id','desc')->get();

        return view('admin.pages.viewMessageReplies',compact('data'));

    }

    public function viewReplyMessageToAdmin($id){

        $message = ceoinbox::where('id',$id)->where('status',1)->first();

        $replies = ceoMessageReply::where('message_id',$id)->get();

        return view('admin.pages.viewReplyMessageToAdmin',compact('message','replies'));
    }

    public function fullViewAdminNotification(){

        $notification = Notification::where('sended_to',\Session::get('adminname'))->orderBy('id','desc')->get();

        return view('admin.pages.fullViewAdminNotification',compact('notification'));
    }

    public function sendMessageIntern(){

        return view('admin.pages.sendMessageIntern');
    }

    public function saveAdminMessageToIntern(Request $request){

       $data= $request->all();

        $message = new ceoinbox;

        $message->sended_by=\Session::get('adminname');
        $message->sended_to=$data['intern_username'];
        $message->message=$data['message'];
        $message->status='0';

        $message->save();

        $notification = new Notification;

        $notification->added_by=\Session::get('adminname');
        $notification->sended_to=$data['intern_username'];
        $notification->field='Inbox Message';
        $notification->type='Inbox';
        $notification->status= '0';

        $notification->save();

        \Session::flash('message', "Message successfully sended");
        \Session::flash('alert-class', 'alert-success');

        return back();
 
    }

    

}

