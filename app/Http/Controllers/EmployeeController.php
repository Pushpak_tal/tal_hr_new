<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\EmployeeDetails;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Str;
use App\TempEmployeeUpdate;
use App\EmployeeDailyWork;
use Carbon\Carbon;
use App\updateRequestEmployee;
use App\Notification;
use App\ceoinbox;
use App\ceoMessageReply;
use Mail;

class EmployeeController extends Controller
{

	public function employeeRegistration(){

		return view('employee.pages.employeeRegister');
	}

	public function employeeRegisterService(Request $request){

		$rules = [

                    'name' =>'required',
                    'email' =>'required|email',
                    'phone' =>'required|digits:10',
                    'dob' =>'required',
                    'join_date' =>'required',
                    'image' =>'required',
                    'id_proof' =>'required',
                    'address_proof'=>'required',
                    ];

        $this->validate($request,$rules);

        $data=$request->all();

        $email_check=EmployeeDetails::where('email',$request->email)->first();

        if(!empty($email_check)){

        	return back();
        }
        else{

        	if($request->hasfile('image')){

        		$file=$request->file('image');
		    	$filename='image'.time().'.'.$request->image->extension();
		    	$destination=storage_path('../public/upload/employee');
		    	$file->move($destination,$filename);
		    	$path1="/".$filename;
        	}

        	if($request->hasfile('id_proof')){

        		$file=$request->file('id_proof');
		    	$filename='id_proof'.time().'.'.$request->id_proof->extension();
		    	$destination=storage_path('../public/upload/employee');
		    	$file->move($destination,$filename);
		    	$path2="/".$filename;
        	}

        	if($request->hasfile('address_proof')){

        		$file=$request->file('address_proof');
		    	$filename='address_proof'.time().'.'.$request->address_proof->extension();
		    	$destination=storage_path('../public/upload/employee');
		    	$file->move($destination,$filename);
		    	$path3="/".$filename;
        	}

        	$employee=new EmployeeDetails;

        	$employee->name=$data['name'];
        	$employee->email=$data['email'];
        	$employee->phone=$data['phone'];
        	$employee->dob=$data['dob'];
        	$employee->join_date=$data['join_date'];
        	$employee->image=$path1;
        	$employee->id_proof=$path2;
        	$employee->address_proof=$path3;

        	$employee->save();

        	$to_name=$data['name'];
            $to_email=$data['email'];


            $data=array('name' => "$to_name");

            Mail::send('employee.pages.mail.employeeMail',$data,function($message) use ($to_name,$to_email){
                             $message->to($to_email)->subject('Employee Detail');
            });

              \Session::flash("message","Thank you for registering, Activation details has been sent to your e-mail, Please follow the link and complete the registration.");
              \Session::flash("status","success");


             return back();


        }

	}


    public function employeeLogin(){

        return view('employee.pages.employeeLogin');
    }

    public function employeeLoginServeice(Request $request){

        $rules = [

            'username' => 'required',
            'password' => 'required'

        ];

        $this->validate($request, $rules);

        $username = $request->input('username');
        $password = $request->input('password');


        $check_login = EmployeeDetails::where('username', $username)->first();

         if (!empty($check_login)) {



            $db_password = $check_login->password;
            $employee_id=$check_login->employee_id;


            if (Hash::check($password, $db_password)) {

                $name = $check_login->name;


                $u_name = $check_login->username;
                $employee_id = $check_login->employee_id;
                // print_r($employee_id);die();
               
                
                session(['employeeidsession'=>$employee_id]);
                session(['employeesession' => $u_name]);
                session(['employeename' => $name]);
                


                \Session::flash('message', "You are successfully loggedin");
            \Session::flash('alert-class', 'alert-success');

                return redirect('/employeeDashboard');

                
            } else {

                \Session::flash('message', "You have entered wrong password");
                \Session::flash('alert-class', 'alert-danger');
                return back();
            }
        } else {

            \Session::flash('message', "You have entered wrong username");
            \Session::flash('alert-class', 'alert-danger');
            return back();
        }
    }

    public function employeeLogout(){

        \Session::flush();
        \Session::flash('message', "You are log out");
        \Session::flash('alert-class', 'alert-success');
        return redirect('/employeeLogin');
    }

    public function employeeProfile(){

        $employee_detail=EmployeeDetails::where('employee_id',session()->get('employeeidsession'))->first();

        // print_r($employee_detail);die();

        return view('employee.pages.employeeProfile',compact('employee_detail'));
    }

    public function editTempEmployee(Request $request){

        $data=$request->all(); 

        if($data['old_name'] != $data['name']){

            $tempEmployee=new TempEmployeeUpdate;

            $tempEmployee->username=$data['username'];
            $tempEmployee->field_name='Name';
            $tempEmployee->old_value=$data['old_name'];
            $tempEmployee->new_value=$data['name'];
            $tempEmployee->status='0';
            $tempEmployee->save();
        }
        if($data['old_email'] != $data['email']){

            $tempEmployee=new TempEmployeeUpdate;

            $tempEmployee->username=$data['username'];
            $tempEmployee->field_name='Email';
            $tempEmployee->old_value=$data['old_email'];
            $tempEmployee->new_value=$data['email'];
            $tempEmployee->status='0';
            $tempEmployee->save();
        }
        if($data['old_phone'] != $data['phone']){

            $tempEmployee=new TempEmployeeUpdate;

            $tempEmployee->username=$data['username'];
            $tempEmployee->field_name='Phone';
            $tempEmployee->old_value=$data['old_phone'];
            $tempEmployee->new_value=$data['phone'];
            $tempEmployee->status='0';
            $tempEmployee->save();
        }
        if($data['old_join_date'] != $data['join_date']){

                $tempEmployee=new TempEmployeeUpdate;

                $tempEmployee->username=$data['username'];
                $tempEmployee->field_name='Join Date';
                $tempEmployee->old_value=$data['old_join_date'];
                $tempEmployee->new_value=$data['join_date'];
                $tempEmployee->status='0';
                $tempEmployee->save();            
        }

        if(isset($data['image'])){

           if($data['old_image'] != $data['image']){

            if($request->hasfile('image')){

                $file=$request->file('image');
                $filename='image'.time().'.'.$request->image->extension();
                $destination=storage_path('../public/upload/employee');
                $file->move($destination,$filename);
                $path1="/".$filename;
            }

            $tempEmployee=new TempEmployeeUpdate;
            $tempEmployee->username=$data['username'];
            $tempEmployee->field_name='Image';
            $tempEmployee->old_value=$data['old_image'];
            $tempEmployee->new_value=$path1;
            $tempEmployee->status='0';
            $tempEmployee->save();

        } 
        }
              
        return back();
    }

    public function employeeWorkDetail(Request $request){

        $date=Carbon::now()->toDateString();
        $white_ips=['103.242.190.64','10.14.205.179','125.22.76.178'];

        $date_check=EmployeeDailyWork::where('added_by',\Session::get('employeesession'))->first();

        if($date_check){

            \Session::flash('message', "You can't update twice in one day");
            \Session::flash('alert-class', 'alert-danger');

            return back();
        }

        else{

            if(!in_array($request->ip(), $white_ips)){

                $daily_work=new EmployeeDailyWork;

                $daily_work->name=\Session::get('employeename');
                $daily_work->body=$request->body;
                $daily_work->date=$date;
                $daily_work->added_by=\Session::get('employeesession');
                $daily_work->type=0;

                $daily_work->save();

                \Session::flash('message', "Update has been posted");
                \Session::flash('alert-class', 'alert-success');

                return back();

            }

            else{

                $daily_work=new EmployeeDailyWork;

                $daily_work->name=\Session::get('employeename');
                $daily_work->body=$request->body;
                $daily_work->date=$date;
                $daily_work->added_by=\Session::get('employeesession');
                $daily_work->type=1;

                $daily_work->save();

                 \Session::flash('message', "Update has been posted");
                    \Session::flash('alert-class', 'alert-success');

                return back();

            }
        }
    }

    public function viewEmployeeDailyUpdate(){

        $empUpdate=EmployeeDailyWork::all();

        return view('employee.pages.viewEmployeeDailyUpdate',compact('empUpdate'));
    }

    public function updateEmployeeDailyTask(Request $request){

        $employee_id=$request->input('id');
        $body=$request->input('body');

        $employee_update=EmployeeDailyWork::where('id',$employee_id)->update(['body'=>$body]);

        \Session::flash('message', "Your task successfully posted");
        \Session::flash('alert-class', 'alert-success');

        return back();

    }

    public function employeeRequestUpdate(){

        return view('employee.pages.employeeRequestUpdate');
    }

    public function updateRequestEmployee(Request $request){

        $rules=[

            'date'=>'required',
            'workplace'=>'required',
            'body'=>'required',
            'reason'=>'required',

        ];

        $this->validate($request,$rules);

        $current_date=Carbon::now()->toDateString();
        $date=$request->input('date');
        $whiteIps = ['103.242.190.64','10.14.205.179','125.22.76.178'];

        $dtcheck=EmployeeDailyWork::where('added_by',\Session::get('employeesession'))->where('date',$date)->first();

        $reqcheck=updateRequestEmployee::where('added_by',\Session::get('employeesession'))->where('date',$date)->first();

        if($date>$current_date){

            \Session::flash('message', "Please enter a valid date for request.");
            \Session::flash('alert-class', 'alert-danger');
                
            return redirect()->back();
        }

        if($dtcheck || $reqcheck){

            \Session::flash('message', "You have already posted update for this date!");
            \Session::flash('alert-class', 'alert-danger');
                
            return redirect()->back();
        }

        if($date==$current_date){

            \Session::flash('message', "Please go to the regular update section for today's update.");
            \Session::flash('alert-class', 'alert-danger');
                
            return redirect()->back();
        }

        else{

            $task=new updateRequestEmployee;

            $task->name=\Session::get('employeename');
            $task->date_of_submission=$current_date;
            $task->date=$request->date;
            $task->reason=$request->reason;
            $task->body=$request->body;
            $task->type=$request->workplace;
            $task->added_by=\Session::get('employeesession');


            $task->save();

            \Session::flash('message', "Request for update has been sent to admin");
             \Session::flash('alert-class', 'alert-success');

            return redirect()->back();

        }
    }

    public function changeNotificationStatus(Request $request){

        $sended_to=$request->input('username');

        $chageStatus=Notification::where('sended_to',$sended_to)->update(['status'=>1]);
        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function fullViewEmployeeTask(){

        $getAllNotification = Notification::where('sended_to',\Session::get('employeesession'))->orderBy('id','desc')->get();

        return view('employee.pages.fullViewEmployeeTask',compact('getAllNotification'));
    }

    public function inboxMessageEmployee(){

        $showQuery = ceoinbox::where('sended_to',\Session::get('employeesession'))->get();

        return view('employee.pages.inboxMessageEmployee',compact('showQuery'));
    }

    public function employeeInboxAnwer(Request $request,$id=null){

        $question= ceoinbox::where('id',$id)->first();

        $answer= ceoMessageReply::where('message_id',$id)->get();

        return view('employee.pages.employeeInboxAnwer',compact('question','answer'));
    }

    public function saveReplyEmployee(Request $request){

        $id = $request->input('id');
        $reply = $request->input('reply');
        $sended_to = $request->input('sended_to');
        $message = $request->input('message');

        // print_r($id);die();

        $update=ceoinbox::where('id',$id)->update(['status'=>1]);

        $saveReply = new ceoMessageReply;

        $saveReply->message_id=$id;
        $saveReply->sended_by=\Session::get('employeesession');
        $saveReply->sended_to=$sended_to;
        $saveReply->reply=$reply;
        $saveReply->message=$message;

        $saveReply->save();

        $notfication = new Notification;

        $notfication->added_by=\Session::get('employeesession');
        $notfication->sended_to=$sended_to;
        $notfication->field='Message Reply';
        $notfication->type='Inbox';
        $notfication->status='0';

        $notfication->save();

        \Session::flash('message', "Message successfully sended");
        \Session::flash('alert-class', 'alert-success');

        return back();
    }

}
