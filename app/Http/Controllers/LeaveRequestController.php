<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\LeaveRequestEmployee;
use App\EmployeeDetails;
use App\LeaveRequestIntern;
use App\InternDetail;
use Carbon\Carbon;
use Mail;

class LeaveRequestController extends Controller
{
    public function employeeLeaveRequest(){

    	return view('employee.pages.employeeLeaveRequest');
    }

    public function saveEmployeeLeave(Request $request){

    	$data=$request->all();

        $current_date=Carbon::now()->toDateString();

        if($data['start_date'] < $current_date){

           \Session::flash('message', "Start date should be equal or greter than current date");
             \Session::flash('alert-class', 'alert-danger'); 

             return back();
        }

        if($data['end_date'] < $current_date){

           \Session::flash('message', "End date should be greter than current date");
             \Session::flash('alert-class', 'alert-danger'); 

             return back();
        }

    	$emp=new LeaveRequestEmployee;

    	$emp->added_by=\Session::get('employeesession');
    	$emp->name=\Session::get('employeename');
    	$emp->start_date=$data['start_date'];
    	$emp->start_time=$data['start_time'];
    	$emp->end_date=$data['end_date'];
    	$emp->end_time=$data['end_time'];
    	$emp->reason=$data['reason'];
    	$emp->status="0";
    	
    	$emp->save();

    	\Session::flash('message', "Your application successfully submitted");
             \Session::flash('alert-class', 'alert-success');

         return back();

    }

    public function viewEmployeeLeaveReq(){

    	$emp_detail=LeaveRequestEmployee::all();

    	return view('admin.pages.viewEmployeeLeaveReq',compact('emp_detail'));
    }

    public function approveEmpLeaveReq(Request $request){

    	$data=$request->all();

    	$emp=EmployeeDetails::where('username',$data['added_by'])->first();

    	$email=$emp->email;

    	// print_r($email);die();

    	$status_update=LeaveRequestEmployee::where('id',$data['id'])->update(['status'=>1]);

    	$to_name=$data['name'];

        $to_email=$email;


    	$datas=array('name' => "$to_name");

            Mail::send('admin.pages.mail.approveEmpLeaveReqMail',$datas,function($message) use ($to_name,$to_email){
                             $message->to($to_email)->subject('Leave Request Approved');
            });

       \Session::flash('message', "Leave successfully approved");
             \Session::flash('alert-class', 'alert-success');

         return back();
    }

    public function rejectEmpLeaveReq(Request $request){

    	$data=$request->all();

    	$emp=EmployeeDetails::where('username',$data['added_by'])->first();

    	$email=$emp->email;

    	// print_r($email);die();

    	$status_update=LeaveRequestEmployee::where('id',$data['id'])->update(['status'=>2]);

    	$to_name=$data['name'];

        $to_email=$email;


    	$datas=array('name' => "$to_name");

            Mail::send('admin.pages.mail.rejectEmpLeaveReqMail',$datas,function($message) use ($to_name,$to_email){
                             $message->to($to_email)->subject('Leave Request Rejected');
            });

       \Session::flash('message', "Leave successfully rejected");
             \Session::flash('alert-class', 'alert-danger');

         return back();
    }

    public function internLeaveRequest(){

    	return view('intern.pages.internLeaveRequest');
    }

    public function saveInternLeave(Request $request){

    	$data=$request->all();

        $current_date=Carbon::now()->toDateString();

        if($data['start_date'] < $current_date){

           \Session::flash('message', "Start date should be equal or greter than current date");
             \Session::flash('alert-class', 'alert-danger'); 

             return back();
        }

        if($data['end_date'] < $current_date){

           \Session::flash('message', "End date should be greter than current date");
             \Session::flash('alert-class', 'alert-danger'); 

             return back();
        }

    	$emp=new LeaveRequestIntern;

    	$emp->added_by=\Session::get('internusername');
    	$emp->name=\Session::get('internname');
    	$emp->start_date=$data['start_date'];
    	$emp->start_time=$data['start_time'];
    	$emp->end_date=$data['end_date'];
    	$emp->end_time=$data['end_time'];
    	$emp->reason=$data['reason'];
    	$emp->status="0";
    	
    	$emp->save();

    	\Session::flash('message', "Your application successfully submitted");
             \Session::flash('alert-class', 'alert-success');

         return back();
    }

    public function viewInternLeaveReq(){

    	$intern_detail=LeaveRequestIntern::all();

    	return view('admin.pages.viewInternLeaveReq',compact('intern_detail'));
    }

    public function approveInternReq(Request $request){

    	$data=$request->all();

    	$emp=InternDetail::where('username',$data['added_by'])->first();

    	$email=$emp->email;

    	// print_r($email);die();

    	$status_update=LeaveRequestIntern::where('id',$data['id'])->update(['status'=>1]);

    	$to_name=$data['name'];

        $to_email=$email;


    	$datas=array('name' => "$to_name");

            Mail::send('admin.pages.mail.approveInternReqMail',$datas,function($message) use ($to_name,$to_email){
                             $message->to($to_email)->subject('Leave Request Approved');
            });

       \Session::flash('message', "Leave successfully approved");
             \Session::flash('alert-class', 'alert-success');

         return back();
    }

    public function rejectInternReq(Request $request){

    	$data=$request->all();

    	$emp=InternDetail::where('username',$data['added_by'])->first();

    	$email=$emp->email;

    	// print_r($email);die();

    	$status_update=LeaveRequestIntern::where('id',$data['id'])->update(['status'=>2]);

    	$to_name=$data['name'];

        $to_email=$email;


    	$datas=array('name' => "$to_name");

            Mail::send('admin.pages.mail.rejectInternLeaveReqMail',$datas,function($message) use ($to_name,$to_email){
                             $message->to($to_email)->subject('Leave Request Rejected');
            });

       \Session::flash('message', "Leave successfully rejected");
             \Session::flash('alert-class', 'alert-danger');

         return back();
    }
}
