<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\AppraisalQuestions;
use App\AppraisalQuestionsIntern;
use App\EmployeeDetails;
use App\InternDetail;
use App\DateReminder;
use App\DateReminderIntern;
use App\AppraisalAnswersEmployee;
use App\AppreaisalTrackEmployee;
use App\AppraisalAnswersIntern;
use App\AppreaisalTrackIntern;
use Carbon\Carbon;
use Mail;

class AppraisalController extends Controller
{
    public function appraisalEmployee(){

        $get_employee=EmployeeDetails::all(); 	

    	return view('admin.pages.appraisalEmployee',compact('get_employee'));
    }

    public function appraisalQuestions(){

    	$question_detail=AppraisalQuestions::all();

    	return view('admin.pages.appraisalQuestions',compact('question_detail'));
    }

    public function addQuestions(Request $request){

    	$data=$request->all();

    	// print_r($data);die();

    	$rules = [

                    'question' =>'required',
                    'answer_type' =>'required',
                                                            
                    ]; 

    	$this->validate($request,$rules);

    	$post_ques=new AppraisalQuestions;

    	$post_ques->question_id='QA'.rand(1,999).rand(1,999);
    	$post_ques->question=$request->question;
    	$post_ques->answer_type=$request->answer_type;
    	$post_ques->active_status='1';
    	$post_ques->added_by=\Session::get('adminname');

    	$post_ques->save();

         \Session::flash('message', "Question successfully submitted");
             \Session::flash('alert-class', 'alert-success');

    	return back();
    }

    public function editQuestions(Request $request){


    	$id=$request->input('id');
    	$question=$request->input('question');
    	$answer_type=$request->input('answer_type');

    	$question_data= AppraisalQuestions::where('id',$id)->update(array('question' =>"$question",'answer_type'=>"$answer_type"));

        \Session::flash('message', "Question successfully updated");
             \Session::flash('alert-class', 'alert-success');

    	return back();
    }

    public function deleteEmployeeQuestion(Request $request){

    	$id=$request->input('id');

    	$delete=AppraisalQuestions::where('id',$id)->delete();

        \Session::flash('message', "Question successfully deleted");
             \Session::flash('alert-class', 'alert-danger');

    	return back();
    }

    public function appraisalIntern(){

        $internDetail=InternDetail::all();

        return view('admin.pages.appraisalIntern',compact('internDetail'));
    }

    public function appraisalInternQuestions(){

        $question_detail=AppraisalQuestionsIntern::all();

        return view('admin.pages.appraisalQuestionsIntern',compact('question_detail'));
    }

    public function addQuestionsIntern(Request $request){

        $rules = [

                    'question' =>'required',
                    'answer_type' =>'required',
                                                            
                    ]; 

        $this->validate($request,$rules);

        $post_ques=new AppraisalQuestionsIntern;

        $post_ques->question_id='QA'.rand(1,999).rand(1,999);
        $post_ques->question=$request->question;
        $post_ques->answer_type=$request->answer_type;
        $post_ques->active_status='1';
        $post_ques->added_by=\Session::get('adminname');

        $post_ques->save();

        \Session::flash('message', "Question successfully submitted");
             \Session::flash('alert-class', 'alert-success');

        return back();
    }

    public function editQuestionsIntern(Request $request){

        $id=$request->input('id');
        $question=$request->input('question');
        $answer_type=$request->input('answer_type');

        $question_data= AppraisalQuestionsIntern::where('id',$id)->update(array('question' =>"$question",'answer_type'=>"$answer_type"));

        \Session::flash('message', "Question successfully edited");
             \Session::flash('alert-class', 'alert-success');

        return back();
    }

    public function deleteInternQuestion(Request $request){

       $id=$request->input('id');

        $delete=AppraisalQuestionsIntern::where('id',$id)->delete();

        \Session::flash('message', "Question successfully deleted");
             \Session::flash('alert-class', 'alert-danger');

        return back(); 
    }

    public function assignDateToEmployee(Request $request){

        $rules = [

                    'startDate' =>'required',
                    'endDate' =>'required',
                                                            
                    ];

             $this->validate($request,$rules);


        $date1=$request->input('startDate');
        $date2=$request->input('endDate');

        if($date1 >= $date2)

        {
            \Session::flash('message', "start date less than enddate");
             \Session::flash('alert-class', 'alert-danger');

            return back();
        }

        else{

           $assign_date=new DateReminder;

            $assign_date->startDate=$request->startDate;
            $assign_date->endDate=$request->endDate;
            $assign_date->added_by=\Session::get('adminname');
            
            \Session::flash('message', "Date successfully assigned");
             \Session::flash('alert-class', 'alert-success');

            $assign_date->save();


            $emp_emails=EmployeeDetails::all();

            foreach($emp_emails as $mail){

                $to_name=$mail->name;
                $to_email=$mail->email;

                $data=array('name'=>"$to_name",'date1'=>"$date1",'date2'=>"$date2");
                  Mail::send('admin.pages.mail.emp_appraisal_mail',$data,function($message) use ($to_name,$to_email){
                    $message->to($to_email)->subject('TAL-TMS performance appraisal');
                  });
            }

            \Session::flash('message', "Reminder set sucessfully");
                \Session::flash('alert-class', 'alert-success');
                return back();
        }

        
    }

    public function assignDateToIntern(Request $request){

        $rules = [

                    'startDate' =>'required',
                    'endDate' =>'required',
                                                            
                    ];

             $this->validate($request,$rules);


        $date1=$request->input('startDate');
        $date2=$request->input('endDate');

        if($date1 >= $date2)

        {
            \Session::flash('message', "start date less than enddate");
             \Session::flash('alert-class', 'alert-danger');

            return back();
        }

        else{

           $assign_date=new DateReminderIntern;

            $assign_date->startDate=$request->startDate;
            $assign_date->endDate=$request->endDate;
            $assign_date->added_by=\Session::get('adminname');
            
            \Session::flash('message', "Date successfully assigned");
             \Session::flash('alert-class', 'alert-success');

            $assign_date->save();


            $emp_emails=InternDetail::all();

            foreach($emp_emails as $mail){

                $to_name=$mail->name;
                $to_email=$mail->email;

                $data=array('name'=>"$to_name",'date1'=>"$date1",'date2'=>"$date2");
                  Mail::send('admin.pages.mail.intern_appraisal_mail',$data,function($message) use ($to_name,$to_email){
                    $message->to($to_email)->subject('TAL-TMS performance appraisal');
                  });
            }

            \Session::flash('message', "Reminder set sucessfully");
                \Session::flash('alert-class', 'alert-success');
                return back();
        }

        
    }

    public function employeePerformanceAppraisal(){

        $dateLimit=DateReminder::orderBy('created_at','desc')->first();

        // print_r($dateLimit);die();

        $employee_id=\Session::get('employeeidsession');

        $empDetail=EmployeeDetails::where('employee_id',$employee_id)->first();

        $questions=AppraisalQuestions::where('active_status',"1")->get();

        $appraisalAnswer=AppraisalAnswersEmployee::where('added_by',\Session::get('employeesession'))->whereBetween('date_of_submission',[$dateLimit->startDate,$dateLimit->endDate])->first();

        // print_r($appraisalAnswer);die();

        return view('employee.pages.employeePerformanceAppraisal',compact('empDetail','questions','appraisalAnswer','dateLimit'));
    }

    public function appraisalEmployeeAnswerSave(Request $request){

        $track=new AppreaisalTrackEmployee;
        $track->emp_id=\Session::get('employeesession');
        $track->submission_date=Carbon::now()->toDateString();

        $track->save();

        $question_array=$request->input('question');
        $answer_array=$request->input('answer');

        // print_r($answer_array);die();

        $len=count($question_array);

        // echo $len;die();

        for($i=$len-1;$i>=0;$i--){



            $appraisal_data= new AppraisalAnswersEmployee;
        
        $appraisal_data->question=$question_array[$i];
        $appraisal_data->answer=$answer_array[$i];
        $appraisal_data->date_of_submission=Carbon::now()->toDateString();
        $appraisal_data->review_start_date=$request->input('startDate');
        $appraisal_data->review_end_date=$request->input('endDate');
        $appraisal_data->location=$request->input('location');
        $appraisal_data->added_by=\Session::get('employeesession');
        $appraisal_data->save();

        }

        \Session::flash('message', "Appraisal form has been submitted.");
        \Session::flash('alert-class', 'alert-success');
        return back();
    }

    public function internPerformanceAppraisal(){

        $dateLimit=DateReminderIntern::orderBy('created_at','desc')->first();

        $intern_id=\Session::get('internidsession');

        $internDetail=InternDetail::where('intern_id',$intern_id)->first();

        $questions=AppraisalQuestionsIntern::where('active_status',"1")->get();

        $appraisalAnswer=AppraisalAnswersIntern::where('added_by',\Session::get('internusername'))->whereBetween('date_of_submission',[$dateLimit->startDate,$dateLimit->endDate])->first();

        return view('intern.pages.internPerformanceAppraisal',compact('dateLimit','internDetail','questions','appraisalAnswer'));
    }

    public function appraisalInternAnswerSave(Request $request){

        $track=new AppreaisalTrackIntern;
        $track->intern_id=\Session::get('internusername');
        $track->submission_date=Carbon::now()->toDateString();

        $track->save();

        $question_array=$request->input('question');
        $answer_array=$request->input('answer');

        // print_r($answer_array);die();

        $len=count($question_array);

        // echo $len;die();

        for($i=$len-1;$i>=0;$i--){



            $appraisal_data= new AppraisalAnswersIntern;
        
        $appraisal_data->question=$question_array[$i];
        $appraisal_data->answer=$answer_array[$i];
        $appraisal_data->date_of_submission=Carbon::now()->toDateString();
        $appraisal_data->review_start_date=$request->input('startDate');
        $appraisal_data->review_end_date=$request->input('endDate');
        $appraisal_data->location=$request->input('location');
        $appraisal_data->added_by=\Session::get('internusername');
        $appraisal_data->save();

        }

        \Session::flash('message', "Appraisal form has been submitted.");
        \Session::flash('alert-class', 'alert-success');
        return back();


    }

    public function employeeAppraisalList(Request $request){

        // $data=$request->all();

        // print_r($data);die();

        $date1=$request->input('startDate');
        $date=$request->input('endDate');

        $date2 = date($date, strtotime($date .' +1 day'));

        $emp_id=$request->input('emp_id');


        if($date1>$date2){

            \Session::flash('message', "Start date should be less than end date");
            \Session::flash('alert-class', 'alert-danger');
            return back();
        }

        $data=AppreaisalTrackEmployee::where('emp_id',$emp_id)->whereBetween('submission_date',[$date1,$date2])->orderBy('created_at','desc')->paginate(10);

        // echo "<pre>";print_r($data);die();

        return view('admin.pages.employeeAppraisalList',compact('data'));


    }

    public function employeeAppraisalFullView(Request $request){

        $emp_id=$request->input('emp_id');
        $date=$request->input('submission_date');

        $empDetail=EmployeeDetails::where('username',$emp_id)->get();

        // echo "<pre>"; print_r($empDetail);die();

        $answers=AppraisalAnswersEmployee::where('date_of_submission',$date)->where('added_by',$emp_id)->get();

        $answersLocation=AppraisalAnswersEmployee::where('date_of_submission',$date)->where('added_by',$emp_id)->pluck('location')->first();

        $answersSubmissionDate=AppraisalAnswersEmployee::where('date_of_submission',$date)->where('added_by',$emp_id)->pluck('date_of_submission')->first();



        $answersSubStartDate=AppraisalAnswersEmployee::where('date_of_submission',$date)->where('added_by',$emp_id)->pluck('review_start_date')->first();

        // echo "<pre>"; print_r($answersSubStartDate);die();

        $answersSubEndDate=AppraisalAnswersEmployee::where('date_of_submission',$date)->where('added_by',$emp_id)->pluck('review_end_date')->first();

        return view('admin.pages.employeeAppraisalFullView',compact('empDetail','answers','answersLocation','answersSubmissionDate','answersSubStartDate','answersSubEndDate'));
    }

    public function internAppraisalList(Request $request){

        $inernId=$request->input('internId');

        // echo $inernId;die();

        $date1=$request->input('startDate');
        $date=$request->input('endDate');

        $date2 = date($date, strtotime($date .' +1 day'));

        if($date1>$date2){

            \Session::flash('message', "Start date should be less than end date");
            \Session::flash('alert-class', 'alert-danger');
            return back();
        }


        $data=AppreaisalTrackIntern::where('intern_id',$inernId)->whereBetween('submission_date',[$date1,$date2])->orderBy('created_at','desc')->paginate(10);

        // echo "<pre>";print_r($data);die();

        return view('admin.pages.internAppraisalList',compact('data'));


    }

    public function internAppraisalFullView(Request $request){

        $intern_id=$request->input('intern_id');

        $date=$request->input('submission_date');


        $internDetail=EmployeeDetails::where('username',$intern_id)->get();

        // echo "<pre>"; print_r($empDetail);die();

        $answers=AppraisalAnswersIntern::where('date_of_submission',$date)->where('added_by',$intern_id)->get();

        $answersLocation=AppraisalAnswersIntern::where('date_of_submission',$date)->where('added_by',$intern_id)->pluck('location')->first();

        $answersSubmissionDate=AppraisalAnswersIntern::where('date_of_submission',$date)->where('added_by',$intern_id)->pluck('date_of_submission')->first();



        $answersSubStartDate=AppraisalAnswersIntern::where('date_of_submission',$date)->where('added_by',$intern_id)->pluck('review_start_date')->first();

        // echo "<pre>"; print_r($answersSubStartDate);die();

        $answersSubEndDate=AppraisalAnswersIntern::where('date_of_submission',$date)->where('added_by',$intern_id)->pluck('review_end_date')->first();

        return view('admin.pages.internAppraisalFullView',compact('internDetail','answers','answersLocation','answersSubmissionDate','answersSubStartDate','answersSubEndDate'));
    }

    public function viewEmployeeAppraisal(){

        $emp_id=\Session::get('employeesession');

        // print_r($emp_id);die();

        $data=AppreaisalTrackEmployee::where('emp_id',$emp_id)->orderBy('created_at','desc')->paginate(10);



        // echo "<pre>";print_r($data);die();

        return view('employee.pages.employeeAppraisalList',compact('data'));
    }

    public function appraisalFullViewEmployee(Request $request){

        $emp_id=$request->input('emp_id');
        $date=$request->input('submission_date');

        $empDetail=EmployeeDetails::where('username',$emp_id)->get();

        // echo "<pre>"; print_r($empDetail);die();

        $answers=AppraisalAnswersEmployee::where('date_of_submission',$date)->where('added_by',$emp_id)->get();

        $answersLocation=AppraisalAnswersEmployee::where('date_of_submission',$date)->where('added_by',$emp_id)->pluck('location')->first();

        $answersSubmissionDate=AppraisalAnswersEmployee::where('date_of_submission',$date)->where('added_by',$emp_id)->pluck('date_of_submission')->first();



        $answersSubStartDate=AppraisalAnswersEmployee::where('date_of_submission',$date)->where('added_by',$emp_id)->pluck('review_start_date')->first();

        // echo "<pre>"; print_r($answersSubStartDate);die();

        $answersSubEndDate=AppraisalAnswersEmployee::where('date_of_submission',$date)->where('added_by',$emp_id)->pluck('review_end_date')->first();

        return view('employee.pages.employeeAppraisalFullView',compact('empDetail','answers','answersLocation','answersSubmissionDate','answersSubStartDate','answersSubEndDate'));
    }

    public function viewAppraisalIntern(){

        $intern_id=\Session::get('internusername');

        // print_r($emp_id);die();

        $data=AppreaisalTrackIntern::where('intern_id',$intern_id)->orderBy('created_at','desc')->paginate(10);

        // echo "<pre>";print_r($data);die();

        return view('intern.pages.viewAppraisalIntern',compact('data'));
    }

    public function internAppraisalView(Request $request){

        $intern_id=$request->input('intern_id');

        $date=$request->input('submission_date');


        $internDetail=InternDetail::where('username',$intern_id)->get();

        // echo "<pre>"; print_r($internDetail);die();

        $answers=AppraisalAnswersIntern::where('date_of_submission',$date)->where('added_by',$intern_id)->get();

        $answersLocation=AppraisalAnswersIntern::where('date_of_submission',$date)->where('added_by',$intern_id)->pluck('location')->first();

        $answersSubmissionDate=AppraisalAnswersIntern::where('date_of_submission',$date)->where('added_by',$intern_id)->pluck('date_of_submission')->first();



        $answersSubStartDate=AppraisalAnswersIntern::where('date_of_submission',$date)->where('added_by',$intern_id)->pluck('review_start_date')->first();

        // echo "<pre>"; print_r($answersSubStartDate);die();

        $answersSubEndDate=AppraisalAnswersIntern::where('date_of_submission',$date)->where('added_by',$intern_id)->pluck('review_end_date')->first();

        return view('intern.pages.internAppraisalFullView',compact('internDetail','answers','answersLocation','answersSubmissionDate','answersSubStartDate','answersSubEndDate'));
    }

}
