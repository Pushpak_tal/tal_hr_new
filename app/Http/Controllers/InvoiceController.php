<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Invoice;
use App\Customer;
use PDF;

class InvoiceController extends Controller
{
    public function saveInvoice(Request $request){

    	$data=$request->all();

    	$rules=[

    		'customer_id'=>'required',
    		'sac'=>'required',
    		'invoice_date'=>'required',
    		'rate'=>'required',
    		'qty'=>'required',
    		'total'=>'required',
    		'discount'=>'required',
    		'gst_type'=>'required',
    		'gst_percentage'=>'required',
    		'subtotal'=>'required',
    		'amt'=>'required',
    		'due'=>'required',
    		'description'=>'required',

    	];

    	$this->validate($request,$rules);

    	$d_o_i=date_create($request->input('invoice_date'));
    	$date=date_format($d_o_i,"dmy");
    	$inv_no="TAL/".$date."/".mt_rand(1,9999);

    	// print_r($inv_no);die();

    	$total_amt=$data['rate'] * $data['qty'];

    	// print_r($total_amt);die();

    	$amt_discounted= ($total_amt * $data['discount'])/100;

    	$after_discount_amt= $total_amt - $amt_discounted;

    	$gst_amt= ($after_discount_amt * $data['gst_percentage'])/100;

    	$after_gst_amt= $after_discount_amt + $gst_amt;

    	// print_r($after_gst_amt);die();

    	$invoice_copy_checking=Invoice::where('invoice_number',$inv_no)->first();

    	if(empty($invoice_copy_checking)){

    		$invoice=new Invoice;

    		$invoice->invoice_number=$inv_no;
    		$invoice->date=$data['invoice_date'];
    		$invoice->client_id=$data['customer_id'];
    		$invoice->description=$data['description'];
    		$invoice->sac=$data['sac'];
    		$invoice->qty=$data['qty'];
    		$invoice->rate=$data['rate'];
    		$invoice->total=$data['total'];
    		$invoice->amt=$data['amt'];
    		$invoice->sub_total=$data['subtotal'];
    		$invoice->due=$data['due'];

    		if($data['gst_type']=="0"){

    			$invoice->cgst  = $data['gst_percentage'] / 2;
                $invoice->sgst  = $data['gst_percentage'] / 2;
                $invoice->igst  = '0';
    		}else{

    			$invoice->igst  = $data['gst_percentage'];
                $invoice->cgst  = '0';
                $invoice->sgst  = '0';
    		}

    		$invoice->gst_amt=$data['gst_amt'];
    		$invoice->discount_amt=$amt_discounted;
    		$invoice->discount_precentage=$data['discount'];


    		$invoice->save();

    		\Session::flash('message', "Invoice has been created"); 
            \Session::flash('alert-class', 'alert-success'); 

             return back();  


    	}

    }

    public function invoiceList(){

    	$get_invoice=Invoice::all();

    	return view('admin.pages.viewInvoice',compact('get_invoice'));
    }

    public function invoicePdf($id){

    	$data=Invoice::find($id);

    	$pdf = PDF::loadView('admin.pages.pdf.invoice_pdf',compact('data'));
      	return $pdf->stream('invoice.pdf');
    }

    public function editInvoice($id){

    	$data=Invoice::find($id);

        $customer_id=Customer::where('id',$data->client_id)->first();

        // print_r($customer_id);die();


        return view('admin.pages.editInvoice',compact('data','customer_id'));


    }

    public function updateInvoice(Request $request){

        $invoiceId=$request->input('id');

        $data=$request->all();

        $total_amt=$data['rate'] * $data['qty'];

        // print_r($total_amt);die();

        $amt_discounted= ($total_amt * $data['discount'])/100;

        $after_discount_amt= $total_amt - $amt_discounted;

        $gst_amt= ($after_discount_amt * $data['gst_percentage'])/100;

        $after_gst_amt= $after_discount_amt + $gst_amt;

        $get=Invoice::find($invoiceId);

            $get->client_id=$data['customer_id'];
            $get->description=$data['description'];
            $get->sac=$data['sac'];
            $get->qty=$data['qty'];
            $get->rate=$data['rate'];
            $get->total=$data['total'];
            $get->amt=$data['amt'];
            $get->sub_total=$data['subtotal'];
            $get->due=$data['due'];
            $get->gst_percentage=$data['gst_percentage'];
            if($data['gst_type']=="0"){

                $get->cgst  = $data['gst_percentage'] / 2;
                $get->sgst  = $data['gst_percentage'] / 2;
                $get->igst  = '0';
            }else{

                $get->igst  = $data['gst_percentage'];
                $get->cgst  = '0';
                $get->sgst  = '0';
            }

            $get->gst_amt=$data['gst_amt'];
            $get->discount_amt=$amt_discounted;
            $get->discount_precentage=$data['discount'];


            $get->update();

            \Session::flash('message', "Invoice has been updated"); 
            \Session::flash('alert-class', 'alert-success'); 

             return back();

    }

    public function searchInvoice(Request $request){

        $data=$request->all();

        $search=Invoice::where('invoice_number','LIKE','%'.$data['invoice_number'].'%')->get();

        return view('admin.pages.viewInvoice',compact('search'));
    }
}
