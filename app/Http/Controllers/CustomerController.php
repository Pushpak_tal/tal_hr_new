<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    public function invoice(){

    	$getCustomer=Customer::all();

    	return view('admin.pages.createInvoice',compact('getCustomer'));
    }

    public function addCustomer(Request $request){

    	$data=$request->all();

    	$customer=new Customer;
    	$customer->name=$data['name'];
    	$customer->business_name=$data['business_name'];
    	$customer->phone=$data['phone'];
    	$customer->address=$data['address'];
    	$customer->emailid=$data['email'];
    	$customer->gstin=$data['gstin'];

    	$customer->save();

    	\Session::flash('message', "Customer successfully added!");
             \Session::flash('alert-class', 'alert-success');

    	return back();
    }
}
