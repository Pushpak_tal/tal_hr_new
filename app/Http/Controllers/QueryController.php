<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Query;
use App\QueryAnswer;
use DB;

class QueryController extends Controller
{
    public function adminConcern(){

    	return view('admin.pages.adminConcern');
    }

    public function saveAdminConcern(Request $request){

    	$data=$request->all();

    	$query=new Query;

    	$query->name=\Session::get('adminname');
    	$query->user_id=\Session::get('adminsession');
        $query->added_by=\Session::get('adminsession');
    	$query->title=$data['title'];
    	$query->body=$data['body'];
    	$query->tag=$data['tag'];
        $query->team='Admin';
    	$query->save();

    	 \Session::flash('message', "Query successfully added");
             \Session::flash('alert-class', 'alert-success');

             return back();
    }
    public function adminQuerySolution(){

    	 $showQuery=Query::orderBy('id','desc')->take(10)->get();


    	return view('admin.pages.adminQuerySolution',compact('showQuery'));
    }

    public function queryAnswer($id){

        $question=Query::find($id);
        $answer=QueryAnswer::where('query_id',$id)->get();

        return view('admin.pages.queryAnswer',compact('question','answer'));
    }

    public function saveAnswerAdmin(Request $request){

        $data=$request->all();

        $answer=new QueryAnswer;

        $answer->query_id=$data['query_id'];
        $answer->added_by=$data['added_by'];
        $answer->answer=$data['answer'];

        $answer->save();

        \Session::flash('message', "answer successfully added");
             \Session::flash('alert-class', 'alert-success');

             return back();
    }

    public function employeeConcern(){

        return view('employee.pages.employeeConcern');
    }

    public function saveEmployeeConcern(Request $request){

        $data=$request->all();

        $query=new Query;

        $query->name=\Session::get('employeename');
        $query->user_id=\Session::get('employeesession');
        $query->added_by=\Session::get('employeesession');
        $query->title=$data['title'];
        $query->body=$data['body'];
        $query->tag=$data['tag'];
        $query->team='Employee';

        $query->save();

         \Session::flash('message', "Query successfully added");
             \Session::flash('alert-class', 'alert-success');

             return back(); 
    }

    public function employeeQuerySolution(){

        $showQuery=Query::orderBy('id','desc')->take(10)->get();

        return view('employee.pages.employeeQuerySolution',compact('showQuery'));
    }

    public function queryAnswerEmployee($id){

        $question=Query::find($id);
        $answer=QueryAnswer::where('query_id',$id)->get();

        return view('employee.pages.queryAnswerEmployee',compact('question','answer')); 
    }

    public function saveAnswerEmployee(Request $request){

        $data=$request->all();

        $answer=new QueryAnswer;

        $answer->query_id=$data['query_id'];
        $answer->added_by=$data['added_by'];
        $answer->answer=$data['answer'];

        $answer->save();

        \Session::flash('message', "answer successfully added");
             \Session::flash('alert-class', 'alert-success');

             return back(); 
    }

    public function internConcern(){

        return view('intern.pages.internConcern');
    }

    public function saveInternConcern(Request $request){

        $data=$request->all();

        $query=new Query;

        $query->name=\Session::get('internname');
        $query->user_id=\Session::get('internusername');
        $query->added_by=\Session::get('internusername');
        $query->title=$data['title'];
        $query->body=$data['body'];
        $query->tag=implode(',', $data['tag']);
        $query->team='Intern';

        $query->save();

         \Session::flash('message', "Query successfully added");
             \Session::flash('alert-class', 'alert-success');

             return back();
    }

    public function internQuerySolution(Request $request){

        $showQuery=Query::orderBy('id','desc')->take(10)->get();

        return view('intern.pages.internQuerySolution',compact('showQuery')); 
    }

    public function queryAnswerIntern($id){

        $question=Query::find($id);
        $answer=QueryAnswer::where('query_id',$id)->get();

        return view('intern.pages.queryAnswerIntern',compact('question','answer'));
    }

    public function saveAnswerIntern(Request $request){

        $data=$request->all();

        $answer=new QueryAnswer;

        $answer->query_id=$data['query_id'];
        $answer->added_by=$data['added_by'];
        $answer->answer=$data['answer'];

        $answer->save();

        \Session::flash('message', "answer successfully added");
             \Session::flash('alert-class', 'alert-success');

             return back();
    }

    public function searchQuery(Request $request){

        $query=$request->input('query');

        // print_r($query);die();

        $querySearch=Query::where('name','LIKE','%'.$query.'%')->orWhere('title','LIKE','%'.$query.'%')->orWhere('body','LIKE','%'.$query.'%')->orWhere('tag','LIKE','%'.$query.'%')->get();

       
        // echo "<pre>";print_r($showQuery);die();

        return view('admin.pages.adminQuerySolution',compact('querySearch'));
    }

    public function searchQueryEmployee(Request $request){

        $query=$request->input('query');

        // print_r($query);die();

        $querySearch=Query::where('name','LIKE','%'.$query.'%')->orWhere('title','LIKE','%'.$query.'%')->orWhere('body','LIKE','%'.$query.'%')->orWhere('tag','LIKE','%'.$query.'%')->get();

        // echo "<pre>";print_r($query);die();

        return view('employee.pages.employeeQuerySolution',compact('querySearch'));
    }

    public function searchQueryIntern(Request $request){

        $query=$request->input('query');

        // print_r($query);die();

        $querySearch=Query::where('name','LIKE','%'.$query.'%')->orWhere('title','LIKE','%'.$query.'%')->orWhere('body','LIKE','%'.$query.'%')->orWhere('tag','LIKE','%'.$query.'%')->get();

        // echo "<pre>";print_r($query);die();

        return view('intern.pages.internQuerySolution',compact('querySearch'));
    }


}
