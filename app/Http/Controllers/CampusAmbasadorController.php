<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CampusAmbasador;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use App\TempAmbasadorUpdate;
use App\AssignTaskAmbasador;
use App\AmbasadorTaskStatus;
use App\Notification;
use Mail;

class CampusAmbasadorController extends Controller
{
    public function campusAmbasadorRegistration(){

    	return view('campusAmbasador.pages.campusAmbasadorRegistration');
    }

    public function ambasadorRegisterService(Request $request){

    	$rules=[

    		'name'=>'required',
    		'email'=>'required',
    		'phone'=>'required',
    		'state'=>'required',
    		'district'=>'required',
    		'city'=>'required',
    		'pin'=>'required',
    		'image'=>'required',
    		'id_proof'=>'required',
    		'address_proof'=>'required'
    	];

    	$this->validate($request,$rules);

    	$data=$request->all();

    	$check_email=CampusAmbasador::where('email',$data['email'])->first();
    	$check_phone=CampusAmbasador::where('phone',$data['phone'])->first();

    	if(!empty($check_email)){

    		\Session::flash('message', "Email already exists");
             \Session::flash('alert-class', 'alert-danger');

    		return back();
    	}
    	if(!empty($check_phone)){

    		\Session::flash('message', "Phone Number already exists");
             \Session::flash('alert-class', 'alert-danger');

    		return back();
    	}

    	else{

    		if($request->hasfile('image')){

        		$file=$request->file('image');
		    	$filename='image'.time().'.'.$request->image->extension();
		    	$destination=storage_path('../public/upload/ambasador');
		    	$file->move($destination,$filename);
		    	$path1="/".$filename;
        	}

        	if($request->hasfile('id_proof')){

        		$file=$request->file('id_proof');
		    	$filename='id_proof'.time().'.'.$request->id_proof->extension();
		    	$destination=storage_path('../public/upload/ambasador');
		    	$file->move($destination,$filename);
		    	$path2="/".$filename;
        	}

        	if($request->hasfile('address_proof')){

        		$file=$request->file('address_proof');
		    	$filename='address_proof'.time().'.'.$request->address_proof->extension();
		    	$destination=storage_path('../public/upload/ambasador');
		    	$file->move($destination,$filename);
		    	$path3="/".$filename;
        	}

        	$ambasador=new CampusAmbasador;

        	$ambasador->name=$data['name'];
        	$ambasador->email=$data['email'];
        	$ambasador->college=$data['college'];
        	$ambasador->phone=$data['phone'];
        	$ambasador->state=$data['state'];
        	$ambasador->district=$data['district'];

        	$ambasador->city=$data['city'];
        	$ambasador->pin=$data['pin'];
        	$ambasador->phone=$data['phone'];
        	$ambasador->state=$data['state'];
        	$ambasador->district=$data['district'];


        	$ambasador->image=$path1;
        	$ambasador->id_proof=$path2;
        	$ambasador->address_proof=$path3;
        	$ambasador->status=0;

        	$ambasador->save();

        	\Session::flash('message', "Your login detail will send you after verification");
             \Session::flash('alert-class', 'alert-success');

             return back();
    	}
    }

    public function ambasadorDetails(){

    	$ambasador_detail=CampusAmbasador::all();

    	return view('admin.pages.ambasadorDetails',compact('ambasador_detail'));
    }

    public function approveStatusAmbasador(Request $request){

    	$id=$request->input('id');
    	$email=$request->input('email');
    	$name=$request->input('name');

    	$username="TAL".substr($name, 0,3).rand(1,999).rand(1,999);
    	$password=str::random(9);

    	$data=array('name' => "$name",'password' => "$password",'username'=>"$username");

            Mail::send('campusAmbasador.pages.mail.ambasadorLoginDetail',$data,function($message) use ($name,$email,$password){
                             $message->to($email)->subject('Campus Ambasador Login Details');
            });

         $password=Hash::make($password);


    	$updateStatus=CampusAmbasador::where('id',$id)->update(['username'=>$username,'password'=>$password,'status'=>1]);

    		\Session::flash('message', "Campus Ambasador successfully approved");
             \Session::flash('alert-class', 'alert-success');

             return back();

    }

    public function rejectStatusAmbasador(Request $request){

    	$id=$request->input('id');
    	$email=$request->input('email');
    	$name=$request->input('name');
    	$reason=$request->input('reason');

    	$data=array('name' => "$name",'reason'=>"$reason");

            Mail::send('campusAmbasador.pages.mail.ambasadorReject',$data,function($message) use ($name,$reason,$email){
                             $message->to($email)->subject('Campus Ambasador registration rejected');
            });

         $updateStatus=CampusAmbasador::where('id',$id)->update(['status'=>2]);

    		\Session::flash('message', "Campus Ambasador rejected successfully");
             \Session::flash('alert-class', 'alert-danger');

             return back();
    }

    public function campusAmbasadorLogin(){

        return view('campusAmbasador.pages.campusAmbasadorLogin');
    }

    public function ambasadorLoginServeice(Request $request){

        $rules=[

            'username'=>'required',
            'password'=>'required'
        ];

        $this->validate($request,$rules);

        $username=$request->input('username');
        $password=$request->input('password');

        $check_login=CampusAmbasador::where('username',$username)->first();

        if(!empty($check_login)){

            $db_password=$check_login->password;

            if(Hash::check($password,$db_password)){

                $u_name=$check_login->username;
                $name=$check_login->name;
                $user_image=$check_login->image;

                session(['ambasadorsession'=>$u_name]);
                session(['ambasadorname'=>$name]);
                session(['imagesession'=>$user_image]);

                \Session::flash('message', "successfully log in");
             \Session::flash('alert-class', 'alert-danger');

                return redirect('/campusAmbasadorDashboard');
            }
        }
    }

    public function campusAmbasadorDashboard(){

        return view('campusAmbasador.pages.campusAmbasadorDashboard');
    }

    public function ambasadorLogout(){

        \Session::flush();

        \Session::flash('message', "Successfully Log out");
        \Session::flash('alert-class', 'alert-danger');

        return redirect('/campusAmbasadorLogin');
    }

    public function ambasadorProfile(){

        $username=\Session::get('ambasadorsession');

        $getDetail=CampusAmbasador::where('username',$username)->first();

        return view('campusAmbasador.pages.ambasadorProfile',compact('getDetail'));
    }

    public function tempSaveAmbasador(Request $request){

        $data=$request->all();

        if($data['old_name'] != $data['name']){

            $tempEmployee=new TempAmbasadorUpdate;

            $tempEmployee->username=$data['username'];
            $tempEmployee->field_name='Name';
            $tempEmployee->old_value=$data['old_name'];
            $tempEmployee->new_value=$data['name'];
            $tempEmployee->status='0';
            $tempEmployee->save();
        }
        if($data['old_email'] != $data['email']){

            $tempEmployee=new TempAmbasadorUpdate;

            $tempEmployee->username=$data['username'];
            $tempEmployee->field_name='Email';
            $tempEmployee->old_value=$data['old_email'];
            $tempEmployee->new_value=$data['email'];
            $tempEmployee->status='0';
            $tempEmployee->save();
        }
        if($data['old_phone'] != $data['phone']){

            $tempEmployee=new TempAmbasadorUpdate;

            $tempEmployee->username=$data['username'];
            $tempEmployee->field_name='Phone';
            $tempEmployee->old_value=$data['old_phone'];
            $tempEmployee->new_value=$data['phone'];
            $tempEmployee->status='0';
            $tempEmployee->save();
        }
        if($data['old_state'] != $data['state']){

                $tempEmployee=new TempAmbasadorUpdate;

                $tempEmployee->username=$data['username'];
                $tempEmployee->field_name='State';
                $tempEmployee->old_value=$data['old_state'];
                $tempEmployee->new_value=$data['state'];
                $tempEmployee->status='0';
                $tempEmployee->save();            
        }
        if($data['old_district'] != $data['district']){

                $tempEmployee=new TempAmbasadorUpdate;

                $tempEmployee->username=$data['username'];
                $tempEmployee->field_name='District';
                $tempEmployee->old_value=$data['old_district'];
                $tempEmployee->new_value=$data['district'];
                $tempEmployee->status='0';
                $tempEmployee->save();            
        }
        if($data['old_city'] != $data['city']){

                $tempEmployee=new TempAmbasadorUpdate;

                $tempEmployee->username=$data['username'];
                $tempEmployee->field_name='City';
                $tempEmployee->old_value=$data['old_city'];
                $tempEmployee->new_value=$data['city'];
                $tempEmployee->status='0';
                $tempEmployee->save();            
        }
        if($data['old_pin'] != $data['pin']){

                $tempEmployee=new TempAmbasadorUpdate;

                $tempEmployee->username=$data['username'];
                $tempEmployee->field_name='Pin';
                $tempEmployee->old_value=$data['old_pin'];
                $tempEmployee->new_value=$data['pin'];
                $tempEmployee->status='0';
                $tempEmployee->save();            
        }

        if(isset($data['image'])){

           if($data['old_image'] != $data['image']){

            if($request->hasfile('image')){

                $file=$request->file('image');
                $filename='image'.time().'.'.$request->image->extension();
                $destination=storage_path('../public/upload/ambasador');
                $file->move($destination,$filename);
                $path1="/".$filename;
            }

            $tempEmployee=new TempAmbasadorUpdate;
            $tempEmployee->username=$data['username'];
            $tempEmployee->field_name='Image';
            $tempEmployee->old_value=$data['old_image'];
            $tempEmployee->new_value=$path1;
            $tempEmployee->status='0';
            $tempEmployee->save();

        } 
        }

        return back();


    }

    public function fullViewAmbasadorTask(){

        return view("campusAmbasador.pages.fullViewAmbasadorTask");
    }

    public function ambasadorTaskList(){

        $username=\Session::get('ambasadorsession');

        $getTask=AssignTaskAmbasador::where('ambasador_username',$username)->orderBy('id','desc')->get();

        return view("campusAmbasador.pages.ambasadorTaskList",compact('getTask')); 
    }

    public function ambasadorUpdateStatus(Request $request){

        $id=$request->input('task_id');
        $data=$request->all();

        $getDetail=AssignTaskAmbasador::where('id',$id)->first();

        $username=$getDetail->ambasador_username;

        // echo $username;die();


        if($request->hasfile('attachment')){

                $file=$request->file('attachment');
                $filename='attachment'.time().'.'.$request->attachment->extension();
                $destination=storage_path('../public/upload/ambasador');
                $file->move($destination,$filename);
                $path1="/".$filename;
            }

        else{
            $path1="Null";
        }

        $ambasador=new AmbasadorTaskStatus;

        $ambasador->task_id=$data['task_id'];
        $ambasador->username=$username;
        $ambasador->task_status=$data['task_status'];
        $ambasador->comment=$data['comment'];
        $ambasador->attachment=$path1;
        $ambasador->save();
       

        \Session::flash('message', "Your task status successfully updated");
        \Session::flash('alert-class', 'alert-success');

        return back();
    }

    public function changeNotificationStatusAmbasador(Request $request){

        $sended_to=$request->input('username');

        $chageStatus=Notification::where('sended_to',$sended_to)->update(['status'=>1]);
        return response()->json(['success'=>'Status changed successfully.']);
    }

    public function fullViewAmbasadorNotification(){

        $getAllNotification = Notification::where('sended_to',\Session::get('ambasadorsession'))->orderBy('id','desc')->get();

        return view('campusAmbasador.pages.fullViewAmbasadorNotification',compact('getAllNotification'));
    }
}
