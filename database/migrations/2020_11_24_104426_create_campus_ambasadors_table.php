<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCampusAmbasadorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('campus_ambasadors', function (Blueprint $table) {
            $table->id();
            $table->string('name',255);
            $table->string('college',255);
            $table->string('email',255);

            $table->string('username',255)->nullable();
            $table->string('password',255)->nullable();

            $table->string('phone',255);
            $table->string('state',255);
            $table->string('district',255);
            $table->string('city',255);
            $table->string('pin',255);
            $table->string('image',255);
            $table->string('id_proof',255);
            $table->string('address_proof',255);
            $table->tinyInteger('status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('campus_ambasadors');
    }
}
