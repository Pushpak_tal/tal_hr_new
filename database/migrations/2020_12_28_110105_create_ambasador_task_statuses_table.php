<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAmbasadorTaskStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ambasador_task_statuses', function (Blueprint $table) {
            $table->id();
            $table->string('task_id',255);
            $table->string('username',255);
            $table->string('task_status',255);
            $table->string('comment',1000)->nullable();
            $table->string('attachment',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ambasador_task_statuses');
    }
}
