<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUpdateRequestEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('update_request_employees', function (Blueprint $table) {
            $table->id();
            $table->string('name',255);
            $table->date('date_of_submission',255);
            $table->date('date',255);
            $table->string('body',255);
            $table->string('reason',255);
            $table->string('added_by',255);
            $table->integer('type');
            $table->string('status',255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('update_request_employees');
    }
}
