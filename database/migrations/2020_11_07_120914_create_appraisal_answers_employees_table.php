<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppraisalAnswersEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_answers_employees', function (Blueprint $table) {
            $table->id();
            $table->string('question',255);
            $table->string('location',255);
            $table->string('answer',255);
            $table->date('date_of_submission');
            $table->date('review_start_date');
            $table->string('review_end_date',255);
            $table->string('added_by',255);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_answers_employees');
    }
}
