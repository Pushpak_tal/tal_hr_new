<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAssignTaskAmbasadorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_task_ambasadors', function (Blueprint $table) {
            $table->id();
            $table->string('added_by',255);
            $table->string('ambasador_username',255);
            $table->string('points',255);
            $table->string('task',2000);
            $table->string('task_type',100);
            $table->date('submission_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('assign_task_ambasadors');
    }
}
