<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTempInternUpdatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('temp_intern_updates', function (Blueprint $table) {
            $table->bigIncrements('temp_intern_id');
            $table->string('username', 500);
            $table->string('field_name', 500);
            $table->string('old_value',500);
            $table->string('new_value');
            $table->string('reason',1000)->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('temp_intern_updates');
    }
}
