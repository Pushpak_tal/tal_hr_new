<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('intern_details', function (Blueprint $table) {
            $table->bigIncrements('intern_id');
            $table->string('name', 500);
            $table->string('email', 500);
            $table->string('username', 255)->nullable();
            $table->string('password', 255)->nullable();
            $table->bigInteger('phone');
            $table->date('dob');
            $table->date('join_date',255);
            $table->string('image',255)->nullable();
            $table->string('id_proof',255);
            $table->string('address_proof',255);
            $table->tinyInteger('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('intern_details');
    }
}
