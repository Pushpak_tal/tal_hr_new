<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoices', function (Blueprint $table) {
            $table->id();
            $table->string('invoice_number',255);
            $table->date('date');
            $table->string('client_id',255);
            $table->string('description',255);
            $table->string('sac',255);
            $table->string('qty',255);
            $table->string('rate',255);
            $table->string('total',255);
            $table->string('amt',255);
            $table->string('sub_total',255);
            $table->string('due',255);
            $table->string('igst',255);
            $table->integer('gst_percentage');
            $table->string('cgst',255);
            $table->string('sgst',255);
            $table->string('gst_amt',255);
            $table->string('discount_amt')->nullable();
            $table->string('discount_precentage')->nullable();


            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoices');
    }
}
