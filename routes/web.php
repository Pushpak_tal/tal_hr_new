<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



// Admin Registration

Route::get('/admin/registration','AdminController@adminRegistration');

Route::get('/admin/login',function(){
  if(session()->has('adminsession')) {
        return redirect('/adminDashboard');
    }
    else {
        return view('admin.pages.adminLogin');
    }
});

Route::post('/adminloginserveice','AdminController@adminloginserveice');

Route::get('/adminLogout','AdminController@adminLogout');



Route::group(['middleware' => ['AdminMiddleware']], function () {

	Route::get('/adminDashboard', function () {
    return view('admin.layouts.birthdayhome');
});

Route::get('/employeeDetail','AdminController@employeeDetail');

Route::post('/approve/status/','AdminController@updateApproveStatus');

Route::post('/cancel/status/','AdminController@updateCancelStatus');

Route::get('/internDetail','AdminController@internDetail');

Route::post('/approve/status/intern','AdminController@updateApproveStatusIntern');

Route::post('/cancel/status/intern','AdminController@updateCancelStatusIntern');

Route::get('/employeeProfileUpdateReq','AdminController@employeeProfileUpdateReq')->name('tempUpdate.profileReq');

Route::post('/approve/employeePofile/status','AdminController@approveEmployeeProfileStatus');

Route::post('/cancel/employeePofile/status','AdminController@cancelEmployeeProfileStatus');


Route::get('/internProfileUpdateReq','AdminController@internProfileUpdateReq');

Route::post('/approve/internPofile/status','AdminController@approveImpProfileStatus');

Route::post('/cancel/internPofile/status','AdminController@cancelInternProfileStatus');

Route::get('/employeeDailyUpate','AdminController@employeeDailyUpate');

Route::get('/dailyUpdateFiltered','AdminController@dailyUpdateFiltered');

Route::get('/internDailyUpdate','AdminController@internDailyUpdate');

Route::get('/interDailyUpdateFiltered','AdminController@interDailyUpdateFiltered');

Route::get('/viewUpdateRequestEmployee','AdminController@viewUpdateRequestEmployee');

Route::post('/approve/RequestEmployee/','AdminController@approveRequestEmployee');

Route::post('/reject/RequestEmployee/','AdminController@rejectRequestEmployee');

Route::get('/viewUpdateRequestintern','AdminController@viewUpdateRequestintern');

Route::post('/approve/RequestIntern/','AdminController@approveRequestIntern');

Route::post('/reject/RequestIntern/','AdminController@rejectRequestIntern');

Route::get('/appraisalEmployee','AppraisalController@appraisalEmployee');

Route::get('/appraisalQuestions','AppraisalController@appraisalQuestions');

Route::post('/addQuestions','AppraisalController@addQuestions');

Route::post('/editQuestions','AppraisalController@editQuestions');

Route::post('/deleteEmployeeQuestion','AppraisalController@deleteEmployeeQuestion');

Route::get('/appraisalIntern','AppraisalController@appraisalIntern');

Route::get('/appraisalInternQuestions','AppraisalController@appraisalInternQuestions');

Route::post('/addQuestionsIntern','AppraisalController@addQuestionsIntern');

Route::post('/editQuestionsIntern','AppraisalController@editQuestionsIntern');

Route::post('/deleteInternQuestion','AppraisalController@deleteInternQuestion');

Route::post('/assignDateToEmployee','AppraisalController@assignDateToEmployee');

Route::post('/assignDateToIntern','AppraisalController@assignDateToIntern');

Route::post('/employee/appraisal/list','AppraisalController@employeeAppraisalList');

Route::post('/employee/appraisal/fullView','AppraisalController@employeeAppraisalFullView');

Route::post('/intern/appraisal/list','AppraisalController@internAppraisalList');

Route::post('/intern/appraisal/fullView','AppraisalController@internAppraisalFullView');

Route::get('/invoice','CustomerController@invoice');

Route::post('/addCustomer','CustomerController@addCustomer');

Route::post('/saveInvoice','InvoiceController@saveInvoice');

Route::get('/invoiceList','InvoiceController@invoiceList');

Route::get('/invoice/pdf/{id}','InvoiceController@invoicePdf');

Route::post('/editInvoice','InvoiceController@editInvoice');

Route::post('/searchEmployeeDetail','AdminController@searchEmployeeDetail');

Route::post('/searchInternDetail','AdminController@searchInternDetail');

Route::post('/searchEmployeeDailyUpdate','AdminController@searchEmployeeDailyUpdate');

Route::post('/searchInternDailyUpdate','AdminController@searchInternDailyUpdate');

Route::get('/edit/invoice/{id}','InvoiceController@editInvoice');

Route::post('/updateInvoice','InvoiceController@updateInvoice');

Route::post('/searchInvoice','InvoiceController@searchInvoice');

Route::get('/adminConcern','QueryController@adminConcern');

Route::post('/saveAdminConcern','QueryController@saveAdminConcern');

Route::get('/adminQuerySolution','QueryController@adminQuerySolution');

Route::get('/queryAnswer/{id}','QueryController@queryAnswer');

Route::post('/saveAnswerAdmin','QueryController@saveAnswerAdmin');

Route::post('/searchQuery','QueryController@searchQuery');

Route::get('/upcomingBirthday','AdminController@upcomingBirthday');

Route::post('/wishEmployee','AdminController@wishEmployee');

Route::get('/upcomingBirthdayIntern','AdminController@upcomingBirthdayIntern');

Route::post('/wishIntern','AdminController@wishIntern');

Route::get('/viewEmployeeLeaveReq','LeaveRequestController@viewEmployeeLeaveReq');

Route::post('/approveEmpLeaveReq','LeaveRequestController@approveEmpLeaveReq');

Route::post('/rejectEmpLeaveReq','LeaveRequestController@rejectEmpLeaveReq');

Route::get('/viewInternLeaveReq','LeaveRequestController@viewInternLeaveReq');

Route::post('/approveInternReq','LeaveRequestController@approveInternReq');

Route::post('/rejectInternReq','LeaveRequestController@rejectInternReq');

Route::get('/ambasadorDetails','CampusAmbasadorController@ambasadorDetails');

Route::post('/approveStatusAmbasador','CampusAmbasadorController@approveStatusAmbasador');

Route::post('/rejectStatusAmbasador','CampusAmbasadorController@rejectStatusAmbasador');

Route::get('/ambasadorProfileUpdateReq','AdminController@ambasadorProfileUpdateReq');

Route::post('/approve/ambasadorPofile/status','AdminController@approveAmbasadorPofileStatus');

Route::post('/cancel/ambasadorPofile/status','AdminController@rejectAmbasadorPofileStatus');

Route::get('/assignTask','AdminController@assignTask');
Route::post('/search', 'AdminController@ajax_search')->name('search');

Route::post('/get','AdminController@getData')->name('get');

Route::post('/assignTaskAmbasador','AdminController@assignTaskAmbasador');

Route::post('/assginTaskToAmbasador','AdminController@assginTaskToAmbasador');

Route::get('/sendMessageEmployee' , 'AdminController@sendMessageEmployee');

Route::post('/saveAdminMessageToEmployee','AdminController@saveAdminMessageToEmployee');

Route::get('/viewMessageReplies','AdminController@viewMessageReplies');

Route::get('/viewReplyMessageToAdmin/{id}','AdminController@viewReplyMessageToAdmin');

Route::get('/fullViewAdminNotification','AdminController@fullViewAdminNotification');

Route::get('/sendMessageIntern','AdminController@sendMessageIntern');

Route::post('/saveAdminMessageToIntern','AdminController@saveAdminMessageToIntern');

});


Route::get('/employeeLogin', function () {
    if(session()->has('employeesession')) {
        return redirect('/employeeDashboard');
    }
    else {
        return view('employee.pages.employeeLogin');
    }
});


Route::get('/employee/registration','EmployeeController@employeeRegistration');

Route::post('/employeeRegisterService','EmployeeController@employeeRegisterService');

Route::get('/employeeLogout','EmployeeController@employeeLogout');



Route::post('/employeeLoginServeice','EmployeeController@employeeLoginServeice');

Route::group(['middleware' => ['EmployeeMiddleware']], function () {

	

Route::get('/employeeDashboard', function () {
    return view('employee.layouts.addWorkDashboard');

});

Route::get('/employeeProfile','EmployeeController@employeeProfile')->name('employee.profile');

Route::post('/editTempEmployee','EmployeeController@editTempEmployee')->name('edit.tempemployee');

Route::post('/employeeWorkDetail','EmployeeController@employeeWorkDetail');

Route::get('/viewEmployeeDailyUpdate','EmployeeController@viewEmployeeDailyUpdate');

Route::post('/updateEmployeeDailyTask','EmployeeController@updateEmployeeDailyTask');

Route::get('/employeeRequestUpdate','EmployeeController@employeeRequestUpdate');

Route::post('/updateRequestEmployee','EmployeeController@updateRequestEmployee');

Route::get('/employeePerformanceAppraisal','AppraisalController@employeePerformanceAppraisal');

Route::post('/appraisalEmployeeAnswerSave','AppraisalController@appraisalEmployeeAnswerSave');

Route::get('/viewEmployeeAppraisal','AppraisalController@viewEmployeeAppraisal');

Route::post('/appraisalFullViewEmployee','AppraisalController@appraisalFullViewEmployee');

Route::get('/employeeConcern','QueryController@employeeConcern');

Route::post('/saveEmployeeConcern','QueryController@saveEmployeeConcern');

Route::get('/employeeQuerySolution','QueryController@employeeQuerySolution');

Route::get('/queryAnswer/employee/{id}','QueryController@queryAnswerEmployee');

Route::post('/saveAnswerEmployee','QueryController@saveAnswerEmployee');

Route::post('/searchQueryEmployee','QueryController@searchQueryEmployee');

Route::get('/employeeLeaveRequest','LeaveRequestController@employeeLeaveRequest');

Route::post('/saveEmployeeLeave','LeaveRequestController@saveEmployeeLeave');

Route::post('/changeNotificationStatus','EmployeeController@changeNotificationStatus')->name('changeNotificationStatus');

Route::get('/fullViewEmployeeTask','EmployeeController@fullViewEmployeeTask');

Route::get('/inboxMessageEmployee','EmployeeController@inboxMessageEmployee');

Route::get('/employeeInboxAnwer/{id}','EmployeeController@employeeInboxAnwer');

Route::post('/saveReplyEmployee','EmployeeController@saveReplyEmployee');

});



Route::get('/intern/registration','InternController@internRegistration')->name('intern.registration');

Route::post('/internRegisterService','InternController@internRegisterService');

Route::post('/internLoginServeice','InternController@internLoginServeice');

Route::get('/internLogin', function () {
    if(session()->has('internusername')) {
        return redirect('/internDashboard');
    }
    else {
        return view('intern.pages.internLogin');
    }
});



Route::group(['middleware' => ['InternMiddleware']], function () {


Route::get('/internDashboard', function () {
    return view('intern.layouts.addWorkDashboardIntern');

});

Route::get('/internProfile','InternController@internProfile');

Route::post('/editTempIntern','InternController@editTempintern')->name('edit.tempintern');

Route::post('/internWorkDetail','InternController@internWorkDetail');

Route::get('/viewInternDailyUpdate','InternController@viewInternDailyUpdate');

Route::post('/updateInternDailyTask','InternController@updateInternDailyTask');

Route::get('/internRequestUpdate','InternController@internRequestUpdate');

Route::post('/updateRequestIntern','InternController@updateRequestIntern');

Route::get('/internPerformanceAppraisal','AppraisalController@internPerformanceAppraisal');

Route::post('/appraisalInternAnswerSave','AppraisalController@appraisalInternAnswerSave');

Route::get('/viewAppraisalIntern','AppraisalController@viewAppraisalIntern');

Route::post('/internAppraisalView','AppraisalController@internAppraisalView');

Route::get('/internConcern','QueryController@internConcern');

Route::post('/saveInternConcern','QueryController@saveInternConcern');

Route::get('/internQuerySolution','QueryController@internQuerySolution');

Route::get('/queryAnswer/intern/{id}','QueryController@queryAnswerIntern');

Route::post('/saveAnswerIntern','QueryController@saveAnswerIntern');

Route::post('/searchQueryIntern','QueryController@searchQueryIntern');

Route::get('/internLeaveRequest','LeaveRequestController@internLeaveRequest');

Route::post('/saveInternLeave','LeaveRequestController@saveInternLeave');

Route::post('/changeNotificationStatusIntern','InternController@changeNotificationStatusIntern')->name('changeNotificationStatusIntern');

Route::get('/fullViewInterNotification','InternController@fullViewInterNotification');

Route::get('/inboxMessageIntern','InternController@inboxMessageIntern');

Route::get('/internInboxAnwer/{id}','InternController@internInboxAnwer');

Route::post('/saveReplyIntern','InternController@saveReplyIntern');


});

Route::get('/internLogout','InternController@internLogout');

//Campus ambasador routes

Route::get('/campusAmbasadorRegistration','CampusAmbasadorController@campusAmbasadorRegistration');

Route::post('/ambasadorRegisterService','CampusAmbasadorController@ambasadorRegisterService');

Route::post('/ambasadorLoginServeice','CampusAmbasadorController@ambasadorLoginServeice');

Route::get('/campusAmbasadorLogin', function () {
    if(session()->has('ambasadorsession')) {
        return redirect('/campusAmbasadorDashboard');
    }
    else {
        return view('campusAmbasador.pages.campusAmbasadorLogin');
    }
});



Route::group(['middleware' => ['CampusAmbasadorMiddleware']], function () {

   Route::get('/campusAmbasadorDashboard','CampusAmbasadorController@campusAmbasadorDashboard'); 

   Route::get('/ambasadorProfile','CampusAmbasadorController@ambasadorProfile');

   Route::post('/tempSaveAmbasador','CampusAmbasadorController@tempSaveAmbasador');

   Route::get('/fullViewAmbasadorTask','CampusAmbasadorController@fullViewAmbasadorTask');

   Route::get('/ambasadorTaskList','CampusAmbasadorController@ambasadorTaskList');

   Route::post('/ambasadorUpdateStatus','CampusAmbasadorController@ambasadorUpdateStatus');

   Route::post('/changeNotificationStatusAmbasador','CampusAmbasadorController@changeNotificationStatusAmbasador');

   Route::get('/fullViewAmbasadorNotification','CampusAmbasadorController@fullViewAmbasadorNotification');
});
Route::get('/ambasadorLogout','CampusAmbasadorController@ambasadorLogout');
