<!DOCTYPE html>
<html lang="en">
<head>
  

  <title>TAL | Dashboard</title>
  <link rel="icon" type="image/png" href="./upload/logo.png"/>

  @include('intern.includes.headerlinks')

  
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">


  @include('intern.includes.navbar')
  @include('intern.includes.sidebar')
  @yield('content')
  @include('intern.includes.rightsidebar')
 
 @include('intern.includes.footerlinks')
 @include('intern.includes.footer')

  
</div>

<script>
        $(document).ready(function(){

    $(document).on("click", "#notification", function() { 
		$.ajax({
			url: '/changeNotificationStatusIntern',
			type: "POST",
			dataType: "json",
			data:{
                _token:'{{ csrf_token() }}',
				username: $('#username').val(),
			},
			success: function(dataResult){
                $('#output').load(document.URL +  ' #output');;
			},
			error: function (dataResult) {
                 alert(dataResult);
           }
		});
	}); 
});

    </script>



</body>
</html>
