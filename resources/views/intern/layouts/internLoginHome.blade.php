<!DOCTYPE html>
<html lang="en">
<head>
  

  <title>TAL | Dashboard </title>
  <link rel="icon" type="image/png" href="./upload/logo.png"/>

  @include('intern.includes.headerlinks')

  
</head>
<body class="hold-transition login-page">

  @yield('content')

<script>
	const submitBtn = document.getElementById('submit-btn');

const validate = (e) => {
  e.preventDefault();
  const username = document.getElementById('name');
  const emailAddress = document.getElementById('email');
  const phone = document.getElementById('phone');
  const dob = document.getElementById('dob');
  const join = document.getElementById('join_date');
  const image = document.getElementById('image');
   const id_proof = document.getElementById('id_proof');
    const address_proof = document.getElementById('address_proof');
  if (username.value === "") {
    alert("Please enter your name.");
    username.focus();
    return false;
  }
  if (emailAddress.value === "") {
    alert("Please enter your email address.");
    emailAddress.focus();
    return false;
  }
  if (phone.value === "") {
    alert("Please enter your phone number.");
    phone.focus();
    return false;
  }
  if (dob.value === "") {
    alert("Please enter Date of birth.");
    dob.focus();
    return false;
  }
  if (join.value === "") {
    alert("Please enter Date of birth.");
    join.focus();
    return false;
  }
  if (image.value === "") {
    alert("Please select image.");
    image.focus();
    return false;
  }

  if (id_proof.value === "") {
    alert("Please enter Date of birth.");
    id_proof.focus();
    return false;
  }

  if (address_proof.value === "") {
    alert("Please enter Date of birth.");
    address_proof.focus();
    return false;
  }
  
  return true;
}

submitBtn.addEventListener('click', validate);
</script>


</body>
</html>
