@extends('intern.layouts.home')
@section('content')

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->

     <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">CEO Inbox</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-12 order-2 order-md-1">
              <div class="row">


                
              </div>
              <div class="row">
                <div class="col-12">
                    <div class="post">
                     
                      <h3>
                        {{$question->message}}
                      </h3>

                    </div>

                   
                </div>
              </div>
            </div>
            
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>

    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Reply</h3>

         
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
              <div class="row">
                
              </div>
              <div class="row">
                <div class="col-12">

                	@foreach($answer as $item)
                  
                    <div class="post">
                      <div class="user-block">
                        
                       
                      </div>
                      <!-- /.user-block -->
                      <p>
                        {{$item->reply}}
                      </p>

                      <p>
                        
                      </p>
                    </div>

                    @endforeach
                   
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>

    </section>

    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Your Reply</h3>

         
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-12 order-2 order-md-1">
              <div class="row">
                
              </div>
              <div class="row">
                <div class="col-12">

                	<form method="post" action="{{url('/saveReplyIntern')}}">
                		<div class="post">

                			@csrf

                			<input type="hidden" name="id" value="{{$question->id}}">
                			<input type="hidden" name="sended_to" value="{{$question->sended_by}}">
                			<input type="hidden" name="message" value="{{$question->message}}">
                      
                      		<label>Submit your reply regarding this message</label>
                     		 <textarea class="form-control" name="reply" cols="10" rows="10" required></textarea><br>

                     		 <button type="submit" class="btn btn-primary">Send</button>
                   
                		</div>
                	</form>
                  
                    
              </div>
            </div>
            
          </div>
        </div>
      </div>

    </section>
</div>

@endsection