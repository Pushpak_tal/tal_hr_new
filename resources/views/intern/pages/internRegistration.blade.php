@extends('intern.layouts.internLoginHome')
@section('content')

<div class="register-box">
  <div class="register-logo">
    <a href="../../index2.html"><b>Think Again</b> Lab</a>
  </div>

  <div class="card">
    <div class="card-body register-card-body">
      <p class="login-box-msg">Register new intern</p>

       @if(Session::has('message'))
      <p class="alert alert-{{ Session::get('status') }}">{{ Session::get('message') }}</p>
       @endif

      <form action="{{url('/internRegisterService')}}" method="post" enctype="multipart/form-data">
      	@csrf
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="name" id="name" placeholder="Name" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-user"></span>
            </div>
          </div>
        </div>
        <p style="color:red;">{{ $errors->first('name') }}</p>

        <div class="input-group mb-3">
          <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <p style="color:red;">{{ $errors->first('email') }}</p>

        <div class="input-group mb-3">
          <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" required>
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <p style="color:red;">{{ $errors->first('phone') }}</p>
        <!-- <div class="input-group mb-3">
          <input type="password" class="form-control" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div> -->

        <div class="input-group mb-3">
        	<label>Date of Birth</label>
          <input type="date" class="form-control" name="dob" id="dob" style="margin-top: 8%;margin-left: -28%;" required>
        </div>
        <p style="color:red;">{{ $errors->first('dob') }}</p>

        <div class="input-group mb-3">
        	<label>Date of Join</label>
          <input type="date" class="form-control" name="join_date" id="join" style="margin-top: 8%;margin-left: -28%;" required>
        </div>
        <p style="color:red;">{{ $errors->first('join_date') }}</p>
       
       <div class="input-group mb-3">
        	<label>Photo</label><br><br>
          <input type="file" class="form-control" name="image" id="image" style="margin-top: 8%;margin-left: -13%;" required>
        </div>
        <p style="color:red;">{{ $errors->first('image') }}</p>

        <div class="input-group mb-3">
        	<label>Id Proof</label><br><br>
          <input type="file" class="form-control" name="id_proof" id="id_proof" style="margin-top: 8%;margin-left: -17%;" required>
        </div>
        <p style="color:red;">{{ $errors->first('id_proof') }}</p>

        <div class="input-group mb-3">
        	<label>Photo</label><br><br>
          <input type="file" class="form-control" name="address_proof" id="address_proof" style="margin-top: 8%;margin-left: -12%;" required>
        </div>
        <p style="color:red;">{{ $errors->first('address_proof') }}</p>

        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="terms" name="terms" value="agree" required>
              <label for="agreeTerms">
               Signed all <a href="#">documents</a>
              </label>
            </div>
          </div>
          <p style="color:red;">{{ $errors->first('terms') }}</p>
          <!-- /.col -->
          <div class="col-4">
            <button type="submit" class="btn btn-primary btn-block">Register</button>
          </div>
          <!-- /.col -->
        </div>
      </form>

  

    </div>
    <!-- /.form-box -->
  </div><!-- /.card -->
</div>
@endsection