@extends('intern.layouts.home')
@section('content')

<div class="content-wrapper">
   
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12" style="margin-top: 3%;">

          	 @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            <!-- general form elements -->
            <div class="card card-primary">

              <div class="card-header">
                <h3 class="card-title">Fill your leave application form</h3>
              </div>
             
              <form role="form" method="post" action="{{url('/saveInternLeave')}}">
              	@csrf

                <div class="card-body">
                  <div class="form-group">

                  	<label>Start Date</label>
                  	<input type="date" name="start_date" class="form-control" required>
                   
                  </div>

                  <div class="form-group">

                  	<label>Start Time</label>
                  	<input type="time" name="start_time" class="form-control" required>
                   
                  </div>

                  <div class="form-group">

                  	<label>End Date</label>
                  	<input type="date" name="end_date" class="form-control" required>
                   
                  </div>

                  <div class="form-group">

                  	<label>End Time</label>
                  	<input type="time" name="end_time" class="form-control" required>
                   
                  </div>

                  <div class="form-group">

                  	<label>Resaon for leave</label>

                  	<textarea name="reason" class="form-control" required></textarea>
                   
                  </div>
                 

               
                  <button type="submit" class="btn btn-primary">Submit</button>
                
              </form>
            </div>
        
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection