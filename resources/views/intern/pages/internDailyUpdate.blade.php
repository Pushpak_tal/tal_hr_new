@extends('intern.layouts.home')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Intern Table</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Intern</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

          
            
            <div class="card">
              <div class="card-header">

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>#</th>
                    <th>Date</th>
                    <th>Task</th>
                    <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                  	<?php $count=1;?>
                    @foreach($internUpdate as $item)
                  <tr>
                  	<td>{{$count++}}</td>
                    <td>{{$item->date}}</td>
                    <td>{{$item->body}}</td>

                    @if($item->date==\Carbon\Carbon::now()->ToDateString())

                    <td>
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$item->id}}">Edit</button>
                    </td>

                     @else

                     <td></td>

                     @endif

                  </tr>

                    @include('intern.pages.modal.internDailyTaskEditModal')


                  @endforeach
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


@endsection