@extends('intern.layouts.home')
@section('content')

<div class="content-wrapper">

  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Intern's Inbox</h1>
          </div>
         
        </div>
      </div><!-- /.container-fluid -->
    </section>

    
          	@if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif


    <section class="content">
      <div class="container-fluid">
        <div class="row">

          @foreach($showQuery as $item)
          <div class="col-md-12">


            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">

                <ul class="list-group list-group-unbordered mb-3">
                  <li style="list-style: none;">
                    <h6>Message</h6>
                    <p><b>{{$item->message}} </b> </p>
                  </li>
                  <li style="list-style: none;">
                    <h6>Sended By</h6> 
                    <p><span style="font-weight: bold;">{{$item->sended_by}}</span> 
                  </li>
               

                 
                  
                </ul>

             
                    <a href="{{url('/internInboxAnwer/' .$item->id)}}" class="btn btn-success">Reply </a>
              
              </div>
              <!-- /.card-body -->


            </div>



          </div>


           @endforeach
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
</div>


@endsection