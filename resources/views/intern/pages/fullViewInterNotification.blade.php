@extends('intern.layouts.home')
@section('content')

 <div class="content-wrapper">
   
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          
          <div class="col-md-12" style="margin-top: 3%;">
            <div class="card">
              
              <div class="card-body p-0">
                <table class="table">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Field Name</th>
                      <th>Note</th>
                      <th>Type</th>
                    </tr>
                  </thead>
                  <tbody>

                    @php

                      $count = 1;

                    @endphp


                  	@foreach($getAllNotification as $tasks)
                    <tr>
                      <td>{{$count++}}</td>
                      <td>{{$tasks->field}}</td>
                      <td>{{$tasks->reason}}</td>
                      <td>{{$tasks->type}}</td>
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
           
          </div>
          <!-- /.col -->
        </div>
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection