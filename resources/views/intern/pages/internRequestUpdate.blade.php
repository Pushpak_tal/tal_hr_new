@extends('intern.layouts.home')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Intern Request</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
@if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">


          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Hi {{ Session::get('internname') }}, missed previous update?don't worry&nbsp<i class="fa fa-smile-o"></i></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{url('/updateRequestIntern')}}" id="empDreport" method="post">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">You are posting report for</label>

                    <input type="date" name="date" value="{{old('date')}}" required="" class="form-control">
                    
                  </div>

                  <p style="color:red;">{{ $errors->first('date') }}</p>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Select workplace</label>

                    <select name="workplace" style="width: 130px;" class="form-control">
                  		<option selected="" value="0">Office</option>
                  		<option value="1">Home</option>
                	</select>
                    
                  </div>

                  <p style="color:red;">{{ $errors->first('workplace') }}</p>


                  <div class="form-group">

	                <label>Your work details</label>
	                <textarea name="body" id="textsend" placeholder="what you've done ?" class="form-control" required=""></textarea>
	                <p style="color:red;">{{ $errors->first('update') }}</p>

        		 </div> 

        		<div class="form-group">

	                <label>Why missed?</label>
	                <textarea name="reason" id="textsend2" placeholder="Why did you miss?" class="form-control" required=""></textarea>
	                <p style="color:red;">{{ $errors->first('reason') }}</p>

        		</div>  


                  
                    <button type="submit" class="btn btn-primary" id="button">Send</button>

                </div>
                <!-- /.card-body -->

             
              </form>
            </div>

           

          </div>
          
        </div>
      </div>
    </section>
  </div>



@endsection
