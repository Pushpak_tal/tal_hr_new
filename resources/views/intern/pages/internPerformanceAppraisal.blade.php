@extends('intern.layouts.home')
@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Employee Request</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
          @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">


          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Hi {{ Session::get('employeename') }},  share your work experience with us<i class="fa fa-smile-o"></i></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->

              <?php $dt=Carbon\Carbon::now()->toDateString(); ?>

          @if($dt>=$dateLimit->startDate && $dt<=$dateLimit->endDate)

            @if(empty($appraisalAnswer))

            <form role="form" action="{{url('/appraisalInternAnswerSave')}}" id="empDreport" method="post">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                  	<div class="row">
                  		<div class="col-md-10">
                  			<label for="exampleInputEmail1">Team member's name:</label>&nbsp;

                    		{{$internDetail->name}}
                  		</div>

                  		<div class="col-md-2">
                  			<label for="exampleInputEmail1">Today:</label>&nbsp;

                    		<?php echo \Carbon\Carbon::now()->toDateString(); ?>
                  		</div>
                  	</div>
                    
                    
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Your Location:</label>

                    <input type="text" name="location" class="form-control" required>
                    
                  </div>

                  <p style="color:red;">{{ $errors->first('location') }}</p>


                  <div class="form-group">

	                <label>Performance review period:</label>
	                
	                <input type="date" name="startDate"> &nbsp; to &nbsp;

	                <input type="date" name="endDate">

        		 </div> 

        		 @foreach($questions as $item)

        		<div class="form-group">

        			<input type="hidden" name="question[]" value="{{$item->question}}">

	                <label>{{$item->question}}</label>

	                @if($item->answer_type=="Long")

	                <textarea class="form-control" name="answer[]" required></textarea>

	                @elseif($item->answer_type=="Short")

	                <input type="text" name="answer[]" class="form-control" required>

	                @endif
	               
        		</div> 

        		@endforeach 
                  
                    <button type="submit" class="btn btn-primary" id="button">Send</button>

                </div>
                <!-- /.card-body -->

             
              </form>

              @else
              <marquee>
		       		 <label>You have already submitted appraisal form.</label>
		       </marquee>
		       @endif

		        @else
		        <marquee>
		       		 <label>Sorry! Your assigned date for submission is over.</label>
		        </marquee>
        
        		@endif
            </div>

           

          </div>
          
        </div>
      </div>
    </section>
  </div>


@endsection