<div class="modal fade" id="exampleModal{{$internDetail->employee_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="{{route('edit.tempintern')}}" method="post" enctype="multipart/form-data">

          @csrf
          
          <input type="hidden" value="{{$internDetail->username}}" name="username" class="form-control">
          <input type="hidden" value="{{$internDetail->image}}" name="old_image">

          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle" src="{{asset('/upload/intern/' .$internDetail->image)}}"><br>

          <input type="file" name="image" style="display: none;" value="" id="file-upload">

          <label for="file-upload" class="custom-file-upload" style="cursor: pointer;">
            <i style="color: black;font-size: larger;margin-top: 30%;" class="fa fa-edit fa-2x" aria-hidden="true"></i>
          </label>



          </div>

          <input type="hidden" value="{{$internDetail->username}}" name="username" class="form-control">
          <input type="hidden" name="intern_id" value="{{$internDetail->intern_id}}">
          <input type="hidden" value="{{$internDetail->image}}" name="current_image">



          <label>Name</label>
          <input type="hidden" name="old_name" value="{{$internDetail->name}}" class="form-control">
          <input type="text" name="name" value="{{$internDetail->name}}" class="form-control">

          <label>Email</label>
          <input type="hidden" name="old_email" value="{{$internDetail->email}}" class="form-control">
          <input type="email" name="email" value="{{$internDetail->email}}" class="form-control">

          <label>Mobile Number</label>
          <input type="hidden" name="old_phone" value="{{$internDetail->phone}}" class="form-control">
          <input type="number" name="phone" value="{{$internDetail->phone}}" class="form-control">

          <label>Date of Birth</label>
          <input type="date" name="dob" value="{{$internDetail->dob}}" class="form-control" readonly>

          <label>Date of Joining</label>
          <input type="hidden" name="old_join_date" value="{{$internDetail->join_date}}" class="form-control">
          <input type="date" name="join_date" value="{{$internDetail->join_date}}" class="form-control">
          
        
          

                <!-- <a href="#" class="btn btn-primary"><b>Edit</b></a> -->

             
              <!-- /.card-body -->
            
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>