@extends('intern.layouts.home')
@section('content')

 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Query Answer</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-12 order-2 order-md-1">
              <div class="row">
                
              </div>
              <div class="row">
                <div class="col-12">
                  <h4><b>{{$question->title}}</b></h4>
                    <div class="post">
                     
                      <p>
                        {{$question->body}}
                      </p>

                      <p>
                        <span class="badge badge-primary">{{$question->tag}}</span>
                      </p>
                    </div>

                   
                </div>
              </div>
            </div>
            
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->

    </section>

    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Answers</h3>

         
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-8 order-2 order-md-1">
              <div class="row">
                
              </div>
              <div class="row">
                <div class="col-12">

                	@foreach($answer as $item)
                  
                    <div class="post">
                      <div class="user-block">
                        
                        <span class="username">
                          <a href="#" style="margin-left: -8%;">{{$item->added_by}}</a>
                        </span>
                       
                      </div>
                      <!-- /.user-block -->
                      <p>
                        {{$item->answer}}
                      </p>

                      <p>
                        
                      </p>
                    </div>

                    @endforeach
                   
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>

    </section>

    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Your Answer</h3>

         
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12 col-md-12 col-lg-12 order-2 order-md-1">
              <div class="row">
                
              </div>
              <div class="row">
                <div class="col-12">

                	<form method="post" action="{{url('/saveAnswerIntern')}}">
                		<div class="post">

                			@csrf

                			<input type="hidden" name="query_id" value="{{$question->id}}">
                			<input type="hidden" name="added_by" value="{{Session::get('internname')}}">
                      
                      		<label>Submit your answer regarding this question</label>
                     		 <textarea class="form-control" name="answer" cols="10" rows="10"></textarea><br>

                     		 <button type="submit" class="btn btn-primary">Post Your Answer</button>
                   
                		</div>
                	</form>
                  
                    
              </div>
            </div>
            
          </div>
        </div>
      </div>

    </section>
</div>

@endsection