@extends('intern.layouts.home')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center" style="margin-top: 3%;">
                  

                   <!-- <div class="popup-overlay">
						  <div class="popup-content">
						  	<form method="post" action="{{url('/tempInternImage')}}" enctype="multipart/form-data">
						  		@csrf
						  		<input type="hidden" value="{{$internDetail->username}}" name="username">
                  <input type="hidden" value="{{$internDetail->name}}" name="name">
                  <input type="hidden" value="{{$internDetail->email}}" name="email">
                  <input type="hidden" value="{{$internDetail->phone}}" name="phone">
                  <input type="hidden" value="{{$internDetail->dob}}" name="dob">
                  <input type="hidden" value="{{$internDetail->join_date}}" name="join_date">
						  		<input type="file" name="image" class="form-control" style="margin-top: 2%;">
							    <button type="submit" class="btn btn-primary">Submit</button>
							    <button class="btn btn-danger">Close</button> 
						  	</form>
						    
						    
						  </div>
					</div> -->




						<img class="profile-user-img img-fluid img-circle" src="{{asset('/upload/intern/'.$internDetail->image)}}"><br>
						
                </div>

                <h3 class="profile-username text-center">{{$internDetail->name}}</h3>

                <p class="text-muted text-center">Personal Details</p>

                <ul class="list-group list-group-unbordered mb-3">
             
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{$internDetail->email}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Mobile Number</b> <a class="float-right">{{$internDetail->phone}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Date of Birth</b> <a class="float-right">{{$internDetail->dob}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Date of Joining</b> <a class="float-right">{{$internDetail->join_date}}</a>
                  </li>
                </ul>

                <!-- <a href="#" class="btn btn-primary"><b>Edit</b></a> -->

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$internDetail->employee_id}}">Edit</button>

                @include('intern.pages.modal.editInternModal')
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            
            <!-- /.card -->
          </div>
          <!-- /.col -->
          
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection