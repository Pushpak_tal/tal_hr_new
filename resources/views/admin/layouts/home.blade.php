<!DOCTYPE html>
<html lang="en">
<head>
  

  <title>TAL | Dashboard </title>

  <link rel="icon" type="image/png" href="./upload/logo.png"/>
   <meta name="csrf-token" content="{{ csrf_token() }}" />



  @include('admin.includes.headerlinks')

  
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">


  @include('admin.includes.navbar')
  @include('admin.includes.sidebar')

  @yield('content')


  @include('sweetalert::alert')
  @include('admin.includes.rightsidebar')
 
 @include('admin.includes.footerlinks')

 @include('admin.includes.footer')


  
</div>

<script>
	

  //invoice 
  //calculate total 
  function cal_total() {
    
    var qty= document.getElementById("input-qty").value;
    var rate= document.getElementById("input-rate").value;

    var amt = (qty*rate).toFixed(2);

    document.getElementById('total').value = amt;
   
  }

  //calculate subtotal
  function cal_subtotal(){

    var total = document.getElementById("total").value;
    var discount = document.getElementById("input-discount").value;
    var gst = document.getElementById("input-gst").value;

    if(discount == "0" && gst =="0" ){

       
      document.getElementById("input-subtotal").value = total;
    }

     else{

        var temp_amt = (total * discount)/100;
        
        var amt =  total - temp_amt;  //amount after discount

        var temp_stl =(amt*gst)/100;

       var subtotal = (amt + temp_stl).toFixed(2); //amount after adding gst

      document.getElementById("input-subtotal").value = subtotal;
      var gst_amt =(subtotal-amt);
      document.getElementById("gst_amt").value = gst_amt;
    }

    



  }

  //calculate due
  function caldue(){
    
    var amt = document.getElementById("input-amt").value;
    var subtotal= document.getElementById("input-subtotal").value;

    var due = (subtotal - amt).toFixed(2);

    document.getElementById("input-due").value = due;
  }
  
  


  /*Quotation js*/


  /*calculate total*/
    function cal_Quo_total() {
    
    var qty= document.getElementById("input-qty").value;
    var rate= document.getElementById("input-rate").value;

    var amt = (qty*rate).toFixed(2);

    document.getElementById('input-total').value = amt;
   
  }

  /*calculate subtotal*/

  function cal_Quo_subtotal(){

    var total = document.getElementById("input-total").value;
    var discount = document.getElementById("input-discount").value;
    var gst = document.getElementById("input-gst").value;

    if(discount == "0" && gst =="0" ){

       
      document.getElementById("input-subtotal").value = total;
    }

     else{

        var temp_amt = (total * discount)/100;
        
        var amt =  total - temp_amt;  //amount after discount

        var temp_stl =(amt*gst)/100;

       var subtotal = (amt + temp_stl).toFixed(2); //amount after adding gst

      document.getElementById("input-subtotal").value = subtotal;
    }
}

</script>

<!-- CA result show -->
<script>
  $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#user_data').keyup(function(e){

      var search_data= $('#user_data').val();
      e.preventDefault();
      $.ajax({

        type:'POST',
        url:"{{route('search')}}",
        data:{search_data:search_data},
        success:function($data){
          $('#output').html($data);
        }
      });
    });


    $('#state_data').keyup(function(e){

      var state_data= $('#state_data').val();
      e.preventDefault();
      $.ajax({

        type:'POST',
        url:"{{route('search')}}",
        data:{state_data:state_data},
        success:function($data){
          $('#output').html($data);
        }
      });
    });

    $('#college_data').keyup(function(e){

      var college_data= $('#college_data').val();
      e.preventDefault();
      $.ajax({

        type:'POST',
        url:"{{route('search')}}",
        data:{college_data:college_data},
        success:function($data){
          $('#output').html($data);
        }
      });
    });

    $('#district_data').keyup(function(e){

      var district_data= $('#district_data').val();
      e.preventDefault();
      $.ajax({

        type:'POST',
        url:"{{route('search')}}",
        data:{district_data:district_data},
        success:function($data){
          $('#output').html($data);
        }
      });
    });

    $('#city_data').keyup(function(e){

      var city_data= $('#city_data').val();
      e.preventDefault();
      $.ajax({

        type:'POST',
        url:"{{route('search')}}",
        data:{city_data:city_data},
        success:function($data){
          $('#output').html($data);
        }
      });
    });
</script>

<script type=text/javascript>
      $(document).ready(function() {
        $("#displaybtn").click(function() { 
         
         $.ajax({ 
          type: "POST",
          url: "{{route('get')}}",  
          data: $("#output").serialize(),
          dataType: "html",     
          success: function (response) {
            $("#messagedisplay").html(response);
          }
        });

         $( "#user_data" ).focus(function() {
          
          $.ajax({ 
          type: "POST",
          url: "{{route('get')}}",  
          data: $("#output").serialize(),
          dataType: "html",     
          success: function (response) {
            $("#messagedisplay").html("");
          }
        });
        });

         $( "#state_data" ).focus(function() {
          
          $.ajax({ 
          type: "POST",
          url: "{{route('get')}}",  
          data: $("#output").serialize(),
          dataType: "html",     
          success: function (response) {
            $("#messagedisplay").html("");
          }
        });
        });

         $( "#college_data" ).focus(function() {
          
          $.ajax({ 
          type: "POST",
          url: "{{route('get')}}",  
          data: $("#output").serialize(),
          dataType: "html",     
          success: function (response) {
            $("#messagedisplay").html("");
          }
        });
        });

         $( "#district_data" ).focus(function() {
          
          $.ajax({ 
          type: "POST",
          url: "{{route('get')}}",  
          data: $("#output").serialize(),
          dataType: "html",     
          success: function (response) {
            $("#messagedisplay").html("");
          }
        });
        });

         $( "#city_data" ).focus(function() {
          
          $.ajax({ 
          type: "POST",
          url: "{{route('get')}}",  
          data: $("#output").serialize(),
          dataType: "html",     
          success: function (response) {
            $("#messagedisplay").html("");
          }
        });
        });
      });
      });
      
      </script>


<script>
  $(document).ready(function(){
    $("#displaybtn").click("focusout",function(e){
        $("#output").html("");
    });
});
</script>


<script>
        $(document).ready(function(){

    $(document).on("click", "#notification", function() { 
    $.ajax({
      url: '/changeNotificationStatus',
      type: "POST",
      dataType: "json",
      data:{
                _token:'{{ csrf_token() }}',
        username: $('#username').val(),
      },
      success: function(dataResult){
                $('#output').load(document.URL +  ' #output');;
      },
      error: function (dataResult) {
                 alert(dataResult);
           }
    });
  }); 
});

    </script>
</body>
</html>
