<!DOCTYPE html>
<html lang="en">
<head>
  

  <title>TAL | Dashboard</title>
  
  <link rel="icon" type="image/png" href="./upload/logo.png"/>

  @include('admin.includes.headerlinks')

  
</head>
<body class="hold-transition login-page">

  @yield('content')


</body>
</html>
