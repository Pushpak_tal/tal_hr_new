<!DOCTYPE html>
<html lang="en">
<head>
  

  <title>TAL | Dashboard </title>

  <link rel="icon" type="image/png" href="./upload/logo.png"/>


  @include('admin.includes.headerlinks')

  
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">


  @include('admin.includes.navbar')
  @include('admin.includes.sidebar')

  @yield('content')

  @php 

  $emp=App\EmployeeDetails::whereMonth('dob', '=', Carbon\Carbon::now()->format('m'))->whereDay('dob', '=', Carbon\Carbon::now()->format('d'))->get();

  $intern=App\InternDetail::whereMonth('dob', '=', Carbon\Carbon::now()->format('m'))->whereDay('dob', '=', Carbon\Carbon::now()->format('d'))->get();


  @endphp

  <div class="content-wrapper">

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>

        @if(isset($emp))

        @foreach($emp as $item)
            <marquee>
               <label style="margin-top: 1%;">Today is {{$item->name}}'s birthday</label>
            </marquee>

        @endforeach

        @endif

        @if(isset($intern))

        @foreach($intern as $item)
            <marquee>
               <label style="margin-top: 1%;">Today is {{$item->name}}'s birthday</label>
            </marquee>

        @endforeach

        @endif

        <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
          <div class="col-lg-3 col-6">

            @php

              $countEmp=App\EmployeeDetails::where('status',1)->count();

            @endphp
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$countEmp}}</h3>

                <p>No. of Employee</p>
              </div>
              <div class="icon">
                <i class="fa fa-users" aria-hidden="true"></i>

              </div>
              
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">

            @php

              $countIntern=App\InternDetail::where('status',1)->count();

            @endphp
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$countIntern}}</h3>

                <p>No. of Intern</p>
              </div>
              <div class="icon">
                <i class="fa fa-users" aria-hidden="true"></i>

              </div>
              
            </div>
          </div>

          <!-- ./col -->
          <div class="col-lg-3 col-6">

            @php

              $appraisalCountEmp=App\AppreaisalTrackEmployee::get()->count();

            @endphp
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$appraisalCountEmp}}</h3>

                <p>No. of employee appraisal recived</p>
              </div>
              <div class="icon">
                <i class="fa fa-signal" aria-hidden="true"></i>

              </div>
              
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">

            @php

              $appraisalCountIntern=App\AppreaisalTrackIntern::get()->count();

            @endphp
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{$appraisalCountIntern}}</h3>

                <p>No. of intern appraisal recived</p>
              </div>
              <div class="icon">
                <i class="fa fa-signal" aria-hidden="true"></i>

              </div>
              
            </div>
          </div>
          <!-- ./col -->
        </div>

        <div class="row">
            
          <div class="col-lg-3 col-6">

              @php

               $countUpdateReqEmp=App\updateRequestEmployee::where('status',null)->count();

              @endphp
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$countUpdateReqEmp}}</h3>

                <p>Update Request Employee</p>
              </div>
              <div class="icon">
                <i class="fa fa-wrench" aria-hidden="true"></i>

              </div>
              
            </div>
          </div>


          <div class="col-lg-3 col-6">

            @php

              $countUpdateReqIntern=App\updateRequestIntern::where('status',null)->count();

            @endphp
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                <h3>{{$countUpdateReqIntern}}</h3>

                <p>Update Request Intern</p>
              </div>
              <div class="icon">
                <i class="fa fa-wrench" aria-hidden="true"></i>

              </div>
              
            </div>
          </div>

           <div class="col-lg-3 col-6">

            @php 

                $start = date('z') + 1 - 7;

                $end = date('z') + 1 + 7;

                $empBirthday = App\EmployeeDetails::whereRaw("DAYOFYEAR(dob) BETWEEN $start AND $end")->count();

            @endphp

            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                <h3>{{$empBirthday}}</h3>

                <p>No. of upcoming birthday employee</p>
              </div>
              <div class="icon">
               <i class="fa fa-birthday-cake" aria-hidden="true"></i>

              </div>
              
            </div>
          </div>

          <div class="col-lg-3 col-6">

            @php

              $start = date('z') + 1 - 7;

              $end = date('z') + 1 + 7;

              $internBirthday = App\InternDetail::whereRaw("DAYOFYEAR(dob) BETWEEN $start AND $end")->count();

            @endphp
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                <h3>{{$internBirthday}}</h3>

                <p>No. of upcoming birthday intern</p>
              </div>
              <div class="icon">
                <i class="fa fa-birthday-cake" aria-hidden="true"></i>

              </div>
              
            </div>
          </div>

        </div>

        <div class="row">
          <div class="col-lg-3 col-6">

            @php

              $countAmbasador=App\CampusAmbasador::where('status',1)->count();

            @endphp
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <h3>{{$countAmbasador}}</h3>

                <p>No. of Ambasador</p>
              </div>
              <div class="icon">
                <i class="fa fa-graduation-cap" aria-hidden="true"></i>


              </div>
              
            </div>
          </div>
         
        </div>
       
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
  </div>


  @include('sweetalert::alert')
  @include('admin.includes.rightsidebar')
 
 @include('admin.includes.footerlinks')

 @include('admin.includes.footer')


  
</div>

<script>
	

  //invoice 
  //calculate total 
  function cal_total() {
    
    var qty= document.getElementById("input-qty").value;
    var rate= document.getElementById("input-rate").value;

    var amt = (qty*rate).toFixed(2);

    document.getElementById('total').value = amt;
   
  }

  //calculate subtotal
  function cal_subtotal(){

    var total = document.getElementById("total").value;
    var discount = document.getElementById("input-discount").value;
    var gst = document.getElementById("input-gst").value;

    if(discount == "0" && gst =="0" ){

       
      document.getElementById("input-subtotal").value = total;
    }

     else{

        var temp_amt = (total * discount)/100;
        
        var amt =  total - temp_amt;  //amount after discount

        var temp_stl =(amt*gst)/100;

       var subtotal = (amt + temp_stl).toFixed(2); //amount after adding gst

      document.getElementById("input-subtotal").value = subtotal;
      var gst_amt =(subtotal-amt);
      document.getElementById("gst_amt").value = gst_amt;
    }

    



  }

  //calculate due
  function caldue(){
    
    var amt = document.getElementById("input-amt").value;
    var subtotal= document.getElementById("input-subtotal").value;

    var due = (subtotal - amt).toFixed(2);

    document.getElementById("input-due").value = due;
  }
  
  


  /*Quotation js*/


  /*calculate total*/
    function cal_Quo_total() {
    
    var qty= document.getElementById("input-qty").value;
    var rate= document.getElementById("input-rate").value;

    var amt = (qty*rate).toFixed(2);

    document.getElementById('input-total').value = amt;
   
  }

  /*calculate subtotal*/

  function cal_Quo_subtotal(){

    var total = document.getElementById("input-total").value;
    var discount = document.getElementById("input-discount").value;
    var gst = document.getElementById("input-gst").value;

    if(discount == "0" && gst =="0" ){

       
      document.getElementById("input-subtotal").value = total;
    }

     else{

        var temp_amt = (total * discount)/100;
        
        var amt =  total - temp_amt;  //amount after discount

        var temp_stl =(amt*gst)/100;

       var subtotal = (amt + temp_stl).toFixed(2); //amount after adding gst

      document.getElementById("input-subtotal").value = subtotal;
    }

    



  }




</script>



<script>
        $(document).ready(function(){

    $(document).on("click", "#notification", function() { 
    $.ajax({
      url: '/changeNotificationStatus',
      type: "POST",
      dataType: "json",
      data:{
                _token:'{{ csrf_token() }}',
        username: $('#username').val(),
      },
      success: function(dataResult){
                $('#output').load(document.URL +  ' #output');;
      },
      error: function (dataResult) {
                 alert(dataResult);
           }
    });
  }); 
});

    </script>


</body>
</html>
