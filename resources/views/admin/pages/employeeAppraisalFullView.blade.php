@extends('admin.layouts.home')
@section('content')



<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Employee Request</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
          @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">


          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Appraisal self-assesment<i class="fa fa-smile-o"></i></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{url('/employee/appraisal/fullView')}}" id="empDreport" method="post">
                @csrf

                <div class="card-body">
                  <div class="form-group">
                  	<div class="row">
                  		<div class="col-md-10">
                  			<label for="exampleInputEmail1">Team member's name:</label>&nbsp;

                  			@foreach($empDetail as $item)
                  			{{$item->name}}
                  			@endforeach

                  		</div>

                  		<div class="col-md-2">
                  			<label for="exampleInputEmail1">Date:</label>&nbsp;

                  			{{$answersSubmissionDate}}
                    		
                  		</div>
                  	</div>
                    
                    
                  </div>

                  <div class="form-group">
                    <label for="exampleInputEmail1">Your Location:</label> &nbsp;

                    {{$answersLocation}}
                    
                  </div>


                  <div class="form-group">

	                <label>Performance review period:</label>
	                
	                {{$answersSubStartDate}} &nbsp;  to &nbsp; {{$answersSubEndDate}} 

        		 </div> 

        		 @foreach($answers as $item)

        		<div class="form-group">

        			<b>{{$item->question}} :</b> &nbsp;
        			{{$item->answer}}
	               
        		</div> 

        		@endforeach 
                  
                
                </div>

             
              </form>
            </div>

           

          </div>
          
        </div>
      </div>
    </section>
  </div>


@endsection