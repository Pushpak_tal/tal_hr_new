<!DOCTYPE html>
<html>
<head>
	<title></title>
	<style type="text/css">
		* {
      margin: 0px;
      padding: 0px;
      box-sizing: border-box;
}
body {
      margin: 25px ;
}
p {
      font-size: 17px;
      font-weight: 600;
      line-height: 23px;
}


.left {
      float: left;
      margin-left: 6px;
}
.right {
      float: right;
      
      margin-top: 30px;
}


.right h1{
      color: #ffcc00;
      margin-left: 160px;
      margin-bottom: 30px;
}

 .time {
  
    margin-top: 95px;
    text-align: right;
    margin-right: 40px;
    

}

.tabls {
      clear: both;
}
/* .flex {
      display: flex;
      fle
} */
.brand, .flex {
      display: flex;
      flex-direction: row;
      margin-bottom: 10px;
      align-content: left; 
}
.brand img {
      
      margin-top: 5px;
      height: 50px;
      
}
.brand h3 {
      margin-top: 10px;
      margin-left: 55px;      
      text-transform: uppercase;
      color: #ffcc00;
}
.heading {
      background: #ffcc00;
      color: white;
      padding: 2px;
      width: 50%;
      margin: 20px 0px;
}
table {
      width: 90%;
      margin-top: 20px;
}
th {
      text-transform: uppercase;
      background: #ffcc00;
      color: white;
      font-size: 16px;
      font-weight: 300;
      padding: 6px;
}
.main {
      width: 280px;
}
.second{
    width: 30px;
}
.third{
  width: 30px;
}

tr {
   border-right: 1px solid #212112;   
}
td {
      font-size: 15px;
      font-weight: 600;
      padding: 2px;
     
      border: 1px solid #212112;
      
      
}

.blank { 
      border: none;
}
.first-thing td{
      height: 250px;
}
.comments {
      margin-top: 90px;
      height: 190px;
      width: 520px;
      border: 1px solid #212112;
      align-self: left;
      
     
}

.payments {
      margin-right:60px !important ;
      margin-top:50px;
      text-align: right;
}
.headings {
      font-size: 17px;
      background: #ffcc00;
      padding: 5px;
      border-bottom: 1px solid #212121;
      color: white;
}
.footer {
      margin-top: 380px;      
      text-align: center;
}
page {
  background: white;
  display: block;
  margin: 0 auto;
  margin-bottom: 0.5cm;
  box-shadow: 0 0 0.5cm rgba(0,0,0,0.5);
}
page[size="A4"] {  
  width: 21cm;
  height: 29.7cm; 
}
page[size="A4"][layout="landscape"] {
  width: 29.7cm;
  height: 21cm;  
}
page[size="A3"] {
  width: 29.7cm;
  height: 42cm;
}
page[size="A3"][layout="landscape"] {
  width: 42cm;
  height: 29.7cm;  
}
page[size="A5"] {
  width: 14.8cm;
  height: 21cm;
}
page[size="A5"][layout="landscape"] {
  width: 21cm;
  height: 14.8cm;  
}
@media print {
  body, page {
    margin: 0;
    box-shadow: 0;
  }
}
	</style>
</head>
<body>
<page size="A4">
	 <div class="container head row">
            <div class="col-6 left">
               <div class="brand">
                     <img src="https://media.licdn.com/dms/image/C510BAQFHQVNa6N5ACg/company-logo_200_200/0?e=2159024400&v=beta&t=SbB07wODxoiukBSOC-6hdNiVLvtzGwRy8DZCz3-8msQ" alt="logo">
                     <h3>Thinkagain educational Services</h3>
               </div>

                <p>
                          fl-5 , b/13 bapuji nagar
                      <br>kolkata, west bengal - 700092
                      <br>Phone: 7098378871
                       
                </p>   
                <p>
                      Website : www.thinkagainlab.com <br>
                      Email : hello@thinkagainlab.com <br>
                      GSTIN : 19AAMFT6208M1ZJ
                </p>
                <h5 class="heading">
                  INVOICE FOR
                </h5>
                <p>Name of client <br>
                  Designation of client <br>
                  Comapny name of the client <br>
                  Client's contact number <br>
                  Client's mail id</p>

            </div>
            

                <div class="time">
                  <p>Invoice no.
                <b>{{ $data->invoice_number }}</b> </p>                         
                </div>                     
               
          </div>
            <!-- Description -->
      <div class="container tabls">
            <div class="row">
                  <div class="col-7"> 
                              <!-- tasks -->

      <table style="margin-right: 19px;">
                  <tr>
                    <th class="main">DESCRIPTION</th>
                    <th class="second">SAC No</th> 
                    <th class="second">QTY</th> 
                    <th class="second">RATE(INR)</th>
                    <th class="second">AMOUNT(INR)</th>
                  </tr>
                  <tr class="first-thing">
                    <td>{{ $data->description }}</td>
                    <td>{{ $data->sac }}</td>
                    <td>{{ $data->qty }}</td> 
                    <td>{{$data->rate}}</td>
                    <td>{{$data->total}}</td>
                  </tr>
                  <tr>
                    <td class="blank"></td>
                    <td class="blank"></td>
                  
                    <td class="blank"></td>
              
                  </tr>
                  <tr>
                  <td class="blank"></td>
                  <td class="blank"></td>
                  <td>Discount amt.</td> 
                  <td class="blank"></td>
                  <td>{{$data->discount_amt}}</td>
                  </tr>
                  <tr>
                  <td class="blank"></td>
                  <td class="blank"></td>
                  <td>GST amt.</td> 
                  <td class="blank"></td>
                  <td>{{$data->gst_amt}}</td>
                  </tr>
                  <tr>
                  <td class="blank"></td>
                  <td class="blank"></td>
                  <td>Total amount</td> 
                  <td class="blank"></td>
                  <td>{{$data->sub_total}}</td>
                  </tr>


                </table>
                <br><br><br>

                <div class="comments" >
                          <h2 class="headings">
                            OTHER COMMENTS  
                          </h2>                          
                            <p style="padding-left: 3px;">
                                  1. We have 2 - phase transparent payment system.  <br>
                                  2. 60 % - Project confirmation amount, has to be paid to confirm the
                                  project before development. <br>
                                  3. 40 % - Project development amount, has to be paid after first review. <br>
                                  4. All the amount is in Indian Rupees(INR).
                            </p>
                                    
                  </div><br>
                  <br><br><br><br>   
                        
                  <div class="payments">
                  <p>
                    Make all cheques/bank transfer payable to <br>
                    <b> M/S THINKAGAIN EDUCATIONAL SERVICES LLP</b> <br>
                    A/c No - 3711924610 IFSC - KKBK0006577 <br>
                    Kotak Mahindra Bank , Garia Branch
                  </p>
                  </div>


                    

                <!-- Conclution -->
                <div class="footer">
                      <p>If you have any questions about this invoice, please contact <br>
                              Naina Singh , hello@thinkagainlab.com , 8240925036</p>
                        <h3>Thank You For Your Business!</h3>
                        
                </div>
</page>
</body>
</html>