@extends('admin.layouts.home')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Search Invoice</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Invoice</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


     <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-danger">
            
              <div class="card-body">
                <form method="post" action="{{url('/searchInvoice')}}">
                  @csrf
                  <div class="row">
                  <div class="col-5">
                    <input type="text" class="form-control" placeholder="Invoice Number" name="invoice_number" required>
                  </div>
                  
                  <div class="col-2">
                    <button type="submit" class="btn btn-primary">Search</button>
                  </div>
                </div>
                </form>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

      

          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>


    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Invoice Table</h1>
          </div>
          <div class="col-sm-6">
            
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->

    @if(!empty($search))


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

          
            
            <div class="card">
              <div class="card-header">

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                     <th>Invoice</th>
                    <th>Date</th>
                    <th>Paid</th>
                    <th>Description</th>
                    <th>Amt(₹)</th>
                    <th>Due(₹)</th>
                    <th>Discount</th>
                    <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    @foreach($search as $item)
                  <tr>
                    <td>{{$item->invoice_number}}</td>
                    <td>{{$item->date}}</td>

                    <td>
                      @if($item->due!=0)
                      <i class="bg-warning"></i>  <a href="#"> {{ $item->client_id }} </a>
                      @else
                      <i class="bg-success"></i>  <a href="#"> {{ $item->client_id }} </a>
                      @endif
                    </td>



                    <td>{{$item->description}}</td>
                    <td>{{$item->amt}}</td>
                    <td>{{$item->due}}</td>
                    <td>{{$item->discount_precentage}}%</td>

                    <td>
                      <a href="{{url('/invoice/pdf/' .$item->id)}}" class="btn btn-danger">PDF</a>

                       <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-lg{{$item->id}}">Edit</button> -->

                       <a href="{{url('/edit/invoice/'.$item->id)}}" class="btn btn-primary">Edit</a>
                    </td>
                    
                  </tr>

                  @endforeach
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>


    @else

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

          
            
            <div class="card">
              <div class="card-header">

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                     <th>Invoice</th>
                    <th>Date</th>
                    <th>Paid</th>
                    <th>Description</th>
                    <th>Amt(₹)</th>
                    <th>Due(₹)</th>
                    <th>Discount</th>
                    <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    
                    @foreach($get_invoice as $item)
                  <tr>
                    <td>{{$item->invoice_number}}</td>
                    <td>{{$item->date}}</td>

                    <td>
                      @if($item->due!=0)
                      <i class="bg-warning"></i>  <a href="#"> {{ $item->client_id }} </a>
                      @else
                      <i class="bg-success"></i>  <a href="#"> {{ $item->client_id }} </a>
                      @endif
                    </td>



                    <td>{{$item->description}}</td>
                    <td>{{$item->amt}}</td>
                    <td>{{$item->due}}</td>
                    <td>{{$item->discount_precentage}}%</td>

                    <td>
                      <a href="{{url('/invoice/pdf/' .$item->id)}}" class="btn btn-danger">PDF</a>

                       <!-- <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-lg{{$item->id}}">Edit</button> -->

                       <a href="{{url('/edit/invoice/'.$item->id)}}" class="btn btn-primary">Edit</a>
                    </td>
                    
                  </tr>

                  @endforeach
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>

    @endif
    
    <!-- /.content -->
  </div>
@endsection