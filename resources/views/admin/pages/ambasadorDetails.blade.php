@extends('admin.layouts.home')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Campus Ambasador Details</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Campus Ambasador</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif


    <!-- Main content -->
 <!--    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-danger">
            
              <div class="card-body">
                <form method="post" action="{{url('/searchEmployeeDetail')}}">
                  @csrf
                  <div class="row">
                  <div class="col-5">
                    <input type="text" class="form-control" placeholder="Username" name="username" required>
                  </div>
                  <div class="col-5">
                    <input type="text" class="form-control" placeholder="Name" name="name" required>
                  </div>
                  <div class="col-2">
                    <button type="submit" class="btn btn-primary">Search</button>
                  </div>
                </div>
                </form>
                
              </div>
            </div>

      

          </div>
         
        </div>
      </div>
    </section> -->
    <!-- /.content -->


    <section class="content">
      <div class="container-fluid">
        <div class="row">

          @foreach($ambasador_detail as $item)
          <div class="col-md-4">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('/upload/ambasador/'.$item->image)}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$item->name}}</h3>

                <p class="text-muted text-center">{{$item->username}}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{$item->email}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Phone</b> <a class="float-right">{{$item->phone}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>State</b> <a class="float-right">{{$item->state}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>City</b> <a class="float-right">{{$item->city}}</a>
                  </li>

                   <li class="list-group-item">
                    <b>District</b> <a class="float-right">{{$item->district}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>Pin</b> <a class="float-right">{{$item->pin}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>Id Proof</b> <a href="{{asset('/upload/ambasador/'.$item->id_proof)}}" target="_blank" class="float-right">View</a>
                  </li>

                   <li class="list-group-item">
                    <b>Address Proof</b> <a href="{{asset('/upload/ambasador/'.$item->address_proof)}}" target="_blank" class="float-right">View</a>
                  </li>
                </ul>

               <td>

                      @if($item->status=="1")

                      <button type="button" class="btn btn-secondary" disabled>Approved</button>


                      @elseif($item->status=="2")

                      <button type="button" class="btn btn-secondary" disabled>Rejected</button>

                      @else
                      
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$item->id}}">Approve</button>

                     
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCancel{{$item->id}}">Reject</button>
                      @endif
                    </td>
              </div>
              <!-- /.card-body -->
            </div>

          </div>

           
            @include('campusAmbasador.pages.modal.approveCampusAmbasador')
            @include('campusAmbasador.pages.modal.rejectCampusAmbasador')


           @endforeach
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
 
  </div>

@endsection