@extends('admin.layouts.home')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Employee Daily Update</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">General Form</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

     @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">

         
          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Select Employee to get daily report</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{url('/dailyUpdateFiltered')}}" id="empDreport" method="get">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Employee</label>
                    <select class="form-control" id="dropdown" name="name">
                      <option selected disabled>---Select a Employee---</option>
                      @foreach($empDetail as $item)

                        <option value="{{$item->username}}">{{$item->name}} [{{$item->username}}]</option>

                      @endforeach
                    </select>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Work Place:</label> &nbsp;
                    <input type="radio" value="in_office" name="work_place" value="1" checked=""> In-Office

                    <input type="radio" value="from_home" name="work_place" value="2"> Work from home
                  </div>

                  <div class="form-group">
                    <label for="exampleInputFile">Select Date:</label>
                    <div class="input-group">
                      <div class="custom-file">
                       <input type="date" name="startDate" id="d1">&nbsp;

                       <label>to</label>&nbsp;

                       <input type="date" name="endDate" id="d2">
                      </div>
                      
                    </div>
                  </div>

                    <button type="submit" class="btn btn-primary" id="button">Search</button>

                </div>
                <!-- /.card-body -->

             
              </form>
            </div>

           

          </div>
          
        </div>
      </div>
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

          
            
            <div class="card">
             
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>User id</th>
                      <th>Name</th>
                      <th>Date</th>
                      <th>Task</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $count=1;?>
                    @foreach($list as $item)
                  <tr>
                    <td>{{$count++}}</td>
                    <td>{{$item->added_by}}</td>
                    <td>{{$item->name}}</td>

                    <td>{{$item->date}}</td>
                    
                    <td>{{$item->body}}</td>
                  </tr>

                  @endforeach
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
  </div>

  <script>
    function success() { 

      var dropdown=document.getElementById("dropdown").value;
      var d1=new Date(document.getElementById("d1").value);
      var d2=new Date(document.getElementById("d2").value);

      if (dropdown.value.length==0){
        alert("Please select name");
      }

      else{
        if (d1.getTime() < d2.getTime()) {

            
            document.getElementById('empDreport').action = "/dailyUpdateFiltered";
            document.getElementById('empDreport').submit();
            document.getElementById('button').disabled = false ;
            

          }
           if(d1.getTime() > d2.getTime()) { 
              document.getElementById('button').disabled = true ;
              alert("Start date should be less than end date");
          }
        }
        
    }
</script>

@endsection