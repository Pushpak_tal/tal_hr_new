@extends('admin.layouts.home')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Intenr Appraisal</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
      <a href="{{url('/appraisalInternQuestions')}}" class="btn btn-danger" style="margin-left: 1%;">Questions</a>
    </section>



    @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
          
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">    	
        <div class="row">

          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Select Intern to get performance appraisal</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{url('/intern/appraisal/list')}}" id="empDreport" method="post">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Intern</label>
                    <select class="form-control" id="dropdown" name="internId">
                      <option selected disabled>---Select a Employee---</option>

                      @foreach($internDetail as $item)

                      <option value="{{$item->username}}">{{$item->name}} [{{$item->username}}]</option>

                      @endforeach
                     
                    </select>
                  </div>
                  
                  <div class="form-group">
                    <label for="exampleInputFile">Select Date:</label>
                    <div class="input-group">
                      <div class="custom-file">
                       <input type="date" name="startDate" id="d1">&nbsp;

                       <label>to</label>&nbsp;

                       <input type="date" name="endDate" id="d2">
                      </div>
                      
                    </div>
                  </div>

                    <button type="submit" class="btn btn-primary" id="button">Search</button>

                </div>
                <!-- /.card-body -->

             
              </form>
            </div>

           

          </div>

          <div class="col-md-6">
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Set deadline for submission</h3>
              </div>
              <form role="form" action="{{url('/assignDateToIntern')}}" id="empDreport" method="post">
                @csrf
                <div class="card-body">
                  
                  
                  <div class="form-group">
                    <label for="exampleInputFile">Select Date:</label>
                    <div class="input-group">
                      <div class="custom-file">
                       <input type="date" name="startDate" id="d1" class="form-control">&nbsp;

                       <label>to</label>&nbsp;

                       <input type="date" name="endDate" id="d2" class="form-control">
                      </div>
                      
                    </div>
                  </div>

                    <button type="submit" class="btn btn-primary" id="button">Assign</button>

                </div>

             
              </form>
            </div>

           

          </div>
          
        </div>
      </div>
    </section>
  </div>

   <script>
    function success() { 

      var dropdown=document.getElementById("dropdown").value;
      var d1=new Date(document.getElementById("d1").value);
      var d2=new Date(document.getElementById("d2").value);

      if (dropdown.value.length==0){
        alert("Please select name");
      }

      else{
        if (d1.getTime() < d2.getTime()) {

            
            document.getElementById('empDreport').action = "/dailyUpdateFiltered";
            document.getElementById('empDreport').submit();
            document.getElementById('button').disabled = false ;
            

          }
           if(d1.getTime() > d2.getTime()) { 
              document.getElementById('button').disabled = true ;
              alert("Start date should be less than end date");
          }
        }
        
    }
</script>
@endsection