@extends('admin.layouts.home')
@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Employee Request</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
          @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">


          <!-- left column -->
          <div class="col-md-6">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Appraisal self-assesment<i class="fa fa-smile-o"></i></h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{url('/employee/appraisal/fullView')}}" id="empDreport" method="post">
                @csrf

                @foreach($data as $item)
                <div class="card-body">
                  <div class="form-group">

                  	<input type="hidden" name="emp_id" value="{{$item->emp_id}}">
                  	<input type="hidden" name="submission_date" value="{{$item->submission_date}}">

                  	<label>Employee id :</label> &nbsp;{{$item->emp_id}}<br>
                  
					<label>Submission time:</label>&nbsp;{{$item->created_at}}                    
                  </div>

                  <label>Click on this button to see the whole data</label><br>

                    <button type="submit" class="btn btn-primary" id="button">View</button>

                </div>

                @endforeach
                <!-- /.card-body -->

             
              </form>
            </div>

           

          </div>
          
        </div>
      </div>
    </section>
  </div>

@endsection