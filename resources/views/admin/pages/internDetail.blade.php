@extends('admin.layouts.home')
@section('content')

<div class="content-wrapper">

   <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Search Intern</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Our Intern</li>
            </ol>
          </div>
        </div>
      </div>
    </section>


     <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-danger">
            
              <div class="card-body">
                <form method="post" action="{{url('/searchInternDetail')}}">
                  @csrf
                  <div class="row">
                  <div class="col-5">
                    <input type="text" class="form-control" placeholder="Username" name="username" required>
                  </div>
                  <div class="col-5">
                    <input type="text" class="form-control" placeholder="Name" name="name" required>
                  </div>
                  <div class="col-2">
                    <button type="submit" class="btn btn-primary">Search</button>
                  </div>
                </div>
                </form>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

      

          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>


    @if(!empty($search))


    <section class="content">
      <div class="container-fluid">
        <div class="row">

          @foreach($search as $item)
          <div class="col-md-4">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('/upload/intern/'.$item->image)}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$item->name}}</h3>

                <p class="text-muted text-center">{{$item->username}}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{$item->email}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Phone</b> <a class="float-right">{{$item->phone}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Date of Birth</b> <a class="float-right">{{$item->dob}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>Join Date</b> <a class="float-right">{{$item->join_date}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>Id Proof</b> <a href="{{asset('/upload/intern/'.$item->id_proof)}}" target="_blank" class="float-right">View</a>
                  </li>

                   <li class="list-group-item">
                    <b>Address Proof</b> <a href="{{asset('/upload/intern/'.$item->address_proof)}}" target="_blank" class="float-right">View</a>
                  </li>
                </ul>

               <td>

                      @if($item->status=="1")

                      <button type="button" class="btn btn-secondary" disabled>Approved</button>


                      @elseif($item->status=="2")

                      <button type="button" class="btn btn-secondary" disabled>Rejected</button>

                      @else
                      
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$item->employee_id}}">Approve</button>

                     
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCancel{{$item->employee_id}}">Reject</button>
                      @endif
                    </td>
              </div>
              <!-- /.card-body -->
            </div>

          </div>

            @include('admin.pages.modal.approveIntern')

                  @include('admin.pages.modal.cancelIntern')

           @endforeach
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>


    @else

      <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Our Intern</h1>
          </div>
          
        </div>
      </div>
    </section>

     <section class="content">
      <div class="container-fluid">
        <div class="row">

          @foreach($internDetail as $item)
          <div class="col-md-4">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('/upload/intern/'.$item->image)}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$item->name}}</h3>

                <p class="text-muted text-center">{{$item->username}}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{$item->email}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Phone</b> <a class="float-right">{{$item->phone}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Date of Birth</b> <a class="float-right">{{$item->dob}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>Join Date</b> <a class="float-right">{{$item->join_date}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>Id Proof</b> <a href="{{asset('/upload/intern/'.$item->id_proof)}}" target="_blank" class="float-right">View</a>
                  </li>

                   <li class="list-group-item">
                    <b>Address Proof</b> <a href="{{asset('/upload/intern/'.$item->address_proof)}}" target="_blank" class="float-right">View</a>
                  </li>
                </ul>

               <td>

                      @if($item->status=="1")

                      <button type="button" class="btn btn-secondary" disabled>Approved</button>


                      @elseif($item->status=="2")

                      <button type="button" class="btn btn-secondary" disabled>Rejected</button>

                      @else
                      
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$item->intern_id}}">Approve</button>

                     
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCancel{{$item->intern_id}}">Reject</button>
                      @endif
                    </td>
              </div>
              <!-- /.card-body -->
            </div>

          </div>

            @include('admin.pages.modal.approveIntern')

                  @include('admin.pages.modal.cancelIntern')

           @endforeach
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

    @endif




  
   
  </div>

@endsection