@extends('admin.layouts.home')
@section('content')

<div class="content-wrapper">

	 <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Search Campus Ambasador</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Campus Ambasador</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            <div class="card card-danger">
            
              <div class="card-body">
                  <div class="row">
                  <div class="col-2">
                    <input type="search" class="form-control" placeholder="Name" name="search" id="user_data">
                  </div>

                  <div class="col-2">
                    <input type="search" class="form-control" placeholder="State" name="search" id="state_data">
                  </div>

                  <div class="col-2">
                    <input type="search" class="form-control" placeholder="College" name="search" id="college_data">
                  </div>

                  <div class="col-2">
                    <input type="search" class="form-control" placeholder="District" name="search" id="district_data">
                  </div>

                  <div class="col-2">
                    <input type="search" class="form-control" placeholder="City" name="search" id="city_data">
                  </div>

                  <div class="col-2">
                    <button type="button" class="btn btn-primary" id='displaybtn'>Show All</button>
                  </div>

                  <p id="output"></p>

                <p id="description"></p>
                <p id="messagedisplay"></p>
                </div>               
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

      

          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

</div>



@endsection