<div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">What you've done today?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{url('/editQuestions')}}">
                @csrf
                <label>Question</label>

                <input type="hidden" name="id" value="{{$item->id}}">

                <input class="form-control" name="question" value="{{$item->question}}"></input><br>

                <label>Answer type</label>

                <select class="form-control" name="answer_type">
                  <option value="Short" {{ old('answer_type',$item->answer_type)=='Short' ? 'selected' : ''  }}>Short answer</option>
                  <option value="Long"  {{ old('answer_type',$item->answer_type)=='Long' ? 'selected' : '' }}>Long answer</option>
                </form>
                </select>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-danger">Save</button>
      </div>
      </form>
    </div>
  </div>
</div>