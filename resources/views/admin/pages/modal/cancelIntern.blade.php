<div class="modal fade" id="exampleModalCancel{{$item->intern_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to approve the Intern?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
     
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <form method="post" action="{{url('/cancel/status/intern')}}">
          @csrf

          <input type="hidden" value="{{$item->intern_id}}" name="intern_id">
          <button type="submit" class="btn btn-danger">Yes</button>

        </form>
        
      </div>
    </div>
  </div>
</div>