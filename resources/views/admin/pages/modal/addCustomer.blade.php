<div class="modal fade" id="modal-lg">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Add Customer's Records</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="post" action="{{url('/addCustomer')}}">
                @csrf
                <div class="row">
                  <div class="col-md-6">
                    <label>Name</label>
                    <input type="text" name="name" class="form-control" placeholder="Customer's Name" required>
                  </div>

                  <div class="col-md-6">
                    <label>Company Name</label>
                    <input type="text" name="business_name" class="form-control" placeholder="Business  Name" required>
                  </div>
                </div><br>

                <div class="row">
                  <div class="col-md-6">
                    <label>Phone#</label>
                    <input type="text" name="phone" class="form-control" placeholder="Phone Number" required>
                  </div>

                  <div class="col-md-6">
                    <label>Address</label>
                    <input type="text" name="address" class="form-control" placeholder="Address" required>
                  </div>
                </div><br>

                <div class="row">
                  <div class="col-md-6">
                    <label>Email-Id</label>
                    <input type="email" name="email" class="form-control" placeholder="Email-Id (if any)">
                  </div>

                  <div class="col-md-6">
                    <label>GSTIN</label>
                    <input type="text" name="gstin" class="form-control" placeholder="GSTIN (if any)">
                  </div>
                </div>
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">save</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>