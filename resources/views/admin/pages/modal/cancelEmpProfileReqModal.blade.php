<div class="modal fade" id="exampleModalCancel{{$item->temp_employee_id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to reject the employee?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form method="post" action="{{url('/cancel/employeePofile/status')}}">
          @csrf

          <input type="hidden" value="{{$item->temp_employee_id}}" name="temp_employee_id">
          <input type="hidden" value="{{$item->username}}" name="username">
          <label>Give reason</label>
          <textarea class="form-control" name="reason" required></textarea>
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button type="submit" class="btn btn-danger">Yes</button>
      </div>
      </form>
    </div>
  </div>
</div>