<div class="modal fade" id="modal-lg{{$item->id}}">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h4 class="modal-title">Change Invoice details</h4>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
              <form method="post" action="{{url('/editInvoice')}}">
                @csrf
                <div class="row">
                  <div class="col-md-12">
                    <label>Invoice#</label><br>
                    {{$item->invoice_number}}
                  </div>

                  <input type="hidden" name="id" value="{{$item->id}}">

                  <div class="col-md-6">
                    <label>Date</label>
                    <input type="date" value="{{$item->date}}" name="date" class="form-control">
                  </div>

                  <div class="col-md-6">
                    <label>Description</label>
                    <input type="text" value="{{$item->description}}" name="description" class="form-control">
                  </div><br><br>

                  <div class="col-md-6">
                    <label>Amt(₹)</label>
                    <input type="text" value="{{$item->amt}}" name="amt" class="form-control">
                  </div>

                  <div class="col-md-6">
                    <label>Due(₹)</label>
                    <input type="text" value="{{$item->due}}" name="due" class="form-control">
                  </div>

                  <div class="col-md-6">
                    <label>Discount</label>
                    <input type="text" value="{{$item->discount_precentage}}" name="discount_precentage" class="form-control">
                  </div>

                </div>

              
              
            </div>
            <div class="modal-footer justify-content-between">
              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
              <button type="submit" class="btn btn-primary">save</button>
            </div>
            </form>
          </div>
          <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
      </div>