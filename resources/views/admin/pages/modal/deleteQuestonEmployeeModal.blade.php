<div class="modal fade" id="exampleLogoutEmployee{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Are you sure you want to delete?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <form method="post" action="{{url('/deleteEmployeeQuestion')}}">
          @csrf

          <input type="hidden" name="id" value="{{$item->id}}">

          <button type="submit" class="btn btn-danger">Yes</button>

        </form>
        
      </div>
    </div>
  </div>
</div>