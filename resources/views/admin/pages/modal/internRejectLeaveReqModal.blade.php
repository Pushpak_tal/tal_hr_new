<div class="modal fade" id="exampleModalCancel{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to reject?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>

      <form action="{{url('/rejectInternReq')}}" method="post">

        @csrf

        <input type="hidden" name="added_by" value="{{$item->added_by}}">
         <input type="hidden" name="id" value="{{$item->id}}">
         <input type="hidden" name="name" value="{{$item->name}}">

         <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        <button type="submit" class="btn btn-danger">Yes</button>
      </div>
      
      
      
      </form>
    </div>
  </div>
</div>