@extends('admin.layouts.home')
@section('content')


<div class="content-wrapper">

   @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif


    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Search Intern's requests</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Intern</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-danger">
            
              <div class="card-body">
                <form method="post" action="{{url('/searchInternDailyUpdate')}}">
                  @csrf
                  <div class="row">
                  <div class="col-5">
                    <input type="text" class="form-control" placeholder="Intern Id" name="intern_id">
                  </div>
                  <div class="col-5">
                    <input type="text" class="form-control" placeholder="Name" name="name">
                  </div>
                  <div class="col-2">
                    <button type="submit" class="btn btn-primary">Search</button>
                  </div>
                </div>
                </form>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

      

          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>


     <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Intern's requests for daily update</h1>
          </div>
          <div class="col-sm-6">
            
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    @if(!empty($search))


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

          
            
            <div class="card">
              <div class="card-header">

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>#</th>
                    <th>Employee Id</th>
                    <th>Name</th>
                    <th>Submission Date</th>
                    <th>Date</th>
                    <th>Task</th>
                    <th>Reason</th>
                    <th>Workplace</th>
                    <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $count=1;?>
                    @foreach($search as $item)
                  <tr>
                    <td>{{$count++}}</td>
                    <td>{{$item->added_by}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->date_of_submission}}</td>
                    <td>{{$item->date}}</td>
                    <td>{{$item->body}}</td>
                    <td>{{$item->reason}}</td>
                    
                    @if($item->task==0)

                    <td>Office</td>

                    @else

                    <td>Home</td>

                    @endif

                     <td>

                      @if($item->status=="1")

                      <button type="button" class="btn btn-secondary" disabled>Approved</button>


                      @elseif($item->status=="2")

                      <button type="button" class="btn btn-secondary" disabled>Rejected</button>

                      @else
                      
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$item->id}}">Approve</button>

                     
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCancel{{$item->id}}">Reject</button>
                      @endif
                    </td>
                    
                  </tr>

                  @include('admin.pages.modal.approveInternRequest')

                  @include('admin.pages.modal.rejectInternRequest')

                  @endforeach
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>


    @else


    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">

          
            
            <div class="card">
              <div class="card-header">

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>#</th>
                    <th>Employee Id</th>
                    <th>Name</th>
                    <th>Submission date</th>
                    <th>Date</th>
                    <th>Task</th>
                    <th>Reason</th>
                    <th>Workplace</th>
                    <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $count=1;?>
                    @foreach($getIntern as $item)
                  <tr>
                    <td>{{$count++}}</td>
                    <td>{{$item->added_by}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->date_of_submission}}</td>
                    <td>{{$item->date}}</td>
                    <td>{{$item->body}}</td>
                    <td>{{$item->reason}}</td>
                    
                    @if($item->task==0)

                    <td>Office</td>

                    @else

                    <td>Home</td>

                    @endif

                     <td>

                      @if($item->status=="1")

                      <button type="button" class="btn btn-secondary" disabled>Approved</button>


                      @elseif($item->status=="2")

                      <button type="button" class="btn btn-secondary" disabled>Rejected</button>

                      @else
                      
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$item->id}}">Approve</button>

                     
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCancel{{$item->id}}">Reject</button>
                      @endif
                    </td>
                    
                  </tr>

                  @include('admin.pages.modal.approveInternRequest')

                  @include('admin.pages.modal.rejectInternRequest')

                  @endforeach
                    
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>


    @endif

   
    
    <!-- /.content -->
  </div>

@endsection