@extends('admin.layouts.home')
@section('content')

<div class="content-wrapper">

  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Here are the F.A.Qs</h1>
          </div>
         
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <section class="content">
      <div class="container-fluid">
        <div class="row">

          @foreach($data as $item)
          <div class="col-md-12">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">

                <ul class="list-group list-group-unbordered mb-3">
                  <li style="list-style: none;">
                  	<h6>Message</h6>
                    <p><b>{{$item->message}}</b></p>
                  </li>
                  <li style="list-style: none;">
                    <h6>Sended By</h6> 
                    <p><span style="font-weight: bold;">{{$item->sended_to}}</span>
                  </li>
                  
                </ul>

                <?php  

                $query=App\ceoMessageReply::where('message_id',$item->id)->count();

                ?>


                    <a href="{{url('/viewReplyMessageToAdmin/' .$item->id)}}" class="btn btn-success">Replies ({{$query}})</a>
              
              </div>
              <!-- /.card-body -->


            </div>



          </div>


           @endforeach
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

</div>


@endsection