@extends('admin.layouts.home')
@section('content')

<div class="content-wrapper">

  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Here are the F.A.Qs</h1>
          </div>
         
        </div>
      </div><!-- /.container-fluid -->
    </section>


 <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-danger">
            
              <div class="card-body">
                <form method="post" action="{{url('/searchQuery')}}">
                  @csrf
                  <div class="row">
                  <div class="col-10">
                    <input type="text" class="form-control" placeholder="Search Your Query" name="query" required>
                  </div>
                 
                  <div class="col-2">
                    <button type="submit" class="btn btn-primary">Search</button>
                  </div>
                </div>
                </form>
                
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

      

          </div>
          <!--/.col (left) -->
          <!-- right column -->
         
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>


    @if(isset($querySearch))

    <section class="content">
      <div class="container-fluid">
        <div class="row">

          @foreach($querySearch as $item)
          <div class="col-md-12">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">

                <ul class="list-group list-group-unbordered mb-3">
                  <li style="list-style: none;">
                    <h4><b>{{$item->title}}</b></h4>
                    <p>{{$item->body}}</p>
                  </li>
                  <li style="list-style: none;">
                    <h6><b>Posted by</b></h6> 
                    <p><span style="font-weight: bold;">{{$item->name}}</span> ({{$item->user_id}}) <span style="font-size: small;color: gray;font-weight: 600;">{{$item->team}}</span></p>
                  </li>
                  <li style="list-style: none;">
                     <h6><b>Time</b></h6> 
                    <p>{{$item->created_at}}</p>
                  </li>

                  <li style="list-style: none;">
                    <span class="badge badge-primary">{{$item->tag}}</span>

                  </li>

                  
                </ul>

                <?php  

                $query=App\QueryAnswer::where('query_id',$item->id)->count();

                ?>


                    <a href="{{url('/queryAnswer/' .$item->id)}}" class="btn btn-success">Answer ({{$query}})</a>
              
              </div>
              <!-- /.card-body -->


            </div>



          </div>


           @endforeach
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

    @else

    <section class="content">
      <div class="container-fluid">
        <div class="row">

          @foreach($showQuery as $item)
          <div class="col-md-12">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">

                <ul class="list-group list-group-unbordered mb-3">
                  <li style="list-style: none;">
                    <h4><b>{{$item->title}}</b></h4>
                    <p>{{$item->body}}</p>
                  </li>
                  <li style="list-style: none;">
                    <h6><b>Posted by</b></h6> 
                    <p><span style="font-weight: bold;">{{$item->name}}</span> ({{$item->user_id}}) <span style="font-size: small;color: gray;font-weight: 600;">{{$item->team}}</span></p>
                  </li>
                  <li style="list-style: none;">
                     <h6><b>Time</b></h6> 
                    <p>{{$item->created_at}}</p>
                  </li>

                  <li style="list-style: none;">
                    <span class="badge badge-primary">{{$item->tag}}</span>

                  </li>

                  
                </ul>

                <?php  

                $query=App\QueryAnswer::where('query_id',$item->id)->count();

                ?>


                    <a href="{{url('/queryAnswer/' .$item->id)}}" class="btn btn-success">Answer ({{$query}})</a>
              
              </div>
              <!-- /.card-body -->


            </div>



          </div>


           @endforeach
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

    @endif

</div>

@endsection