@extends('admin.layouts.home')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Our Employee</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User Employee</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    

    <section class="content">
      <div class="container-fluid">
        <div class="row">

          @foreach($request as $item)
          <div class="col-md-4">

            @php

            $empDetail=App\EmployeeDetails::where('username' ,$item->username)->first();


            @endphp

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('/upload/employee/'.$empDetail->image)}}"
                       alt="User profile picture">
                </div>

                <p class="text-muted text-center">{{$item->username}}</p>
                <p class="text-muted text-center" style="margin-top:-5%;">{{$empDetail->name}}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Field Name</b> <a class="float-right">{{$item->field_name}}</a>
                  </li>

                  @if($item->field_name=='Image')

                  <li class="list-group-item">
                    <b>Old Value</b> <a class="float-right"><img class="profile-user-img img-fluid img-circle"
                       src="{{asset('/upload/employee/'.$item->old_value)}}"
                       alt="User profile picture" style="width: 50px;"></a>
                  </li>
                  <li class="list-group-item">
                    <b>New Value</b> <a class="float-right"><img class="profile-user-img img-fluid img-circle"
                       src="{{asset('/upload/employee/'.$item->new_value)}}"
                       alt="User profile picture" style="width: 50px;"></a>
                  </li>

                  @else

                  <li class="list-group-item">
                    <b>Old Value</b> <a class="float-right">{{$item->old_value}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>New Value</b> <a class="float-right">{{$item->new_value}}</a>
                  </li>

                  @endif
                  
                </ul>

              <td>

                      @if($item->status=="1")

                      <button type="button" class="btn btn-secondary" disabled>Approved</button>


                      @elseif($item->status=="2")

                      <button type="button" class="btn btn-secondary" disabled>Rejected</button>

                      @else
                      
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$item->temp_employee_id}}">Approve</button>

                     
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCancel{{$item->temp_employee_id}}">Reject</button>
                      @endif
                    </td>
              </div>
              <!-- /.card-body -->
            </div>

          </div>

            @include('admin.pages.modal.approveEmpProfileReqModal')

                  @include('admin.pages.modal.cancelEmpProfileReqModal')


           @endforeach
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

   
  </div>

  

@endsection
