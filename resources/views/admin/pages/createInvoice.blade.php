@extends('admin.layouts.home')
@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Employee daily update</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

      @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif



          
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">

        
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Create an Invoice</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{url('/saveInvoice')}}" id="empDreport" method="post">
                @csrf
                <div class="card-body">
                  <div class="form-group">
                  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-lg"style="float: right;">Add</button>


                  	<div class="row">

                  		<div class="col-md-6">
                  			<label for="exampleInputEmail1">Employee</label>
			                    <select class="form-control" id="dropdown" name="customer_id">
			                      <option selected disabled>---Select a Customer---</option>
                            @foreach($getCustomer as $item)

                            <option value="{{$item->id}}">{{$item->name}} [{{$item->business_name}}]</option>
                            @endforeach
			                     
			                    </select>
                  		</div>

                  		<div class="col-md-6">
                  			<a href="{{url('/invoiceList')}}" class="btn btn-primary" style="float:right;">See list</a>
                  		</div>
                  	</div>
                    
                  </div>
                  <div class="form-group">
                  	<div class="row">
                  		<div class="col-md-6">
                  			<label for="exampleInputPassword1">SAC No.</label>
                    		<input type="text" name="sac" value="{{ old('sac') }}" class="form-control" placeholder="SAC Number" required>
                  		</div>
                  	</div>
                    
                  </div>

                  <div class="form-group">
                    <div class="row">
                    	<div class="col-md-3">
                    		<label>Invoice Date</label>
                    		<input type="date"value="{{ old('invoice_date') }}" name="invoice_date" class="form-control" required>
                    	</div>

                    	<div class="col-md-3">
                    		<label>Rate</label>
                    		<input id="input-rate" type="text" name="rate" value="{{ old('rate') }}" class="form-control" placeholder="Rate" required onchange="cal_total()">
                    	</div>

                    	<div class="col-md-3">
                    		<label>Quantity</label>
                    		<input id="input-qty" type="text" value="{{ old('qty') }}" name="qty" placeholder="Qty" class="form-control" required onchange="cal_total()">
                    	</div>

                    	<div class="col-md-3">
                    		<label>Total</label>
                    		<input id="total" type="text" value="{{ old('total') }}" name="total" placeholder="Total" class="form-control" readonly>
                    	</div>
                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                    	<div class="col-md-3">
                    		<label>Discount %</label>
                    		<input id="input-discount" type="text" name="discount" value="0" class="form-control" required onclick="cal_subtotal()">
                    	</div>

                    	<div class="col-md-3">
                    		<label>GST Type</label>
                    		<select id="input-address" class="form-control" name="gst_type" required>
                    			<option selected value="0">Intra State</option>
                    			<option value="1">Inter State</option>
                    		</select>
                    	</div>

                    	<div class="col-md-3">
                    		<label>GST</label>
                    		<select id="input-gst" class="form-control" name="gst_percentage" required onclick="cal_subtotal()">
                    			<option selected value="0">0%</option>
                    			<option value="5">5%</option>
                    			<option value="12">12%</option>
                    			<option value="18">18%</option>
                    			<option value="28">28%</option>
                    		</select>
                    	</div>

                    </div>
                    
                  </div>
                  <input type="hidden" value="" id="gst_amt" name="gst_amt">

                  <div class="form-group">
                    <div class="row">
                    	<div class="col-md-3">
                        <label>Sub Total</label>
                    		<input id="input-subtotal" type="text" name="subtotal" class="form-control" placeholder="Sub Total" readonly required>
                    	</div>

                    	<div class="col-md-3">
                    		<label>Paid Amount</label>
                    		<input id="input-amt" type="text" name="amt" class="form-control" onchange="caldue()" placeholder="Amount" required="">
                    	</div>

                    	<div class="col-md-3">
                    		<label>Due</label>
                    		<input id="input-due" type="text" placeholder="Due" name="due" class="form-control" required>
                    	</div>

                    </div>
                  </div>

                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-12">
                        <label>Description</label>
                        <textarea class="form-control" value="{{ old('description') }}" name="description" placeholder="Description ..." required="" ></textarea>
                      </div>
                    </div>
                  </div>

                    <button type="submit" class="btn btn-primary" id="button">Save</button>

                </div>
                <!-- /.card-body -->

             
              </form>
            </div>

           

          </div>
          
        </div>
      </div>
    </section>
  </div>

   @include('admin.pages.modal.addCustomer')


@endsection