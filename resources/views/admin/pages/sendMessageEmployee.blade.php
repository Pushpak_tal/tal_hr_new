@extends('admin.layouts.home')
@section('content')

<div class="content-wrapper">


   
    <section class="content">

      <div class="container-fluid">
        <div class="row">


          <!-- left column -->
          <div class="col-md-12" style="margin-top: 3%;">
            <a href="{{url('/viewMessageReplies')}}" class="btn btn-success" style="margin-top: -3%;">View Replies</a>

          	 @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            <!-- general form elements -->
            <div class="card card-primary">

              <div class="card-header">
                <h3 class="card-title">Send Message to employee's inbox</h3>
              </div>

              @php


              $empUsername = App\EmployeeDetails::all();


              @endphp
             
              <form role="form" method="post" action="{{url('/saveAdminMessageToEmployee')}}">
              	@csrf

                <div class="card-body">
                  <div class="form-group">

                  	<label>Select Employee</label>
                  	<select class="form-control" name="employee_username">
                  		@foreach($empUsername as $emp)
                  			<option value="{{$emp->username}}">{{$emp->name}}[{{$emp->username}}]</option>
                  		@endforeach
                  	</select>
                   
                  </div>

                  <div class="form-group">

                  	<label>Write your message</label>
                  	<textarea name="message" class="form-control" placeholder="Write your message here..." required></textarea>
                   
                  </div>

               
                  <button type="submit" class="btn btn-primary">Submit</button>
                
              </form>
            </div>
        
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


@endsection