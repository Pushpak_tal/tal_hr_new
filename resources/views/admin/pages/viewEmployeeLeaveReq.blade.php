@extends('admin.layouts.home')
@section('content')


<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Employee Leave Request</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Employee</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif


    <section class="content">
      <div class="container-fluid">
        <div class="row">

          @foreach($emp_detail as $item)
          <div class="col-md-4">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                

                <h3 class="profile-username text-center">{{$item->name}}</h3>

                <p class="text-muted text-center">{{$item->added_by}}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Start Date</b> <a class="float-right">{{$item->start_date}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Start Time</b> <a class="float-right">{{$item->start_time}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>End Date</b> <a class="float-right">{{$item->end_date}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>End Time</b> <a class="float-right">{{$item->end_time}}</a>
                  </li>

                  <li class="list-group-item">
                    <b>Reason</b> <a class="float-right">{{$item->reason}}</a>
                  </li>

                  
                </ul>

               <td>

                      @if($item->status=="1")

                      <button type="button" class="btn btn-secondary" disabled>Approved</button>


                      @elseif($item->status=="2")

                      <button type="button" class="btn btn-secondary" disabled>Rejected</button>

                      @else
                      
                      <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$item->id}}">Approve</button>

                     
                      <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#exampleModalCancel{{$item->id}}">Reject</button>
                      @endif
                    </td>
              </div>
              <!-- /.card-body -->
            </div>

          </div>

          @include('admin.pages.modal.empApproveLeaveReqModal')
          @include('admin.pages.modal.empRejectLeaveReqModal')

           @endforeach
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>


    

    

   
  </div>

@endsection