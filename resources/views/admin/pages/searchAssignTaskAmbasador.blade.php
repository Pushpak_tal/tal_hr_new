<!DOCTYPE html> 
<html>
<head>
	<title></title>
</head>

<style>
	.check{
		margin-top: 1%;
		margin-left: 13px;
	}
</style>
<body>

	<div class="container">
		<div class="row">
			<form action="{{url('/assignTaskAmbasador')}}" method="post">
				@csrf
				<table class="table">
					@if(isset($name))

					@if(count($name) == "0")

					<h4 style="margin-top: 18%;margin-left: 47%;"><span class="badge bg-danger" style="width: 650%;">No matching records found</span></h4>
					
					
					@else
					@foreach($name as $names)
						<tr>
							<td><input type="checkbox" class="checkbox" name="username[]" value="{{$names->username}}"> {{$names->name}} (College: {{$names->college}}, Email: {{$names->email}}, Phone: {{$names->phone}}, State: {{$names->state}}, District: {{$names->district}}, City: {{$names->city}}, Pin: {{$names->pin}})
							</td>	
						</tr>
					@endforeach

						<div class="check">
							<input type="checkbox" id="select_all">
						 	<label>Select all</label>
						</div>	

					@endif 

					@elseif(isset($state))

          @if(count($state) == "0")

          <h4 style="margin-top: 18%;margin-left: 47%;"><span class="badge bg-danger" style="width: 650%;">No matching records found</span></h4>

          @else

          @foreach($state as $states)

            <tr>
              <td><input type="checkbox" class="checkbox" name="username[]" value="{{$states->username}}"> {{$states->name}} (College: {{$states->college}}, Email: {{$states->email}}, Phone: {{$states->phone}}, State: {{$states->state}}, District: {{$states->district}}, City: {{$states->city}}, Pin: {{$states->pin}})</td>
            </tr>
          @endforeach

            <div class="check">
              <input type="checkbox"  id="select_all">
              <label>Select all</label>
            </div>

          @endif

          @elseif(isset($college))

          @if(count($college) == "0")

          <h4 style="margin-top: 18%;margin-left: 47%;"><span class="badge bg-danger" style="    width: 650%;">No matching records found</span></h4>

          @else

          @foreach($college as $colleges)

            <tr>
              <td><input type="checkbox" class="checkbox" name="username[]" value="{{$colleges->username}}"> {{$colleges->name}} (College: {{$colleges->college}}, Email: {{$colleges->email}}, Phone: {{$colleges->phone}}, State: {{$colleges->state}}, District: {{$colleges->district}}, City: {{$colleges->city}}, Pin: {{$colleges->pin}})</td>
            </tr>
          @endforeach

          <div class="check">
              <input type="checkbox" id="select_all">
              <label>Select all</label>
            </div>

          @endif

          @elseif(isset($district))

          @if(count($district) == "0")

          <h4 style="margin-top: 18%;margin-left: 47%;"><span class="badge bg-danger" style="    width: 650%;">No matching records found</span></h4>

          @else


          @foreach($district as $dist)

            <tr>
              <td><input type="checkbox" class="checkbox" name="username[]" value="{{$dist->username}}"> {{$dist->name}} (dist: {{$dist->college}}, Email: {{$dist->email}}, Phone: {{$dist->phone}}, State: {{$dist->state}}, District: {{$dist->district}}, City: {{$dist->city}}, Pin: {{$dist->pin}})</td>
            </tr>
          @endforeach

          <div class="check">
              <input type="checkbox"  id="select_all">
              <label>Select all</label>
            </div>

          @endif

          @elseif(isset($city))

          @if(count($city) == "0")

          <h4 style="margin-top: 18%;margin-left: 47%;"><span class="badge bg-danger" style="    width: 650%;">No matching records found</span></h4>

          @else

          @foreach($city as $cities)

            <tr>
              <td><input type="checkbox" class="checkbox" name="username[]" value="{{$cities->username}}"> {{$cities->name}} (dist: {{$cities->college}}, Email: {{$cities->email}}, Phone: {{$cities->phone}}, State: {{$cities->state}}, District: {{$cities->district}}, City: {{$cities->city}}, Pin: {{$cities->pin}})</td>
            </tr>
          @endforeach

          <div class="check">
              <input type="checkbox" id="select_all">
              <label>Select all</label>
          </div>

          @endif

          @elseif(isset($data))

          @foreach($data as $datas)

          <tr>
            <td><input type="checkbox" class="checkbox" name="username[]" value="{{$datas->username}}"> {{$datas->name}} (dist: {{$datas->college}}, Email: {{$datas->email}}, Phone: {{$datas->phone}}, State: {{$datas->state}}, District: {{$datas->district}}, City: {{$datas->city}}, Pin: {{$datas->pin}})</td>

          </tr>

          @endforeach

          <div class="check">
              <input type="checkbox" id="select_all">
              <label>Select all</label>
          </div>


		@endif
				</table>

        <button type="submit" name="" class="btn btn-success" id="submit_prog">Assign</button>
		</form>

		</div>
	</div>

	<script type="text/javascript">
$(document).ready(function(){
    $('#select_all').click(function(){
        if(this.checked){
            $('.checkbox').each(function(){
                this.checked = true;
            });
        }else{
             $('.checkbox').each(function(){
                this.checked = false;
            });
        }
    });
    
    $('.checkbox').click(function(){
        if($('.checkbox:checked').length == $('.checkbox').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
    });
});
</script>

<script>
	    $(document).ready(function() {
    
        var $submit = $("#submit_prog").hide(),
            $cbs = $('#select_all').click(function() {
                $submit.toggle( $cbs.is(":checked") );
            });
    
    });

    $(document).ready(function() {
    
        var $submit = $("#submit_prog").hide(),
            $cbs = $('input[name="username[]"]').click(function() {
                $submit.toggle( $cbs.is(":checked") );
            });
    
    });
</script>

</body>
</html>