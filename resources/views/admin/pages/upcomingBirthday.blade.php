@extends('admin.layouts.home')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Employees Upcoming Birthday's</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Birthday</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    @if(isset($birthday))


    <section class="content">
      <div class="container-fluid">
        <div class="row">

          @foreach($birthday as $item)
          <div class="col-md-4">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  <img class="profile-user-img img-fluid img-circle"
                       src="{{asset('/upload/employee/'.$item->image)}}"
                       alt="User profile picture">
                </div>

                <h3 class="profile-username text-center">{{$item->name}}</h3>

                <p class="text-muted text-center">{{$item->username}}</p>

                <ul class="list-group list-group-unbordered mb-3">
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{$item->email}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Phone</b> <a class="float-right">{{$item->phone}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Date of Birth</b> <a class="float-right">{{$item->dob}}</a>
                  </li>


                </ul>

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$item->employee_id}}">Wish</button>


              </div>
              <!-- /.card-body -->
            </div>

          </div>

          @include('admin.pages.modal.empBirthday')

         

           @endforeach
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

    @endif

    

   
  </div>


@endsection