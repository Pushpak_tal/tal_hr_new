@extends('admin.layouts.home')
@section('content')

<div class="content-wrapper">
   
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12" style="margin-top: 3%;">

          	 @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            <!-- general form elements -->
            <div class="card card-primary">

              <div class="card-header">
                <h3 class="card-title">Give Task to campus ambasador</h3>
              </div>
             
              <form role="form" method="post" action="{{url('/assginTaskToAmbasador')}}">
              	@csrf

              	<input type="hidden" name="ambasador_username" value="{{$ambasadorUsername}}">

                <div class="card-body">
                  <div class="form-group">

                  <div class="form-group">

                    <label>Task</label>
                    <textarea name="task" class="form-control" placeholder="Write detail here" required></textarea>
                   
                  </div>

                  <div class="form-group">

                    <label>Task Type</label>
                    <select name="task_type" class="form-control" required>
                      <option value="compulsory">Compulsory</option>
                      <option value="optional">Optional</option>
                    </select>
                   
                  </div>



                  	<label>Points</label>
                  	<input type="number" name="points" class="form-control" placeholder="Enter points for task" required>
                   
                  </div>

                  <div class="form-group">

                  	<label>Date of submission</label>
                  	<input type="date" name="submission_date" class="form-control" required>
                   
                  </div>
                 

               
                  <button type="submit" class="btn btn-primary">Assign</button>
                
              </form>
            </div>
        
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


@endsection