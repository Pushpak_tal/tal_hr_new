<li class="nav-item dropdown">
        
        <a class="nav-link" data-toggle="dropdown" href="#" id="notification">
          <i class="far fa-bell"></i>
          @php

          $notiCount=App\Notification::where('sended_to',\Session::get('adminname'))->where('status',0)->count();

          $detail=App\Notification::where('sended_to',\Session::get('adminname'))->first();

          @endphp

          @if(!empty($notiCount))
          <span class="badge badge-warning navbar-badge" id="output">{{$notiCount}}</span>
          <input type="hidden" name="username" id="username" value="<?php echo Session::get('adminname') ?>">

          @else

          <span></span>

          @endif
        </a>

        
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">{{$notiCount}} Notifications</span>

          @php

            $getNoti = App\Notification::where('sended_to',\Session::get('adminname'))->orderBy('id','desc')->take(10)->get();

          @endphp

          @foreach($getNoti as $get)

          @if($get->field=='daily task request')

              <a href="{{url('/fullViewAdminNotification')}}" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i>Your daily task request has been...</a>

          @elseif($get->field=='Inbox Message')

              <a href="{{url('/fullViewAdminNotification')}}" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i>Check your inbox...</a>

          @elseif($get->field=='Message Reply')

              <a href="{{url('/fullViewAdminNotification')}}" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i>You recived a message check inbox..</a>

          @else

              <a href="{{url('/fullViewAdminNotification')}}" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i>Your profile request has been...</a>

          @endif

          @endforeach
    
          

          

          <a href="{{url('/fullViewAdminNotification')}}" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
