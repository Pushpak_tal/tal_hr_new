 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{asset('./upload/logo.png')}}" class="brand-image img-circle elevation-3"
           style="opacity: .8;">
      <span class="brand-text font-weight-light">Think Again Lab</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('./upload/admin.png')}}" class="img-circle elevation-2">
        </div>
        <div class="info">
          <a href="{{url('/demoAdmin')}}" class="d-block">Admin</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

        <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-comments" aria-hidden="true"></i>


              <p>
               Ceo's Inbox
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/sendMessageEmployee')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Employee</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/sendMessageIntern')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Intern</p>
                </a>
              </li>
            </ul>
          </li>
         
         <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-book" aria-hidden="true"></i>


              <p>
               Knowledge Bank
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/adminConcern')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>My Concern</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/adminQuerySolution')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Find Solution</p>
                </a>
              </li>
            </ul>
          </li>
         
          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-users" aria-hidden="true"></i>

              <p>
               Team Members
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/employeeDetail')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Employee Detail</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/internDetail')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Intern Detail</p>
                </a>
              </li>

              <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-graduation-cap" aria-hidden="true"></i>


              <p>
               Campus Ambasador
                <i class="right fas fa-angle-left"></i>

              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/ambasadorDetails')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>

                  <p>Ambasador Detail</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{url('/assignTask')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>

                  <p>Assign Task</p>
                </a>
              </li>
            
            </ul>
          </li>

              
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-user-plus" aria-hidden="true"></i>

              <p>
               Profile Request Update
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/employeeProfileUpdateReq')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Employee</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/internProfileUpdateReq')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Intern</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/ambasadorProfileUpdateReq')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Ambasador</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-calendar" aria-hidden="true"></i>


              <p>
               Daily Update
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/employeeDailyUpate')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Employee</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/internDailyUpdate')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Intern</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">

              <i class="nav-icon fa fa-wrench" aria-hidden="true"></i>
              <p>
               Update Requests
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/viewUpdateRequestEmployee')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Employee</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/viewUpdateRequestintern')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Intern</p>
                </a>
              </li>
            </ul>
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-signal" aria-hidden="true"></i>


              <p>
               Performance Appraisal
                <i class="right fas fa-angle-left"></i>

              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/appraisalEmployee')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Employee</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/appraisalIntern')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Intern</p>
                </a>
              </li>
            </ul>
          </li>

           <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-file" aria-hidden="true"></i>



              <p>
               Account
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/invoice')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Invoice</p>
                </a>
              </li> -->
              <!-- <li class="nav-item">
                <a href="{{url('/internDetail')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Intern Detail</p>
                </a>
              </li> -->
           <!--  </ul>
          </li> -->

           <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-birthday-cake" aria-hidden="true"></i>


              <p>
               Ceo's In
                <i class="right fas fa-angle-left"></i>

              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/upcomingBirthday')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Employee</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/upcomingBirthdayIntern')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Intern</p>
                </a>
              </li>
            </ul>
          </li> -->

        <!--   <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-child" aria-hidden="true"></i>

              <p>
               Leave Request
                <i class="right fas fa-angle-left"></i>

              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/viewEmployeeLeaveReq')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Employee</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/viewInternLeaveReq')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Intern</p>
                </a>
              </li>
            </ul>
          </li> -->

          <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-graduation-cap" aria-hidden="true"></i>


              <p>
               Campus Ambasador
                <i class="right fas fa-angle-left"></i>

              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/ambasadorDetails')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>

                  <p>Ambasador Detail</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{url('/assignTask')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>

                  <p>Assign Task</p>
                </a>
              </li>
            
            </ul>
          </li> -->

        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>