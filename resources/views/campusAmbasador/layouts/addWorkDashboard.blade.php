<!DOCTYPE html>
<html lang="en">
<head>
  

  <title>TAL | Dashboard</title>
  <link rel="icon" type="image/png" href="./upload/logo.png"/>

  @include('employee.includes.headerlinks')

  
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">


  @include('employee.includes.navbar')
  @include('employee.includes.sidebar')
<div class="content-wrapper">

  @php 

  $emp=App\empBirthday::whereMonth('dob', '=', Carbon\Carbon::now()->format('m'))->whereDay('dob', '=', Carbon\Carbon::now()->format('d'))->first();


  @endphp

   @if(isset($emp))
            <marquee>
               <label style="margin-top: 1%;">{{$emp->message}}</label>
            </marquee>

        @endif
   
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12" style="margin-top: 3%;">

          	 @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            <!-- general form elements -->
            <div class="card card-primary">

              <div class="card-header">
                <h3 class="card-title">Hi {{Session::get('employeename')}}, Hope you've enjoyed working whole day</h3>
              </div>
             
              <form role="form" method="post" action="{{url('/employeeWorkDetail')}}">
              	@csrf

                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">You are posting report for</label>
                     <input style="text-align: center; background-color: white;border:none;" type="text" disabled="" value="<?php echo \Carbon\Carbon::now()->toDateString(); ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Your work details</label>
                    <textarea name="body" class="form-control" placeholder="What you've done today?" required></textarea>
                  </div>

               
                  <button type="submit" class="btn btn-primary">Submit</button>
                
              </form>
            </div>
        
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @include('employee.includes.rightsidebar')
 
 @include('employee.includes.footer')

 @include('employee.includes.footerlinks')

  
</div>






</body>
</html>
