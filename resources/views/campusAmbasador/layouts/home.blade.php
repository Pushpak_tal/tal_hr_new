<!DOCTYPE html>
<html lang="en">
<head>
  

  <title>TAL | Dashboard</title>
  <link rel="icon" type="image/png" href="./upload/logo.png"/>

  @include('campusAmbasador.includes.headerlinks')

  
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">


  @include('campusAmbasador.includes.navbar')
  @include('campusAmbasador.includes.sidebar')
  @yield('content')
  @include('campusAmbasador.includes.rightsidebar')
 
 @include('campusAmbasador.includes.footer')

 @include('campusAmbasador.includes.footerlinks')

  
</div>

<script>
        $(document).ready(function(){

    $(document).on("click", "#notification", function() { 
		$.ajax({
			url: '/changeNotificationStatusAmbasador',
			type: "POST",
			dataType: "json",
			data:{
                _token:'{{ csrf_token() }}',
				username: $('#username').val(),
			},
			success: function(dataResult){
                $('#output').load(document.URL +  ' #output');;
			},
			error: function (dataResult) {
                 alert(dataResult);
           }
		});
	}); 
});

    </script>


</body>
</html>
