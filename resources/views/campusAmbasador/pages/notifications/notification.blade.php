<li class="nav-item dropdown">
        
        <a class="nav-link" data-toggle="dropdown" href="#" id="notification">
          <i class="far fa-bell"></i>
          @php

          $notiCount=App\Notification::where('sended_to',\Session::get('ambasadorsession'))->where('status',0)->count();

          $detail=App\Notification::where('sended_to',\Session::get('ambasadorsession'))->first();

          @endphp

          @if(!empty($notiCount))
          <span class="badge badge-warning navbar-badge" id="output">{{$notiCount}}</span>
          <input type="hidden" name="username" id="username" value="<?php echo Session::get('ambasadorsession') ?>">

          @else

          <span></span>

          @endif
        </a>

        
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <span class="dropdown-item dropdown-header">{{$notiCount}} Notifications</span>

          @php

            $getNoti = App\Notification::where('sended_to',\Session::get('ambasadorsession'))->orderBy('id','desc')->take(10)->get();

          @endphp

          @foreach($getNoti as $get)

            <a href="{{url('/fullViewAmbasadorNotification')}}" class="dropdown-item">
              <i class="fas fa-envelope mr-2"></i>Your profile request has been...</a>

          @endforeach
    
          

          

          <a href="{{url('/fullViewAmbasadorNotification')}}" class="dropdown-item dropdown-footer">See All Notifications</a>
        </div>
      </li>
