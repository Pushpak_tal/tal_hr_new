@extends('campusAmbasador.layouts.home')
@section('content')

 <div class="content-wrapper">
   
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          
          <div class="col-md-12" style="margin-top: 3%;">
            <div class="card">
              
              <div class="card-body p-0">
                <table class="table">
                  <thead>
                    <tr>
                      <th style="width: 10px">#</th>
                      <th>Task</th>
                    </tr>
                  </thead>
                  <tbody>
                  	@php

                  		$taskDetail=App\AssignTaskAmbasador::where('ambasador_username',Session::get('ambasadorsession'))->orderBy('id','desc')->get();

                  	 $count=1;

                  	@endphp



                  	@foreach($taskDetail as $tasks)
                    <tr>
                      <td>{{$count++}}</td>
                      <td>{{$tasks->task}}</td>
                      
                    </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
           
          </div>
          <!-- /.col -->
        </div>
        
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection