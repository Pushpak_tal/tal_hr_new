<!DOCTYPE html>
<html>
<head>
    <title>TAL | Campus Ambasador</title>
    <link rel="stylesheet" type="text/css" href="form/css/style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

</head>
<body>

    <div class="container">
        <div class="row">
            <div class="col-md-10 offset=md-1">
                <div class="row">
                    <div class="col-md-5 register-left">
                        <img src="form/img/logo.png">
                        <h3>Contact Us</h3>
                        <p>Think Again Lab authorized expert Thinker will visit your campus and provide Workshop</p>

                        <a href="https://thinkagainlab.com/" class="btn btn-primary">Click Here</a>
                    </div>
                    <div class="col-md-7 register-right">
                        <h2>Register Here</h2>

                        <form method="POST" action="{{url('/ambasadorRegisterService')}}" enctype="multipart/form-data">
                            @csrf

                            <div class="register-form">

                                @if(Session::has('message'))
                                <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
                                @endif
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" placeholder="Enter Name" required>
                            </div>

                            <p style="color:red;">{{ $errors->first('name') }}</p>

                            <div class="form-group">
                                <input type="text" name="email" class="form-control" placeholder="Enter Email" required>
                            </div>

                            <p style="color:red;">{{ $errors->first('email') }}</p>

                            <div class="form-group">
                                <input type="text" name="college" class="form-control" placeholder="Enter College" required>
                            </div>

                            <p style="color:red;">{{ $errors->first('college') }}</p>

                            <div class="form-group">
                                <input type="text" name="phone" class="form-control" placeholder="Enter Phone Number" required>
                            </div>

                            <p style="color:red;">{{ $errors->first('phone') }}</p>

                            <div class="form-group">
                                <input type="text" name="state" class="form-control" placeholder="Enter State" required>
                            </div>

                            <p style="color:red;">{{ $errors->first('state') }}</p>

                            <div class="form-group">
                                <input type="text" name="district" class="form-control" placeholder="Enter District" required>
                            </div>

                            <p style="color:red;">{{ $errors->first('district') }}</p>

                            <div class="form-group">
                                <input type="text" name="city" class="form-control" placeholder="Enter City" required>
                            </div>

                            <p style="color:red;">{{ $errors->first('city') }}</p>

                            <div class="form-group">
                                <input type="text" name="pin" class="form-control" placeholder="Enter Pin" required>
                            </div>

                            <p style="color:red;">{{ $errors->first('pin') }}</p>

                            <div class="form-group">
                                <label>Profile Image</label>
                                <input type="file" name="image" class="form-control" required>
                            </div>

                            <p style="color:red;">{{ $errors->first('image') }}</p>

                            <div class="form-group">
                                <label>Id Proof</label>
                                <input type="file" name="id_proof" class="form-control" required>
                            </div>

                            <p style="color:red;">{{ $errors->first('id_proof') }}</p>

                            <div class="form-group">
                                <label>Address Proof</label>
                                <input type="file" name="address_proof" class="form-control" required>
                            </div>

                            <p style="color:red;">{{ $errors->first('address_proof') }}</p>
                            <button type="submit" class="btn btn-primary">Register</button>
                        </div>
                        </form>

                        
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
</html>