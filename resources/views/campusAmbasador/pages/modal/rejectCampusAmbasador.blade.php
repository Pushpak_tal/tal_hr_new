<div class="modal fade" id="exampleModalCancel{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel"> Are you sure you want to approve the employee?</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <form method="post" action="{{url('/rejectStatusAmbasador')}}">
          @csrf

      <div class="modal-body">
        <textarea class="form-control" name="reason" placeholder="write your reason here.." required></textarea>
      </div>
      
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">No</button>
        

          <input type="hidden" value="{{$item->id}}" name="id">
          <input type="hidden" value="{{$item->name}}" name="name">
          <input type="hidden" name="email" value="{{$item->email}}">
          <button type="submit" class="btn btn-danger">Yes</button>

        </form>
        
      </div>
    </div>
  </div>
</div>