<div class="modal fade" id="exampleModal{{$task->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      <form method="post" action="{{url('/ambasadorUpdateStatus')}}" enctype="multipart/form-data">

        @csrf

        <input type="hidden" value="{{$task->id}}" name="task_id">

        <label>Task Status</label>

        <select class="form-control" name="task_status" required>
          <option value="on going">On Going</option>
          <option value="pending">Pending</option>
          <option value="completed">Completed</option>
        </select><br>

        <label>Comment</label>
        <textarea name="comment" class="form-control"></textarea><br>

        <label>Add Attachment</label>
        <input type="file" name="attachment" class="form-control">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Update</button>
      </div>
      </form>
    </div>
  </div>
</div>