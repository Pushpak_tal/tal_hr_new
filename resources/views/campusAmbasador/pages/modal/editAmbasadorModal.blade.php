<div class="modal fade" id="exampleModal{{$getDetail->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Profile</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">

        <form action="{{url('/tempSaveAmbasador')}}" method="post" enctype="multipart/form-data">

          @csrf

          <input type="hidden" value="{{$getDetail->image}}" name="old_image">

          <input type="hidden" value="{{$getDetail->username}}" name="username" class="form-control">
          <input type="hidden" name="id" value="{{$getDetail->id}}">
          <input type="hidden" value="{{$getDetail->image}}" name="current_image">

          <div class="text-center">
            <img class="profile-user-img img-fluid img-circle" src="{{asset('/upload/ambasador/' .$getDetail->image)}}"><br>

          <input type="file" name="image" style="display: none;" value="{{$getDetail->image}}" id="file-upload">

          <label for="file-upload" class="custom-file-upload" style="cursor: pointer;">
            <i style="color: black;font-size: larger;margin-top: 30%;" class="fa fa-edit fa-2x" aria-hidden="true"></i>
          </label>



          </div>
          
          <br>


          <label>Name</label>
          <input type="hidden" name="old_name" value="{{$getDetail->name}}" class="form-control">
          <input type="text" name="name" value="{{$getDetail->name}}" class="form-control">

          <label>Email</label>
          <input type="hidden" name="old_email" value="{{$getDetail->email}}" class="form-control">
          <input type="email" name="email" value="{{$getDetail->email}}" class="form-control">

          <label>Mobile Number</label>
          <input type="hidden" name="old_phone" value="{{$getDetail->phone}}" class="form-control">
          <input type="number" name="phone" value="{{$getDetail->phone}}" class="form-control">

          <label>State</label>
          <input type="hidden" name="old_state" value="{{$getDetail->state}}" class="form-control">
          <input type="text" name="state" value="{{$getDetail->state}}" class="form-control">

          <label>District</label>
          <input type="hidden" name="old_district" value="{{$getDetail->district}}" class="form-control">
          <input type="text" name="district" value="{{$getDetail->district}}" class="form-control">

          <label>City</label>
          <input type="hidden" name="old_city" value="{{$getDetail->city}}" class="form-control">
          <input type="text" name="city" value="{{$getDetail->city}}" class="form-control">

          <label>Pin</label>
          <input type="hidden" name="old_pin" value="{{$getDetail->pin}}" class="form-control">
          <input type="number" name="pin" value="{{$getDetail->pin}}" class="form-control">
          
        
          

                <!-- <a href="#" class="btn btn-primary"><b>Edit</b></a> -->

             
              <!-- /.card-body -->
            
      </div>
      <div class="modal-footer">

        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary">Save changes</button>
      </div>
      </form>
    </div>
  </div>
</div>