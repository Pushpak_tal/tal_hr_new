@extends('campusAmbasador.layouts.home')
@section('content')

<div class="content-wrapper">

  <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Here are your tasks</h1>
          </div>
         
        </div>
      </div><!-- /.container-fluid -->
    </section>


    <section class="content">
      <div class="container-fluid">
        <div class="row">

          @foreach($getTask as $task)
          <div class="col-md-12">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">

                <ul class="list-group list-group-unbordered mb-3">
                  <li style="list-style: none;">
                    <h4><b>Your Task Details</b></h4>
                    <p>{{$task->task}}</p>
                  </li>
                  <li style="list-style: none;">
                    <h6><b>Task Type</b></h6> 
                    <p>{{$task->task_type}}</p>
                  </li>
                  <li style="list-style: none;">
                     <h6><b>Points</b></h6> 
                    <p>{{$task->points}}</p>
                  </li>

                  <li style="list-style: none;">
                     <h6><b>Submission Date</b></h6> 
                    <p>{{$task->submission_date}}</p>
                  </li>
                  
                </ul>

                    <button type="button" class="btn btn-success" data-toggle="modal" data-target="#exampleModal{{$task->id}}">Update Status</button>

                    @include('campusAmbasador.pages.modal.updateAmbasadorTaskStatusModal')
              
              </div>
              <!-- /.card-body -->


            </div>



          </div>


           @endforeach
         
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>

</div>


@endsection