@extends('campusAmbasador.layouts.home')
@section('content')

 @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif

@endsection