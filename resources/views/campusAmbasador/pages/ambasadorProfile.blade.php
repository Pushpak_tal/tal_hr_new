@extends('campusAmbasador.layouts.home')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Campus Ambasador Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  

                   <img class="profile-user-img img-fluid img-circle" src="{{asset('/upload/ambasador/' .$getDetail->image)}}">
                </div>

                <h3 class="profile-username text-center">{{$getDetail->name}}</h3>

                <p class="text-muted text-center">Personal Details</p>

                <ul class="list-group list-group-unbordered mb-3">
             
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{$getDetail->email}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Mobile Number</b> <a class="float-right">{{$getDetail->phone}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>State</b> <a class="float-right">{{$getDetail->state}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>District</b> <a class="float-right">{{$getDetail->district}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>City</b> <a class="float-right">{{$getDetail->city}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Pin</b> <a class="float-right">{{$getDetail->pin}}</a>
                  </li>
                </ul>

                <!-- <a href="#" class="btn btn-primary"><b>Edit</b></a> -->

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$getDetail->id}}">Edit</button>

                

                @include('campusAmbasador.pages.modal.editAmbasadorModal')
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            
            <!-- /.card -->
          </div>
          <!-- /.col -->
          
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection