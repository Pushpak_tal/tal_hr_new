 <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link">
      <img src="{{asset('./upload/logo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
           style="opacity: .8;">
      <span class="brand-text font-weight-light">Think Again Lab</span>
    </a>

    @php

    $get=App\EmployeeDetails::where('username',Session::get('employeesession'))->first();

    $user_image=$get->image;

    @endphp

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{url('/upload/employee/'.$user_image)}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="{{url('/demoEmployee')}}" class="d-block">{{Session::get('employeename')}}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-comments" aria-hidden="true"></i>


              <p>
               Inbox
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/inboxMessageEmployee')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Messages</p>
                </a>
              </li>
            </ul>
          </li>

           <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-book" aria-hidden="true"></i>


              <p>
               Knowledge Bank
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/employeeConcern')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>My Concern</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{url('/employeeQuerySolution')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Find Solution</p>
                </a>
              </li>
            </ul>
          </li>
          
          
          <li class="nav-item has-treeview">
            <a href="{{route('employee.profile')}}" class="nav-link">
              <i class="nav-icon fa fa-user-circle" aria-hidden="true"></i>

              <p>
                Profile
                
              </p>
            </a>
           
          </li>

          <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-calendar" aria-hidden="true"></i>

              <p>
               Daily Report
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/employeeDashboard')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Update</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{url('/viewEmployeeDailyUpdate')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>view</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{url('/employeeRequestUpdate')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Request Update</p>
                </a>
              </li>
           
          </li>

            </ul>
          </li>

        <!-- <li class="nav-item has-treeview">
            <a href="#" class="nav-link">
              <i class="nav-icon fa fa-signal" aria-hidden="true"></i>
              <p>
               Performance Appraisal
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{url('/employeePerformanceAppraisal')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Add Appraisal</p>
                </a>
              </li>

              <li class="nav-item">
                <a href="{{url('/viewEmployeeAppraisal')}}" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>View Appraisal</p>
                </a>
              </li>
            </ul>
          </li> -->

          <!-- <li class="nav-item has-treeview">
            <a href="{{url('/employeeLeaveRequest')}}" class="nav-link">
              <i class="nav-icon fa fa-child" aria-hidden="true"></i>


              <p>
                Leave Request
                
              </p>
            </a>
           
          </li> -->
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>