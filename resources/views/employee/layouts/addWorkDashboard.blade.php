<!DOCTYPE html>
<html lang="en">
<head>
  

  <title>TAL | Dashboard</title>
  <link rel="icon" type="image/png" href="./upload/logo.png"/>
  <script src="https://code.jquery.com/jquery-2.1.3.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert-dev.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.css">

  @include('employee.includes.headerlinks')

  
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">


  @include('employee.includes.navbar')
  @include('employee.includes.sidebar')
<div class="content-wrapper">

  @php 

  $emp=App\empBirthday::whereMonth('dob', '=', Carbon\Carbon::now()->format('m'))->whereDay('dob', '=', Carbon\Carbon::now()->format('d'))->first();


  @endphp

   @if(isset($emp))
            <marquee>
               <label style="margin-top: 1%;">Wishing You a very happy birthday from Think Again Family</label>
            </marquee>

  @endif
   
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12" style="margin-top: 3%;">

          	 @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            <!-- general form elements -->
            <div class="card card-primary">

              <div class="card-header">
                <h3 class="card-title">Hi {{Session::get('employeename')}}, Hope you've enjoyed working whole day</h3>
              </div>
             
              <form role="form" method="post" action="{{url('/employeeWorkDetail')}}">
              	@csrf


                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">You are posting report for</label>
                     <input style="text-align: center; background-color: white;border:none;" type="text" disabled="" value="<?php echo \Carbon\Carbon::now()->toDateString(); ?>">
                  </div>
                  <div class="form-group">
                    <label for="exampleInputPassword1">Your work details</label>
                    <textarea name="body" class="form-control" placeholder="What you've done today?" required></textarea>
                  </div>

               
                  <button type="submit" class="btn btn-primary">Submit</button>
                
              </form>
            </div>
        
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  @include('employee.includes.rightsidebar')
 
 @include('employee.includes.footer')

 @include('employee.includes.footerlinks')

  
</div>



              @php

                  $currentDate = \Carbon\Carbon::now()->format('d M');

                  $empDetail = App\EmployeeDetails::where('username',\Session::get('employeesession'))->first();

                  $birthDate = \Carbon\Carbon::parse($empDetail->dob)->format('d M');

              @endphp


               @if($currentDate == $birthDate)

                  <script>
                    swal({
                      title: "Happy Birthday!",
                      text: "Hope your special day brings you all that your heart desires!",    
                     });
                  </script>
                
              @endif


  <script>
        $(document).ready(function(){

    $(document).on("click", "#notification", function() { 
    $.ajax({
      url: '/changeNotificationStatus',
      type: "POST",
      dataType: "json",
      data:{
                _token:'{{ csrf_token() }}',
        username: $('#username').val(),
      },
      success: function(dataResult){
                $('#output').load(document.URL +  ' #output');;
      },
      error: function (dataResult) {
                 alert(dataResult);
           }
    });
  }); 
});

    </script>


</body>
</html>
