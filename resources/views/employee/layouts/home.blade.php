<!DOCTYPE html>
<html lang="en">
<head>
  

  <title>TAL | Dashboard</title>
  <link rel="icon" type="image/png" href="./upload/logo.png"/>
  <meta name="csrf-token" content="{{ csrf_token() }}" />

  @include('employee.includes.headerlinks')

  
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">


  @include('employee.includes.navbar')
  @include('employee.includes.sidebar')
  @yield('content')
  @include('employee.includes.rightsidebar')
 
 @include('employee.includes.footer')

 @include('employee.includes.footerlinks')

  
</div>

<script>
        $(document).ready(function(){

    $(document).on("click", "#notification", function() { 
		$.ajax({
			url: '/changeNotificationStatus',
			type: "POST",
			dataType: "json",
			data:{
                _token:'{{ csrf_token() }}',
				username: $('#username').val(),
			},
			success: function(dataResult){
                $('#output').load(document.URL +  ' #output');;
			},
			error: function (dataResult) {
                 alert(dataResult);
           }
		});
	}); 
});

    </script>

</body>
</html>
