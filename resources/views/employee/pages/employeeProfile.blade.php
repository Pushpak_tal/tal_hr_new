@extends('employee.layouts.home')
@section('content')

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">User Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12">

            <!-- Profile Image -->
            <div class="card card-primary card-outline">
              <div class="card-body box-profile">
                <div class="text-center">
                  

                   <img class="profile-user-img img-fluid img-circle" src="{{asset('/upload/employee/' .$employee_detail->image)}}">
                </div>

                <h3 class="profile-username text-center">{{$employee_detail->name}}</h3>

                <p class="text-muted text-center">Personal Details</p>

                <ul class="list-group list-group-unbordered mb-3">
             
                  <li class="list-group-item">
                    <b>Email</b> <a class="float-right">{{$employee_detail->email}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Mobile Number</b> <a class="float-right">{{$employee_detail->phone}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Date of Birth</b> <a class="float-right">{{$employee_detail->dob}}</a>
                  </li>
                  <li class="list-group-item">
                    <b>Date of Joining</b> <a class="float-right">{{$employee_detail->join_date}}</a>
                  </li>
                </ul>

                <!-- <a href="#" class="btn btn-primary"><b>Edit</b></a> -->

                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal{{$employee_detail->employee_id}}">Edit</button>

                

                @include('employee.pages.modal.editEmployeeModal')
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->

            <!-- About Me Box -->
            
            <!-- /.card -->
          </div>
          <!-- /.col -->
          
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

@endsection