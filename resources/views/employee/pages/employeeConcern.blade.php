@extends('employee.layouts.home')
@section('content')


<div class="content-wrapper">
   
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12" style="margin-top: 3%;">

          	 @if(Session::has('message'))
            <p class="alert {{ Session::get('alert-class') }}">{{ Session::get('message') }}</p>
            @endif
            <!-- general form elements -->
            <div class="card card-primary">

              <div class="card-header">
                <h3 class="card-title">Ask a public question here</h3>
              </div>
             
              <form role="form" method="post" action="{{url('/saveEmployeeConcern')}}">
              	@csrf

                <div class="card-body">
                  <div class="form-group">

                  	<label>Title</label>
                  	<p>Be specific and imagine you’re asking a question to another person</p>
                  	<input type="text" name="title" class="form-control" placeholder="e.g. New GUI released in wordpress!" required>
                   
                  </div>

                  <div class="form-group">

                  	<label>Body</label>
                  	<p>Include all the information someone would need to answer your question</p>
                  	<textarea name="body" class="form-control" placeholder="Write detail here" required></textarea>
                   
                  </div>

                  <div class="form-group">

                  	<label>Tag's</label>
                  	<p>Add up to 4 tags to describe what your question is about</p>
                  	<input type="text" name="tag" class="form-control" placeholder="e.g. (wordpress,GUI,UX,web)" required>
                   
                  </div>
                 

               
                  <button type="submit" class="btn btn-primary">Submit</button>
                
              </form>
            </div>
        
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>


@endsection